
public class Priest extends Hero{

	public Priest(String name, Side side) {
		super(name, side);
		HP = super.getHP() * 1.5;
	}

	@Override
	public void doSpecialMove(Hero hero) {}
	
	
}
