
public class Tank extends Hero{
	
	

	public Tank(String name, Side side) {
		super(name, side);
		HP = super.getHP()*2;
		defPoints = super.getDefPoints() *1.2;
	}

	@Override
	public void doSpecialMove(Hero hero) {}
	
}
