import java.time.LocalDateTime;
import java.util.Date;

public abstract class Hero {
	protected String name;
	protected double HP;
	protected double attackPoints;
	protected double defPoints;
	protected Side side;
	protected long spawnDate;
	protected boolean isBoss;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getHP() {
		return HP;
	}

	public void setHP(double hP) {
		HP = hP;
	}

	public double getAttackPoints() {
		return attackPoints;
	}

	public void setAttackPoints(double attackPoints) {
		this.attackPoints = attackPoints;
	}

	public double getDefPoints() {
		return defPoints;
	}

	public void setDefPoints(double defPoints) {
		this.defPoints = defPoints;
	}

	public Side getSide() {
		return side;
	}

	public void setSide(Side side) {
		this.side = side;
	}

	public long getSpawnDate() {
		return spawnDate;
	}

	public void setSpawnDate(long spawnDate) {
		this.spawnDate = spawnDate;
	}

	public Hero(String name, Side side) {
		this.name = name;
		this.side = side;
		HP = 10000;
		isBoss = false;
		attackPoints = 100;
		defPoints = 50;
		spawnDate = System.currentTimeMillis();

	}

	public abstract void doSpecialMove(Hero hero);
	
	public void attack(Hero enemy) {
		enemy.HP = enemy.HP - (attackPoints - enemy.defPoints);
		
	}
	

}
