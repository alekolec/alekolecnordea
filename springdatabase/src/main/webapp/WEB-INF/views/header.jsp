<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Witaj na mojej stronie</title>
	</head>
	<body>
	<nav>
		<ul>
			<li><a href='<c:url value="/" /> '>Strona główna</a></li>
			<li><a href='<c:url value="/species/add" /> '>Dodaj gatunek</a></li>
			<li><a href='<c:url value="/animal/add" /> '>Dodaj zwierzę</a></li>
			
		</ul>
	</nav>