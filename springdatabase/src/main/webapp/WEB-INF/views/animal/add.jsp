<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<c:import url="../header.jsp" />

<form action='<c:url value="/animal/add" />' method="POST">
	<input type="text" name="name">
	<select name="species">
		<c:forEach items="${species}" var="s">
			<option value="${s.id}">${s.name}</option>
		</c:forEach>
	</select>
	
	
	
	<button type="submit">Dodaj</button>
</form>
		
<c:import url="../footer.jsp" />