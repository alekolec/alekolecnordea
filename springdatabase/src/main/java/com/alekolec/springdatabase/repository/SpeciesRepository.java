package com.alekolec.springdatabase.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.alekolec.springdatabase.entity.Species;

@Repository
public interface SpeciesRepository extends CrudRepository<Species, Long> {
	

}
