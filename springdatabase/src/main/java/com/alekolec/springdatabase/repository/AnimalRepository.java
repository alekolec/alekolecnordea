package com.alekolec.springdatabase.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.alekolec.springdatabase.entity.Animal;

@Repository
public interface AnimalRepository extends CrudRepository<Animal, Long> {

	//Long, zeby SPRING wiedzial jak je numerowac (po tym jak robimy autoWired to spring zarzadza repozytorium i sam sie tym zajmuje)
	
}
