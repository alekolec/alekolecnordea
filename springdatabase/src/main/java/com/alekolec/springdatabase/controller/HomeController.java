package com.alekolec.springdatabase.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alekolec.springdatabase.entity.Species;
import com.alekolec.springdatabase.repository.SpeciesRepository;

@Controller
public class HomeController {

	@Autowired
	SpeciesRepository speciesRepository;
	
	
	@RequestMapping("/")
	public String homepage(ModelMap model) {
		List<Species> listOfSpecies = (List<Species>) speciesRepository.findAll();
		model.addAttribute("species", listOfSpecies);
		return "index";
		
	}
	
	
	
}
