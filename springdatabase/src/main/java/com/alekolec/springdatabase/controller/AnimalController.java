package com.alekolec.springdatabase.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.alekolec.springdatabase.entity.Animal;
import com.alekolec.springdatabase.entity.Species;
import com.alekolec.springdatabase.repository.AnimalRepository;
import com.alekolec.springdatabase.repository.SpeciesRepository;

@Controller
@RequestMapping("/animal")
public class AnimalController {
	
	@Autowired
	SpeciesRepository speciesRepository;
	
	@Autowired
	AnimalRepository animalRepository;

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addAnimal(ModelMap model) {
		List<Species> listOfSpecies = (List<Species>) speciesRepository.findAll();
		model.addAttribute("species", listOfSpecies);
		return "animal/add";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String handleAddAnimal(@RequestParam("name") String name,
								  @RequestParam("species") long speciesId) {
		Species s = speciesRepository.findOne(speciesId);
		Animal a = new Animal(name);
		a.setSpecies(s);
		animalRepository.save(a);
		return "index";
	}
	
	@RequestMapping("/delete/{id}")
	public void delete(@PathVariable("id") long id,
						HttpServletRequest request,
						HttpServletResponse response) throws IOException {
		Animal a = animalRepository.findOne(id);
		Species s = a.getSpecies();
		List<Animal> listOfAnimals = s.getListOfAnimals();
		listOfAnimals.remove(a);
		speciesRepository.save(s);
		animalRepository.delete(a);
		
		response.sendRedirect(request.getContextPath() + "/");
	}
	
}
