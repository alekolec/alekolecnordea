import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class OrderForm {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextArea textArea;
	private JTextArea textArea_2;
	private JRadioButton rdbtnMczyzna;
	private JRadioButton rdbtnKobieta;
	private JComboBox comboBox;
	private List<Order> orders;
	private DefaultListModel listModel;
	private JList jList;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OrderForm window = new OrderForm();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public OrderForm() {
		initialize();
		ButtonGroup group = new ButtonGroup();
		group.add(rdbtnMczyzna);
		group.add(rdbtnKobieta);
		listModel = new DefaultListModel();
		jList.setModel(listModel);
		orders = new ArrayList<>();

		jList.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {
				textArea_2.setText("Zaznaczono");
				int selectedIndex = jList.getSelectedIndex();
				if (selectedIndex == -1) {
					textArea_2.setText("Nie zaznaczono elementu.");
					return;
				}
				Order order = (Order) listModel.getElementAt(selectedIndex);
				textField.setText(order.getImie());
				textField_1.setText(order.getNazwisko());
				textField_2.setText(Integer.toString(order.getRok()));
				if (order.getPlec() == PLEC.KOBIETA) {
					rdbtnKobieta.setSelected(true);
				} else {
					rdbtnMczyzna.setSelected(true);
				}
				if (order.getCywilny() == STAN_CYWILNY.WOLNY) {
					comboBox.setSelectedItem("Panna/Kawaler");
				} else {
					comboBox.setSelectedItem("Zam�na/�onaty");
				}

				textArea.setText(order.getTresc());

			}
		});

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		BorderLayout borderLayout = (BorderLayout) frame.getContentPane().getLayout();
		borderLayout.setHgap(5);
		borderLayout.setVgap(5);
		frame.setBounds(100, 100, 502, 604);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel centerPanel = new JPanel();
		frame.getContentPane().add(centerPanel, BorderLayout.CENTER);
		GridBagLayout gbl_centerPanel = new GridBagLayout();
		gbl_centerPanel.columnWidths = new int[] { 243, 243, 0 };
		gbl_centerPanel.rowHeights = new int[] { 566, 0 };
		gbl_centerPanel.columnWeights = new double[] { 0.0, 0.0, Double.MIN_VALUE };
		gbl_centerPanel.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		centerPanel.setLayout(gbl_centerPanel);

		JPanel leftPanel = new JPanel();
		GridBagConstraints gbc_leftPanel = new GridBagConstraints();
		gbc_leftPanel.weightx = 0.3;
		gbc_leftPanel.weighty = 1.0;
		gbc_leftPanel.fill = GridBagConstraints.BOTH;
		gbc_leftPanel.insets = new Insets(0, 0, 0, 5);
		gbc_leftPanel.gridx = 0;
		gbc_leftPanel.gridy = 0;
		centerPanel.add(leftPanel, gbc_leftPanel);
		leftPanel.setLayout(new GridLayout(2, 1, 0, 0));

		JPanel upLeft = new JPanel();
		leftPanel.add(upLeft);
		upLeft.setLayout(new GridLayout(11, 1, 0, 0));

		JLabel lblImi = new JLabel("Imi\u0119:");
		lblImi.setAlignmentX(0.5f);
		upLeft.add(lblImi);

		textField = new JTextField();
		upLeft.add(textField);
		textField.setColumns(10);

		JLabel lblNazwisko = new JLabel("Nazwisko:");
		upLeft.add(lblNazwisko);

		textField_1 = new JTextField();
		upLeft.add(textField_1);
		textField_1.setColumns(10);

		JLabel lblRokUrodzenia = new JLabel("Rok urodzenia:");
		upLeft.add(lblRokUrodzenia);

		textField_2 = new JTextField();
		upLeft.add(textField_2);
		textField_2.setColumns(10);

		JLabel lblPe = new JLabel("P\u0142e\u0107:");
		upLeft.add(lblPe);

		rdbtnMczyzna = new JRadioButton("M\u0119\u017Cczyzna");
		upLeft.add(rdbtnMczyzna);
		rdbtnMczyzna.setSelected(true);

		rdbtnKobieta = new JRadioButton("Kobieta");
		upLeft.add(rdbtnKobieta);

		JLabel lblStanCywilny = new JLabel("Stan cywilny:");
		upLeft.add(lblStanCywilny);

		comboBox = new JComboBox<>();
		upLeft.add(comboBox);
		comboBox.addItem("Panna/Kawaler");
		comboBox.addItem("Zam�na/�onaty");

		JPanel downLeft = new JPanel();
		leftPanel.add(downLeft);
		downLeft.setLayout(new GridLayout(4, 1, 0, 0));

		JLabel lblTreOgloszenia = new JLabel("Tre\u015B\u0107 ogloszenia:");
		downLeft.add(lblTreOgloszenia);

		JScrollPane scrollPane = new JScrollPane();
		downLeft.add(scrollPane);

		textArea = new JTextArea();
		scrollPane.setViewportView(textArea);

		JPanel panel = new JPanel();
		downLeft.add(panel);
		panel.setLayout(new GridLayout(0, 3, 0, 0));

		JButton btnNewButton = new JButton("Wyczy\u015B\u0107");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				clearFields();

			}

		});
		panel.add(btnNewButton);

		JButton btnNewButton_1 = new JButton("Dodaj");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String imie = textField.getText();
				String nazwisko = textField_1.getText();
				try {
					int rok = Integer.parseInt(textField_2.getText());
					if (rok < 1900 || rok > 2017) {
						textArea_2.setText("Niemo�liwa data!");
						clearFields();
						return;
					}
					PLEC plec = PLEC.KOBIETA;
					if (rdbtnMczyzna.isSelected()) {
						plec = PLEC.MEZCZYZNA;
					}
					STAN_CYWILNY stan = STAN_CYWILNY.WOLNY;
					if (comboBox.getSelectedItem().equals("Zam�na/�onaty")) {
						stan = STAN_CYWILNY.ZONATY;
					}

					String tresc = textArea.getText();
					Order order = new Order(imie, nazwisko, rok, plec, stan, tresc);
					orders.add(order);
					listModel.addElement(order);
					textArea_2.setText("Dodano zg�oszenie!");
					System.out.println("Dodano: " + order);
				} catch (Exception e) {
					textArea_2.setText("Niepoprawny format!");
					return;

				}

			}
		});
		panel.add(btnNewButton_1);

		JButton btnUsu = new JButton("Usu\u0144");
		btnUsu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (jList.getSelectedIndex() == -1) {
					textArea_2.setText("�aden element nie zosta� zaznaczony!");
					return;
				}
				Order order = (Order) listModel.getElementAt(jList.getSelectedIndex());
				listModel.removeElement(order);
				orders.remove(order);
				textArea_2.setText("Usuni�to!");
			}
		});
		panel.add(btnUsu);

		JButton btnNewButton_2 = new JButton("Aktualizuj");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (jList.getSelectedIndex() == -1) {
					textArea_2.setText("�aden element nie zosta� zaznaczony!");
					return;
				}
				Order order = (Order) listModel.getElementAt(jList.getSelectedIndex());
				order.setImie(textField.getText());
				order.setNazwisko(textField_1.getText());
				order.setRok(Integer.parseInt(textField_2.getText()));
				order.setTresc(textArea.getText());
				if (rdbtnKobieta.isSelected()) {
					order.setPlec(PLEC.KOBIETA);
				} else {
					order.setPlec(PLEC.MEZCZYZNA);
				}
				if (comboBox.getSelectedItem().equals("Zam�na/�onaty")) {
					order.setCywilny(STAN_CYWILNY.ZONATY);
				} else {
					order.setCywilny(STAN_CYWILNY.WOLNY);
				}
				clearFields();
				textArea_2.setText("Zaktualizowano!");
			}
		});
		panel.add(btnNewButton_2);

		JButton btnNewButton_3 = new JButton("Zapisz");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					PrintStream out = new PrintStream("zgloszenia.txt");
					for (Order order : orders) {
						out.println(order.getImie() + ";" + order.getNazwisko() + ";" + order.getRok() + ";"
								+ order.getPlec() + ";" + order.getCywilny() + ";" + order.getTresc().replace("\n", "%20"));
					}
					clearFields();
					textArea_2.setText("Zapisano!");
					out.close();
					} catch (FileNotFoundException e) {
					textArea_2.setText("Nie uda�o si�");
					e.printStackTrace();
				}
			}
		});
		panel.add(btnNewButton_3);

		JButton btnNewButton_4 = new JButton("Wczytaj");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Scanner scanner;
				try {
					scanner = new Scanner(new File("zgloszenia.txt"));

					List<Order> loadOrders = new ArrayList<Order>();

					while (scanner.hasNextLine()) {
						String line = scanner.nextLine();
						String[] split = line.split(";");
						Order order = new Order();

						order.setImie(split[0]);
						order.setNazwisko(split[1]);
						order.setRok(Integer.parseInt(split[2]));
						order.setPlec(PLEC.valueOf(split[3]));
						order.setCywilny(STAN_CYWILNY.valueOf(split[4]));
						order.setTresc(split[5].replace("%20", "\n"));

						listModel.addElement(order);
						orders.add(order);

					}
					textArea_2.setText("Wczytano!");
				} catch (FileNotFoundException e1) {
					textArea_2.setText("Nie uda�o si�");
					e1.printStackTrace();
				}

			}

		});
		panel.add(btnNewButton_4);

		textArea_2 = new JTextArea();
		downLeft.add(textArea_2);

		JPanel rightPanel = new JPanel();
		GridBagConstraints gbc_rightPanel = new GridBagConstraints();
		gbc_rightPanel.weighty = 1.0;
		gbc_rightPanel.weightx = 1.0;
		gbc_rightPanel.fill = GridBagConstraints.BOTH;
		gbc_rightPanel.gridx = 1;
		gbc_rightPanel.gridy = 0;
		centerPanel.add(rightPanel, gbc_rightPanel);
		rightPanel.setLayout(new BorderLayout(0, 0));

		JLabel lblListaZgosze = new JLabel("Lista zg\u0142osze\u0144:");
		lblListaZgosze.setVerticalAlignment(SwingConstants.TOP);
		rightPanel.add(lblListaZgosze, BorderLayout.NORTH);

		JScrollPane scrollPane_1 = new JScrollPane();
		rightPanel.add(scrollPane_1);

		jList = new JList();
		scrollPane_1.setViewportView(jList);
	}

	private void clearFields() {
		textField.setText(null);
		textField_1.setText(null);
		textField_2.setText(null);
		rdbtnMczyzna.setSelected(true);
		textArea.setText(null);
	}
}
