
public class Order {
	private String imie;
	private String nazwisko;
	private int rok;
	private PLEC plec;
	private STAN_CYWILNY cywilny;
	private String tresc;
	
	public Order(String imie, String nazwisko, int rok, PLEC plec, STAN_CYWILNY cywilny, String tresc) {
		super();
		this.imie = imie;
		this.nazwisko = nazwisko;
		this.rok = rok;
		this.plec = plec;
		this.cywilny = cywilny;
		this.tresc = tresc;
	}
	
	public Order() {
		
	}

	public String getImie() {
		return imie;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}

	public int getRok() {
		return rok;
	}

	public void setRok(int rok) {
		this.rok = rok;
	}

	public PLEC getPlec() {
		return plec;
	}

	public void setPlec(PLEC plec) {
		this.plec = plec;
	}

	public STAN_CYWILNY getCywilny() {
		return cywilny;
	}

	public void setCywilny(STAN_CYWILNY cywilny) {
		this.cywilny = cywilny;
	}

	public String getTresc() {
		return tresc;
	}

	public void setTresc(String tresc) {
		this.tresc = tresc;
	}

	@Override
	public String toString() {
		return imie + ", " +  nazwisko + ", " + rok + ", " + plec + ", " +  cywilny + ", " + tresc;
	}
	

	
	

}
