package firma;


public class Company {
	
	public static void main(String[] args) {
		
		Worker worker1 = new SimpleWorker("Antoni", "Pracownik");
		Worker worker2 = new SimpleWorker("Zdzis�aw", "Sprz�tacz");
		Worker worker3 = new SimpleWorker("Czes�aw", "Ochroniarz");
		Worker worker4 = new SimpleWorker("Anna", "Ksi�gowa");
		
		Manager menago2 = new Manager("Zenon", worker1, worker2);
		
		
		Manager menago = new Manager("Stefan", worker1, worker2, worker3, worker4, menago2);
		
		menago.introduce();

	}

}
