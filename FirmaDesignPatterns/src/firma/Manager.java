package firma;

import java.util.LinkedList;
import java.util.List;

public class Manager implements Worker{
	private String name;
	private List<Worker> workers = new LinkedList<>();
	
	
	public Manager(String name, Worker... workers) {  // taka jakby nieskonczona tablica workerow
		this.name = name;
		for(int i = 0; i< workers.length; i++)
		this.workers.add(workers[i]);
		//this.workers = workers;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public List<Worker> getWorkers() {
		return workers;
	}


	public void setWorkers(List<Worker> workers) {
		this.workers = workers;
	}
	
	public void addWorker(Worker worker) {
		workers.add(worker);
	}


	@Override
	public void introduce() {   // metoda introduce wyswietla grupe pracownikow
		System.out.println("Jestem managerem, nazywam si� " + name + " , a to moim pracownicy: ");
		
		for(Worker worker : workers) {
			worker.introduce();
		
	}
	
	}

}
