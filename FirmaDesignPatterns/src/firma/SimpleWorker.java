package firma;

public class SimpleWorker implements Worker{
	private String name;
	private String function;
	
	
	public SimpleWorker(String name, String function) {
		super();
		this.name = name;
		this.function = function;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getFunction() {
		return function;
	}


	public void setFunction(String function) {
		this.function = function;
	}


	@Override
	public void introduce() {
		System.out.println("Cze��, jestem pracownikiem i mam na imi� " + name + ", a moja funkcja to: " + function);
		
	}
	
	

}
