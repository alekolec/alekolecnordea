package pl.sdacademy.dbconnector;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class CityResolver extends AbstractResolver<City> {

	@Override
	public City get(int id) {
		City c = null;
		
		try {
			Connection connection = DBConnector.getConnection();
			Statement stmt = connection.createStatement();
			String sql = "SELECT * FROM city WHERE id = " + id;
			ResultSet rs = stmt.executeQuery(sql);
			if(rs.next()) {
				c = new City(rs.getInt("id"), rs.getString("name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return c;
	}

	@Override
	public List<City> get() {
		List<City> listOfcities = new LinkedList<>();
		
		try {
			Connection connection = DBConnector.getConnection();
			Statement stmt = connection.createStatement();
			String sql = "SELECT * FROM city WHERE id > 0";
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()) {
				listOfcities.add(new City(rs.getInt("id"), rs.getString("name")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return listOfcities;
	}

	@Override
	public void delete(int id) {
		try {
			Connection connection = DBConnector.getConnection();
			Statement stmt = connection.createStatement();
			String sql = "DELETE FROM city WHERE id = " + id;
			stmt.execute(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void insert(Map<String, String> params) {
		try {
			Connection connection = DBConnector.getConnection();
			Statement stmt = connection.createStatement();
			String sql = insertQuery(params);
			stmt.execute(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void update(int id, Map<String, String> params) {
		try {
			Connection connection = DBConnector.getConnection();
			Statement stmt = connection.createStatement();
			String sql = updateQuery(params);
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private String insertQuery(Map<String, String> params) {
		String sql = "INSERT INTO city (";
		String keys = "";
		String values = "";
		
		for(String key : params.keySet()) {
			keys += key + ", ";
			values += "'"+ params.get(key) + "', ";
		}
		if(keys.length() > 0) {
			keys = keys.substring(0, keys.length() - 2);
			values = values.substring(0, values.length() - 2);
		}
		
		sql += keys + ") VALUES (" + values + ")";
		return sql;
	}
	
	private String updateQuery(Map<String, String> params) {
		if(!params.containsKey("id")) {
			return null;
		}
		int id = Integer.valueOf(params.get("id"));
		params.remove("id");
		String sql = "UPDATE city SET ";
		
		for(String key : params.keySet()) {
			sql += key + " = '" + params.get(key) + "', ";
			sql = sql.substring(0, sql.length() - 2);
		}	
		sql += " WHERE id = " + id;
		return sql;
	}

}