package pl.sdacademy.dbconnector;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

public class App {

	public static void main(String[] args) {
		
		CityResolver cr = new CityResolver();
		
		Map<String,String> p = new HashMap<>();
		p.put("id", "1");
		p.put("name", "Gdynia");
		
		
		
		String dbhost = "localhost";
		String dbname = "GEO";
		String dbuser = "postgres";
		String dbpass = "admin";
		int dbport = 5432;

		Connection connection = null;
		String url = "jdbc:postgresql://" + dbhost + ":" + dbport + "/" + dbname;

		try {
			//connection = DriverManager.getConnection(url, dbuser, dbpass);
			connection = DBConnector.getConnection();
			Statement stmt = connection.createStatement();
			String sql = "SELECT * FROM city WHERE id>0 LIMIT 200";
			ResultSet rs = stmt.executeQuery(sql);
			
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");

				System.out.println(id + "." + name);
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

	}

}
