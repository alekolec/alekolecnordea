package pl.sdacademy.dbconnector;

public class SingletonExample {
	private static SingletonExample _instance = null;
	
	private SingletonExample() {
		
	}
	
	public SingletonExample getInstance() {
		if(_instance == null) {
			_instance = new SingletonExample();
			System.out.println("Utworzyłem instancję!");
		} 
		System.out.println("Zwracam instancję");
		return _instance;
	}

}
