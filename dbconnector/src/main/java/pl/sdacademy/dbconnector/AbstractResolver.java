package pl.sdacademy.dbconnector;

import java.util.List;
import java.util.Map;

public abstract class AbstractResolver<T> {

	public abstract T get(int id);
	public abstract List<T> get();
	public abstract void delete(int id);
	public abstract void insert(Map<String, String> params);
	public abstract void update(int id, Map<String,String> params);
	
	
}
