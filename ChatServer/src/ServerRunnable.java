import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerRunnable implements Runnable {
	
	private BufferedReader reader;
	private PrintWriter writer;
	private IMessageListener listener;
	
	
	
	public ServerRunnable(IMessageListener listener) {
		super();
		this.listener = listener;
	}



	@Override
	public void run() {
		try {
			ServerSocket socketPolaczenia = new ServerSocket(10001);
			while(true){ // nieskonczona petla
			Socket otwartySocket = socketPolaczenia.accept();

			// otwieranie wyjscia
			OutputStream kanalWyjscia = otwartySocket.getOutputStream();
			writer = new PrintWriter(kanalWyjscia);
			
			
			// otwieranie wejscia
			InputStream kanalWejscia = otwartySocket.getInputStream();
			reader = new BufferedReader(new InputStreamReader(kanalWejscia));
			

			String otrzymanaLinia = null;
			do{
				otrzymanaLinia = reader.readLine();
				listener.messageArrived("Wiadomo��: " + otrzymanaLinia);

				System.out.println(otrzymanaLinia);
			}while(otrzymanaLinia != null);
			reader.close();
			}
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
			ioe.printStackTrace();
		}
	}
	
	public void sendMessage(String text) {
		writer.println(text);
		writer.flush();
	}

}
