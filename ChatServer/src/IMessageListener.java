
public interface IMessageListener {
	void messageArrived(String newMessage);
}
