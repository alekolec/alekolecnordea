import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import javax.swing.JTextField;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.JLabel;

public class MainWindow {

	private JFrame frame;
	private JTextField txtAdresSerwera;
	private JTextField txtPortserwera;
	private JButton btnConnect;
	private JButton btnNewButton;
	private JTextField txtTre;
	private JTextArea textArea;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 347, 345);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		lblNewLabel = new JLabel("Host:");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		panel.add(lblNewLabel, gbc_lblNewLabel);
		
		txtAdresSerwera = new JTextField();
		txtAdresSerwera.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_txtAdresSerwera = new GridBagConstraints();
		gbc_txtAdresSerwera.gridwidth = 3;
		gbc_txtAdresSerwera.insets = new Insets(0, 0, 5, 5);
		gbc_txtAdresSerwera.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtAdresSerwera.gridx = 1;
		gbc_txtAdresSerwera.gridy = 0;
		panel.add(txtAdresSerwera, gbc_txtAdresSerwera);
		txtAdresSerwera.setColumns(10);
		
		lblNewLabel_1 = new JLabel("Port");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_1.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel_1.gridx = 5;
		gbc_lblNewLabel_1.gridy = 0;
		panel.add(lblNewLabel_1, gbc_lblNewLabel_1);
		
		txtPortserwera = new JTextField();
		txtPortserwera.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_txtPortserwera = new GridBagConstraints();
		gbc_txtPortserwera.insets = new Insets(0, 0, 5, 0);
		gbc_txtPortserwera.gridwidth = 3;
		gbc_txtPortserwera.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPortserwera.gridx = 6;
		gbc_txtPortserwera.gridy = 0;
		panel.add(txtPortserwera, gbc_txtPortserwera);
		txtPortserwera.setColumns(10);
		
		btnConnect = new JButton("Connect");
		GridBagConstraints gbc_btnConnect = new GridBagConstraints();
		gbc_btnConnect.insets = new Insets(0, 0, 5, 5);
		gbc_btnConnect.gridx = 4;
		gbc_btnConnect.gridy = 1;
		panel.add(btnConnect, gbc_btnConnect);
		
		textArea = new JTextArea();
		GridBagConstraints gbc_textArea = new GridBagConstraints();
		gbc_textArea.gridwidth = 5;
		gbc_textArea.gridheight = 7;
		gbc_textArea.insets = new Insets(0, 0, 5, 5);
		gbc_textArea.fill = GridBagConstraints.BOTH;
		gbc_textArea.gridx = 2;
		gbc_textArea.gridy = 2;
		panel.add(textArea, gbc_textArea);
		
		txtTre = new JTextField();
		txtTre.setHorizontalAlignment(SwingConstants.CENTER);
		txtTre.setText("TRE\u015A\u0106");
		GridBagConstraints gbc_txtTre = new GridBagConstraints();
		gbc_txtTre.gridwidth = 5;
		gbc_txtTre.insets = new Insets(0, 0, 5, 5);
		gbc_txtTre.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtTre.gridx = 2;
		gbc_txtTre.gridy = 9;
		panel.add(txtTre, gbc_txtTre);
		txtTre.setColumns(10);
		
		btnNewButton = new JButton("SEND");
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(0, 0, 0, 5);
		gbc_btnNewButton.gridx = 4;
		gbc_btnNewButton.gridy = 10;
		panel.add(btnNewButton, gbc_btnNewButton);
	}

}
