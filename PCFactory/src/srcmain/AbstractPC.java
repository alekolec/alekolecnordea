package srcmain;

public abstract class AbstractPC {
	private String name;
	private COMPUTER_BRAND brand;
	private int cpu_power;
	private double gpu_power;
	private boolean isOverclocked;
		
	
	public AbstractPC(String name, COMPUTER_BRAND brand, int cpu_power, double gpu_power, boolean isOverclocked) {
		super();
		this.name = name;
		this.brand = brand;
		this.cpu_power = cpu_power;
		this.gpu_power = gpu_power;
		this.isOverclocked = isOverclocked;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public COMPUTER_BRAND getBrand() {
		return brand;
	}
	public void setBrand(COMPUTER_BRAND brand) {
		this.brand = brand;
	}
	public int getCpu_power() {
		return cpu_power;
	}
	public void setCpu_power(int cpu_power) {
		this.cpu_power = cpu_power;
	}
	public double getGpu_power() {
		return gpu_power;
	}
	public void setGpu_power(int gpu_power) {
		this.gpu_power = gpu_power;
	}
	public boolean isOverclocked() {
		return isOverclocked;
	}
	public void setOverclocked(boolean isOverclocked) {
		this.isOverclocked = isOverclocked;
	}
	@Override
	public String toString() {
		return "AbstractPC [name=" + name + ", brand=" + brand + ", cpu_power=" + cpu_power + ", gpu_power=" + gpu_power
				+ ", isOverclocked=" + isOverclocked + "]";
	}
	
	

}
