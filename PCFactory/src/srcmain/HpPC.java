package srcmain;

public class HpPC extends AbstractPC{

	public HpPC(String name, COMPUTER_BRAND brand, int cpu_power, double gpu_power, boolean isOverclocked) {
		super(name, brand, cpu_power, gpu_power, isOverclocked);
		
	}
	
	public static AbstractPC createHPFastPC() {
		return new HpPC("HP1", COMPUTER_BRAND.HP, 89, 0.56, true );
		
	}
	
	public static AbstractPC createHPSlowPC() {
		return new HpPC("HP2", COMPUTER_BRAND.HP, 43, 0.23, false );
		
	}

}
