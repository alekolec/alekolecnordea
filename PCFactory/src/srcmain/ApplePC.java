package srcmain;

public class ApplePC extends AbstractPC {

	public ApplePC(String name, COMPUTER_BRAND brand, int cpu_power, double gpu_power, boolean isOverclocked) {
		super(name, brand, cpu_power, gpu_power, isOverclocked);

	}

	public static AbstractPC createAppleFastPC() {
		return new ApplePC("Apple1", COMPUTER_BRAND.APPLE, 89, 0.56, true);

	}

	public static AbstractPC createAppleSlowPC() {
		return new ApplePC("Apple2", COMPUTER_BRAND.APPLE, 43, 0.23, false);

	}

}
