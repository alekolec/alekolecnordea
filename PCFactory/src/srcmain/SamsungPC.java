package srcmain;

public class SamsungPC extends AbstractPC {

	public SamsungPC(String name, COMPUTER_BRAND brand, int cpu_power, double gpu_power, boolean isOverclocked) {
		super(name, brand, cpu_power, gpu_power, isOverclocked);
		
	}
	
	public static AbstractPC createSamsungFastPC() {
		return new SamsungPC("Samsung1", COMPUTER_BRAND.SAMSUNG, 89, 0.56, true );
		
	}
	
	public static AbstractPC createHPSlowPC() {
		return new SamsungPC("Samsung2", COMPUTER_BRAND.SAMSUNG, 43, 0.23, false );
		
	}

}
