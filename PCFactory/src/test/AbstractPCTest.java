package test;

import org.junit.Assert;
import org.junit.Test;

import srcmain.AbstractPC;
import srcmain.ApplePC;
import srcmain.AsusPC;
import srcmain.HpPC;
import srcmain.SamsungPC;

public class AbstractPCTest {
	
	@Test
	public void testHP1() {
		AbstractPC hp1 = HpPC.createHPSlowPC();
		
		Assert.assertEquals("HP2", hp1.getName());
		Assert.assertEquals(43, hp1.getCpu_power());
		Assert.assertEquals(0.23, hp1.getGpu_power(),0);
	}
	
	@Test
	public void testHP2() {
		AbstractPC hp2 = HpPC.createHPFastPC();
		
		Assert.assertEquals("HP1", hp2.getName());
		Assert.assertEquals(89, hp2.getCpu_power());
	}

	@Test
	public void testSamsung1() {
		AbstractPC samsung1 = SamsungPC.createSamsungFastPC();
		
		Assert.assertEquals("Samsung1", samsung1.getName());
		Assert.assertEquals(89, samsung1.getCpu_power());
	}
	
	@Test
	public void testSamsung2() {
		AbstractPC samsung2 = SamsungPC.createHPSlowPC();
		
		Assert.assertEquals("Samsung2", samsung2.getName());
		Assert.assertEquals(43, samsung2.getCpu_power());
	}
	
	@Test
	public void testApple1() {
		AbstractPC apple1 = ApplePC.createAppleFastPC();
		
		Assert.assertEquals("Apple1", apple1.getName());
		Assert.assertEquals(89, apple1.getCpu_power());
	}
	
	@Test
	public void testApple2() {
		AbstractPC apple2 = ApplePC.createAppleSlowPC();
		
		Assert.assertEquals("Apple2", apple2.getName());
		Assert.assertEquals(43, apple2.getCpu_power());
	}
	
	@Test
	public void testAsus1() {
		AbstractPC asus1 = AsusPC.createASUSFastPC();
		
		Assert.assertEquals("Asus1", asus1.getName());
		Assert.assertEquals(89, asus1.getCpu_power());
	}
	
	@Test
	public void testAsus2() {
		AbstractPC asus2 = AsusPC.createASUSSlowPC();
		
		Assert.assertEquals("Asus2", asus2.getName());
		Assert.assertEquals(43, asus2.getCpu_power());
	}
	
}
