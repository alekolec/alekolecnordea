package com.alekolec.communication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientMain {
	public static void main(String[] args) {

		// Scanner scanner = new Scanner(System.in);
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

			String line = null;

			try {
				System.out.println("Podaj adres hosta");
				String host = reader.readLine();
				System.out.println("Podaj numer portu");
				int port = Integer.parseInt(reader.readLine());

				Socket socket = new Socket(host, port);

				OutputStream os = socket.getOutputStream();
				InputStream is = socket.getInputStream();

				PrintWriter writer = new PrintWriter(os);
				do {
					System.out.println("Podaj wiadomo�� do wys�ania");
					line = reader.readLine();
					writer.println(line);
					writer.flush();
					BufferedReader readerServerAnswer = new BufferedReader(new InputStreamReader(is));
					System.out.println(readerServerAnswer.readLine());
				
					
				} while (!line.equals(""));
				writer.close();
			} catch (IOException ioe) {
				System.out.println(ioe.getMessage());
				ioe.printStackTrace();
			}

		} catch (Exception ioe) {
			System.out.println("Exception" + ioe.getMessage());

		}

	}

}
