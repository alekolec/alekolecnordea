package com.alekolec.dbStudentJDBC.resolver;

import java.util.List;
import java.util.Map;

public abstract class AbstractResolver<T> {
	public abstract T get(int id);
	public abstract List<T> get();
	public abstract boolean delete(int id);
	public abstract boolean insertOrUpdate(Map<String, String> params);

}
