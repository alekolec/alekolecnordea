package com.alekolec.dbStudentJDBC.resolver.student;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alekolec.dbStudentJDBC.connector.DBConnector;
import com.alekolec.dbStudentJDBC.helper.QueryHelper;
import com.alekolec.dbStudentJDBC.model.Student;
import com.alekolec.dbStudentJDBC.resolver.AbstractResolver;

public class StudentResolver extends AbstractResolver<Student> {
	private Connection connection = DBConnector.getConnection();
	private final String TABLE_NAME = "student";

	@Override
	public Student get(int id) {
		Student student = null;

		try {
			Statement stmt = connection.createStatement();
			Map<String, String> param = new HashMap<>();
			param.put("id", "" + id);
			String sql = QueryHelper.select(TABLE_NAME, param);
			ResultSet rs = stmt.executeQuery(sql);
			if (rs.next()) {
				student = new Student(rs.getInt("id"), rs.getString("name"), rs.getString("lastname"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return student;
	}

	@Override
	public List<Student> get() {
		List<Student> listOfStudents = new ArrayList<>();

		try {
			Statement stmt = connection.createStatement();
			String sql = QueryHelper.select(TABLE_NAME, null);
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				listOfStudents.add(new Student(rs.getInt("id"), rs.getString("name"), rs.getString("lastname")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return listOfStudents;
	}

	@Override
	public boolean delete(int id) {
		try {
			Statement stmt = connection.createStatement();
			String sql = QueryHelper.delete(TABLE_NAME, id);
			stmt.execute(sql);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean insertOrUpdate(Map<String, String> params) {
		try {
			if (params.containsKey("id")) {
				Statement stmt = connection.createStatement();
				String sql = QueryHelper.update(TABLE_NAME, params);
				stmt.executeUpdate(sql);
			} else {

				Statement stmt = connection.createStatement();
				String sql = QueryHelper.insert(TABLE_NAME, params);
				stmt.execute(sql);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
