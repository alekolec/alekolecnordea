package com.alekolec.dbStudentJDBC.helper;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class QueryHelper {

	//-------------------- UPDATE------------------------//
	public static String update(String tableName, Map<String, String> props) {
		StringBuffer sql = new StringBuffer();

		if (props.size() == 0 || !props.containsKey("id")) {
			return null;
		}
		int id = Integer.valueOf(props.get("id"));
		props.remove("id"); 

		sql.append("UPDATE ");
		sql.append(tableName);
		sql.append(" SET ");

		for (String key : props.keySet()) {
			sql.append(key);
			sql.append("= '");
			sql.append(props.get(key));
			sql.append("', ");
		}

		String sqlQuery = sql.toString();

		sqlQuery = sqlQuery.substring(0, sqlQuery.length() - 2);
		sql = new StringBuffer(sqlQuery);
		sql.append(" WHERE id= ");
		sql.append(id);

		return sql.toString();

	}

	//-------------------------INSERT---------------------//
	public static String insert(String tableName, Map<String, String> props) {
		StringBuffer sql = new StringBuffer();
		sql.append("INSERT INTO ");
		sql.append(tableName);
		StringBuffer keys = new StringBuffer();
		StringBuffer values = new StringBuffer();
		
		for (String key : props.keySet()) {
			keys.append(key);
			keys.append(", ");
			
			values.append("'");
			values.append(props.get(key));
			values.append("', ");

		}
		
		String keysSub = keys.substring(0, keys.length() - 2);
		String valuesSub = values.substring(0, values.length() - 2);
		
		keys = new StringBuffer(keysSub);
		values = new StringBuffer(valuesSub);
		
		sql.append(" (");
		sql.append(keys);
		sql.append(") VALUES (");
		sql.append(values);
		sql.append(")");
		
		
		
		
		return sql.toString();
	}

	//-----------------------SELECT----------------------------------//
	public static String select(String tableName, Map<String, String> props) {
		if (props == null || props.size() == 0) {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT * FROM ");
			sql.append(tableName);

			return sql.toString();

		} else if (props.size() == 1) {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT * FROM ");
			sql.append(tableName);
			sql.append("WHERE ");
			String key = "", value = "";
			for (Entry<String, String> e : props.entrySet()) {
				key = e.getKey();
				value = e.getValue();
			}

			sql.append(key);
			sql.append("=");
			sql.append(value);

			return sql.toString();

		} else {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT * FROM ");
			sql.append(tableName);
			sql.append(" WHERE ");
			String key = "", value = "";
			for (Entry<String, String> e : props.entrySet()) {
				key = e.getKey();
				value = e.getValue();
				sql.append(key);
				sql.append("= '");
				sql.append(value);
				sql.append("' AND ");
			}

			String sqlQuery = sql.toString();
			sqlQuery = sqlQuery.substring(0, sqlQuery.length() - 5);

			return sqlQuery.toString();
		}
	}

	//-----------------------------DELETE----------------------//
	public static String delete(String tableName, int id) {
		StringBuffer sql = new StringBuffer();
		sql.append("DELETE FROM ");
		sql.append(tableName);
		sql.append(" WHERE id= ");
		sql.append(id);
		
		return sql.toString();
	}

	public static void main(String[] args) {
		Map<String, String> mapa = new HashMap<>();
		mapa.put("id", "3");
		mapa.put("name", "Ola");
		mapa.put("lastName", "Koleczka");
		mapa.put("city", "Gdynia");

		System.out.println(update("ja", mapa));
		mapa.put("id", "3");
		System.out.println(select("ja", mapa));
		System.out.println(insert("ja", mapa));
		System.out.println(delete("ja", 4));
	}
}
