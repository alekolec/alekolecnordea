package com.alekolec.dbStudentJDBC.model;

import java.util.ArrayList;
import java.util.List;

public class Student {
	private int id;
	private String name;
	private String lastname;
	private List<Double> grades;

	public Student() {
	}

	public Student(int id, String name, String lastname) {
		super();
		this.id = id;
		this.name = name;
		this.lastname = lastname;
		this.grades = new ArrayList<>();
	}

	public void setGrades(List<Double> grades) {
		this.grades = grades;
	}

	public List<Double> getGrades() {
		return grades;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", lastname=" + lastname + "]";
	}

}
