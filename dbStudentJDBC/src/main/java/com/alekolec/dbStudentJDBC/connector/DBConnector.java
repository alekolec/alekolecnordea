package com.alekolec.dbStudentJDBC.connector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnector {
	private String dbhost = "localhost";
	private String dbname = "student";
	private String dbuser = "postgres";
	private String dbpass = "admin";
	private int dbport = 5432;
	private static DBConnector _instance = null;
	private Connection _connection = null;

	private DBConnector() {

	}

	public static Connection getConnection() {
		if (_instance == null) {
			_instance = new DBConnector();
			try {
				String url = "jdbc:postgresql://" + _instance.dbhost + ":" + _instance.dbport + "/" + _instance.dbname;
				_instance._connection = DriverManager.getConnection(url, _instance.dbuser, _instance.dbpass);
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}
		return _instance._connection;
	}
}