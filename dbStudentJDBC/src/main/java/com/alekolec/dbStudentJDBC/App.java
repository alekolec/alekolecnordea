package com.alekolec.dbStudentJDBC;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.alekolec.dbStudentJDBC.resolver.student.StudentResolver;

public class App {
	private static final int SHOW = 1;
	private static final int ADD = 2;
	private static final int DELETE = 3;
	private static final int UPDATE = 4;
	private static final int GRADE = 5;
	private static final int EXIT = 6;

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		StudentResolver resolver = new StudentResolver();

		showMenu();
		int option = Integer.parseInt(scanner.nextLine());

		while (option != EXIT) {
			switch (option) {
			case SHOW:
				System.out.println(resolver.get());
				break;

			case ADD:
				Map<String, String> params = new HashMap<>();
				System.out.println("Podaj imie i nazwisko");
				String line = scanner.nextLine();
				String[] parse = line.split(" ");
				params.put("name", parse[0]);
				params.put("lastname", parse[1]);
				resolver.insertOrUpdate(params);
				break;

			case DELETE:
				System.out.println("Podaj id");
				int id = Integer.parseInt(scanner.nextLine());
				if (resolver.delete(id)) {
					System.out.println("Usuni�to studenta o id: " + id);
				} else {
					System.out.println("Nie ma takiego studenta!");
				}
				break;
			case UPDATE:
				Map<String, String> paramsUpdate = new HashMap<>();
				System.out.println("Podaj id, imi� i nazwisko");
				String lineUpdate = scanner.nextLine();
				String[] parseUpdate = lineUpdate.split(" ");
				paramsUpdate.put("id", parseUpdate[0]);
				paramsUpdate.put("name", parseUpdate[1]);
				paramsUpdate.put("lastname", parseUpdate[2]);
				if (resolver.insertOrUpdate(paramsUpdate)) {
					System.out.println("Update!");
				} else {
					System.out.println("Nie ma takiego studenta!");
				}

				break;
			case GRADE:
				break;
			case EXIT:
				System.out.println("Koniec!");
				System.exit(0);
			}
			showMenu();
			option = Integer.parseInt(scanner.nextLine());
		}
	}

	public static void showMenu() {
		System.out.println("1. Poka� rekordy");
		System.out.println("2. Dodaj rekord");
		System.out.println("3. Usu� rekord [id=]");
		System.out.println("4. Nadpisz rekord");
		System.out.println("5. Koniec");

	}
}
