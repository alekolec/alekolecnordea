package sda.nordea;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		UserConsole console = new UserConsole();

		while (true) {
			System.out.println("Podaj dzia�anie");
			String line = sc.nextLine();

			if (line.equals("quit")) {
				break;
			} else if (line.equals("add")) {
				System.out.println("Podaj ilo��");
				try {
				int amount = Integer.parseInt(sc.nextLine());
				console.add(amount);
				} catch (NumberFormatException nfe) {
					System.out.println("Niepoprawny format, przyjmuj� liczby ca�kowite");
				}
				continue;
			} else if (line.equals("printSpace")) {
				console.printSpace();
				continue;
			} else if (line.equals("clear")) {
				console.getList().clear();
				continue;
			} else if (line.equals("collect")) {
				Runtime.getRuntime().gc();
				continue;
			} else if (line.endsWith("recursive")) {
				try {
					int amount = Integer.parseInt(sc.nextLine());
					console.recurrentExecuteMethod(amount);;
					} catch (NumberFormatException nfe) {
						System.out.println("Niepoprawny format, przyjmuj� liczby ca�kowite");
					}
					continue;
			} else {
				System.out.println("Niepoprawna komenda");
				continue;
			}

		}

	}

}
