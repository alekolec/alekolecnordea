package pl.sda.training.tdd;



import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import javax.swing.text.DateFormatter;

public class AgeCalculator {
	
	/**
	 * Calculates age of person based on his day of birth and the given day
	 * @param birthDate - date of birth in given format: dd.MM.YYYU, e.g. 31.01.2010
	 * @param givenDay - day for which the age should be calculated in given format: dd.MM.YYYU, e.g. 31.01.2010
	 * @return calculated age, e.g. calculateAgeInGivenDay('02.01.2010','01.01.2017') -> will return 6
	 * @throws RunTimeException  when incorrect format was used OR givenDay<birthDate
	 */
	
	public int calculateAgeInGivenDay(String birthDate, String givenDay) {
		int age = 0;
		checkArguments(birthDate, givenDay);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
				
		LocalDate birthDateFormat = LocalDate.parse(birthDate, formatter);
		LocalDate givenDayFormat = LocalDate.parse(givenDay, formatter);
		
		if(birthDateFormat.getYear() == 0) {
			//birthDateFormat.getYear() 
		}
		
		//LocalDate difference = givenDayFormat.minusYears(birthDateFormat.getYear());
		
		return Period.between(birthDateFormat, givenDayFormat).getYears();
		
	}

	private void checkArguments(String birthDate, String givenDay) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
		
		LocalDate birthDateFormat = LocalDate.parse(birthDate, formatter);
		LocalDate givenDayFormat = LocalDate.parse(givenDay, formatter);
		if(givenDayFormat.isBefore(birthDateFormat)) {
			throw new RuntimeException("Sth went wrong!");
		}
		
		
	}

}
