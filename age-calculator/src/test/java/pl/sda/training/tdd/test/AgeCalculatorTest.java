package pl.sda.training.tdd.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pl.sda.training.tdd.AgeCalculator;

public class AgeCalculatorTest {

	private AgeCalculator calc;

	@Before
	public void setup() {
		calc = new AgeCalculator();

	}
	
	@Test(expected=RuntimeException.class)
	public void incorrectFormatOfGivenDateShouldReturnException() {
		calc.calculateAgeInGivenDay("01.01.2017", "10/01/2017");
	}
	
	@Test(expected=RuntimeException.class)
	public void incorrectFormatOfBirthDateShouldReturnException() {
	calc.calculateAgeInGivenDay("01/01/2017", "10.01.2017");
	
}
	
	@Test(expected=RuntimeException.class)
	public void birthDateGreaterThanGivenDayShouldReturnException() {
		calc.calculateAgeInGivenDay("10.05.2004", "10.03.2000");
	}
	
	@Test
	public void personShouldhave21Years() {
		Assert.assertEquals(21, calc.calculateAgeInGivenDay("01.01.1800", "02.01.1821"));
	}
	
	@Test
	public void personShouldhave20Years() {
		Assert.assertEquals(20, calc.calculateAgeInGivenDay("01.01.1800", "31.12.1820"));
	}
	
	@Test
	public void personShouldhave10Years() {
		Assert.assertEquals(10, calc.calculateAgeInGivenDay("01.01.0000", "01.01.0010"));
	}
	
	@Test
	public void personShouldhave0Years() {
		Assert.assertEquals(0, calc.calculateAgeInGivenDay("08.08.2004", "08.08.2004"));
	}
	
	@Test
	public void personShouldhave8Years() {
		Assert.assertEquals(8, calc.calculateAgeInGivenDay("31.12.1999", "01.01.2008"));
	}
}

