<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

	<head>
		<title>Recipebook</title>
		<meta charset="UTF-8">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</head>
	<body>


	<div class="container">
			<nav class="navbar">
				<a href="#" class="navbar-brand">RECIPEBOOK</a>
				<ul class="nav navbar-nav">
		
		<li><a href="./index.jsp">Przeglądaj przepisy</a></li>
		<c:if test="${sessionScope.loggedin}">
		<li><a href="./admin.jsp">Dodaj produkt</a></li>
		<li><a href="./admin.jsp">Dodaj przepis</a></li>
		<li><a href="./LoginServlet?action=logout">Wyloguj</a></li>
		</c:if>
		<c:if test="${sessionScope.loggedin ne true}">
		<li><a href="./login.jsp">Zaloguj</a></li>
		</c:if>
	</ul>
</nav>
</div>
<c:if test="${sessionScope.loggedin}">
	UWAGA JESTEM ZALOGOWANY!
</c:if>