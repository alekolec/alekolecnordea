package com.alekolec.recipebook.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;


@Entity
public class Product {
	@Id
	@GeneratedValue
	private long id;
	private String name;
	@ManyToOne
	private Recipe recipe;
	

}
