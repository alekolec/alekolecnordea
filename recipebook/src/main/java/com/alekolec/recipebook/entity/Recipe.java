package com.alekolec.recipebook.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Recipe {

	@Id
	@GeneratedValue
	private long id;
	private String name;
	@OneToMany
	private List<Product> product;
	private String preparation;
	

}
