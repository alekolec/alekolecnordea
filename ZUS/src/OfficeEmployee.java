
public class OfficeEmployee {
	private String imie;
	private String nazwisko;

	public OfficeEmployee(String imie, String nazwisko) {
		this.imie = imie;
		this.nazwisko = nazwisko;
	}

	public void registerClient(Client klientDoRejestracji) {
		if (klientDoRejestracji == null) {
			System.out.println("Kolejka pusta");
			return;
		}
		if (!ZUSRegistry.checkIfRegistered(klientDoRejestracji.getPesel())) {
			ZUSRegistry.register(klientDoRejestracji);
			System.out.println("Zarejestrowa�em klienta!");

		} else {
			System.out.println("Klient by� zarejestrowany");
		}
	}

	public void checkClientStatus(Client client) {
		ZUSRegistry.checkIfRegistered(client.getPesel());
	}

	public void handle(Client client) {
		if (client != null) {
			if (client.getConcern() == ClientConcern.REGISTER) {
				registerClient(client);
			}
			if (client.getConcern() == ClientConcern.CHECK_REGISTRATION) {
				checkClientStatus(client);
			}
		}
	}
}