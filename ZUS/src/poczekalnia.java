

public class poczekalnia {

	public static void main(String[] args) {
		
		WaitingRoom poczekalnia = new WaitingRoom();
		
		
		
		poczekalnia.addClient(new Client("Jan","Kowalski","646456456456", ClientConcern.REGISTER));
		poczekalnia.addClient(new Client("Stefan","Batory","383737373", ClientConcern.CHECK_REGISTRATION));
		poczekalnia.addClient(new Client("Adam","Dziki","1212121212", ClientConcern.REGISTER));
		poczekalnia.addClient(new Client("Leszek","Nowak","343434343434", ClientConcern.CHECK_REGISTRATION));
		poczekalnia.addClient(new Client("Zbyszek","Sowa","55555555555", ClientConcern.REGISTER));
		poczekalnia.addClient(new Client("Janusz","Lew","333333333333", ClientConcern.CHECK_REGISTRATION));
		
		
		OfficeEmployee pracownik = new OfficeEmployee("Ewa", "Szybka");
		
		pracownik.handle(poczekalnia.handleClient());
		pracownik.handle(poczekalnia.handleClient());
		pracownik.handle(poczekalnia.handleClient());
		pracownik.handle(poczekalnia.handleClient());
		pracownik.handle(poczekalnia.handleClient());
		pracownik.handle(poczekalnia.handleClient());
		pracownik.handle(poczekalnia.handleClient());
		pracownik.handle(poczekalnia.handleClient());
		
		ZUSRegistry.showRegistered();

		

	
		
		
	}

}
