
public class Client {
private String imie;
private String nazwisko;
private String pesel;
private ClientConcern concern;

public Client(String imie, String nazwisko, String pesel, ClientConcern concern) {
	this.imie = imie;
	this.nazwisko = nazwisko;
	this.pesel = pesel;
	this.concern = concern;
}

public String getImie() {
	return imie;
}

public String getNazwisko() {
	return nazwisko;
}

public String getPesel() {
	return pesel;
}

public ClientConcern getConcern() {
	return concern;
}

@Override
	public String toString() {
		
		return "Imie: " + imie + " Nazwisko: " + nazwisko + " Pesel: " + pesel + " Concern: " + concern;
	}
	
	
}
