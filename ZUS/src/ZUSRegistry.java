import java.util.Collection;
import java.util.HashMap;




public class ZUSRegistry {
	
	private static HashMap<String, Client> rejestrOsob = new HashMap<String, Client>();
	
	
	
	public static void register(Client klientDoZarejestrowania) {
		rejestrOsob.put(klientDoZarejestrowania.getPesel(), klientDoZarejestrowania);
		
	}
	
	public static boolean checkIfRegistered(String pesel) {
		if(rejestrOsob.containsKey(pesel)) {
			System.out.println("Klient o peselu: " + pesel + " istnieje");
			return true;
		} 
		System.out.println("Klient o peselu: " + pesel + " nie istnieje");
		return false;
	}
	
	public static Collection<Client> showRegistered() {
			
	return rejestrOsob.values();
		
	}

}

