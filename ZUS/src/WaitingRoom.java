import java.util.LinkedList;


public class WaitingRoom {
	
	private LinkedList<Client> lista = new LinkedList<Client>();
	

	private static int nrKlienta = 0;
	public void addClient(Client nowyClient) {
		lista.add(nowyClient);
		nrKlienta +=1;
		System.out.println("Oczekujacych: " + lista.size());
		System.out.println("Nr klienta: " + nrKlienta);
	}
	
	public Client handleClient() {
		if (!lista.isEmpty()) {
			Client obslugiwany = lista.getFirst();
			System.out.println("Obsluguje klienta: " + obslugiwany.getImie());
			lista.removeFirst();
			
			return obslugiwany;
			
		}
		return null;
	}
	
	

}

