
public class IloczynLiczb {

	public static void main(String[] args) {
		int [] array = {1,2,3,4};
		int multiply = 1;
		
		iloczyn(array, multiply);

	}

	private static void iloczyn(int[] array, int multiply) {
		for(int liczba : array) {
			multiply = multiply * liczba;
		}
		
		System.out.println(multiply);
	}

}
