package pl.sda.tdd.test;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import pl.sda.tdd.PrimeNumber;

@RunWith(Parameterized.class)

public class PrimeNumberParametrizedTest {
	
	private int numberTested;
	private boolean shouldBePrimeNumber;
	
	public PrimeNumberParametrizedTest(int numberTested, boolean shouldBePrimeNumber) {
		this.numberTested = numberTested;
		this.shouldBePrimeNumber = shouldBePrimeNumber;
	}
	
	@Test
	public void testPrimeNumber(){
		Assert.assertEquals(shouldBePrimeNumber, PrimeNumber.isPrimeNumber(numberTested));
	}
	
	
	@Parameters(name = "{index} should {0} be prime number? {1}")
	public static Collection<Object[]> dataProvider(){
		return Arrays.asList(new Object[][]{
			{1, false},
			{4, false},
			{2, true},
			{7, true},
			{-13, false},
			{12503, true},
			{373, true},
			{1000, false},
			{0, false}
					});
	}
	
}
