package pl.sda.tdd.test;

import org.junit.Assert;
import org.junit.Test;

import pl.sda.tdd.PrimeNumber;

public class PrimeNumberTest {

	@Test
	public void fourIsNotPrimeNumber() {
		Assert.assertFalse(PrimeNumber.isPrimeNumber(4));
	}
	
	@Test
	public void zeroIsNotPrimeNumber() {
		Assert.assertFalse(PrimeNumber.isPrimeNumber(0));
	}
	
	@Test
	public void oneIsNotPrimeNumber() {
		Assert.assertFalse(PrimeNumber.isPrimeNumber(1));
	}
	
	@Test
	public void twoIsPrimeNumber() {
		Assert.assertTrue(PrimeNumber.isPrimeNumber(2));
	}
	
	@Test
	public void tenThousandIsPrimeNumber() {
		Assert.assertFalse(PrimeNumber.isPrimeNumber(10000));
	}
	
	@Test
	public void minusThirteenIsNotPrimeNumber() {
		Assert.assertFalse(PrimeNumber.isPrimeNumber(-13));
	}
	
	@Test
	public void threeHundredSeventyThreeIsPrimeNumber() {
		Assert.assertTrue(PrimeNumber.isPrimeNumber(373));
	}
	
	@Test
	public void bigNumberIsPrimeNumber() {
		Assert.assertTrue(PrimeNumber.isPrimeNumber(12503));
	}
	
	@Test
	public void veeeeryBigPrimeNumber() {
		Assert.assertTrue(PrimeNumber.isPrimeNumber(1800000011));
	}
	
	
}
