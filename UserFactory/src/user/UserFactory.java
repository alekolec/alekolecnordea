package user;

import java.util.HashMap;
import java.util.Map;

public class UserFactory {
	private Map<String, User> prototypes = new HashMap<>();

	public void addPrototype(User user) {
		prototypes.put(user.getKind(), user);
	}

	public User create(String nickname, String password, String kind) {
			
			try {
				User prototype = prototypes.get(kind);
				User user = prototype.clone();
				user.setNickname(nickname);
				user.setPassword(password);
				return user;
				
			} catch (CloneNotSupportedException e) {
					e.printStackTrace();
			}

			return null;
		}
	}
	
	
	

