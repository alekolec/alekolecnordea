package test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import builder.AbstractPC;
import builder.COMPUTER_BRAND;


public class AbstractPCTest {
	private  AbstractPC.Builder builder;

	@Before
	public void prepare() {
	builder = new AbstractPC.Builder();
	builder.setMotherboardName("costam").setRamCount(4.5).setOverclocked(false);
	}

	@Test
	public void testAsus() {
		builder.setBrand(COMPUTER_BRAND.ASUS).setName("Asus1");
		AbstractPC asus1 = builder.create();

		Assert.assertEquals("Asus1", asus1.getM_name());
		Assert.assertEquals("costam", asus1.getM_motherboardName());
		Assert.assertEquals(4.5, asus1.getM_RamCount(),0);
		Assert.assertEquals(false, asus1.isM_isOverclocked());
	}

	@Test
	public void testHP() {
		builder.setBrand(COMPUTER_BRAND.HP).setName("HP1").setRamCount(5.6);
		AbstractPC hp1 = builder.create();

		Assert.assertEquals("HP1", hp1.getM_name());
		Assert.assertEquals(COMPUTER_BRAND.HP, hp1.getM_brand());
		Assert.assertEquals("costam", hp1.getM_motherboardName());
		Assert.assertEquals(5.6, hp1.getM_RamCount(),0);
		Assert.assertEquals(false, hp1.isM_isOverclocked());
	}

	@Test
	public void testApple() {
		builder.setBrand(COMPUTER_BRAND.APPLE).setName("Apple1");
		AbstractPC apple1 = builder.create();

		Assert.assertEquals("Apple1", apple1.getM_name());
		Assert.assertEquals(COMPUTER_BRAND.APPLE, apple1.getM_brand());
		Assert.assertEquals("costam", apple1.getM_motherboardName());
		Assert.assertEquals(4.5, apple1.getM_RamCount(),0);
		Assert.assertEquals(false, apple1.isM_isOverclocked());
	}

	@Test
	public void testSamsung() {
		builder.setBrand(COMPUTER_BRAND.SAMSUNG).setName("Samsung1").setGpu_power(0.45);
		AbstractPC samsung1 = builder.create();

		Assert.assertEquals("Samsung1", samsung1.getM_name());
		Assert.assertEquals(COMPUTER_BRAND.SAMSUNG, samsung1.getM_brand());
		Assert.assertEquals("costam", samsung1.getM_motherboardName());
		Assert.assertEquals(4.5, samsung1.getM_RamCount(),0);
		Assert.assertEquals(0.45, samsung1.getM_gpu_power(),0);
		Assert.assertEquals(false, samsung1.isM_isOverclocked());
	}

}
