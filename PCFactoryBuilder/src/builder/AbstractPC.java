package builder;

public class AbstractPC {
	private String m_name;
	private COMPUTER_BRAND m_brand;
	private int m_cpu_power;
	private double m_gpu_power;
	private boolean m_isOverclocked;
	private String m_motherboardName;
	private int m_hddCount;
	private double m_RamCount;

	public AbstractPC(String m_name, COMPUTER_BRAND m_brand, int m_cpu_power, double m_gpu_power,
			boolean m_isOverclocked, String m_motherboardName, int m_hddCount, double m_RamCount) {
		super();
		this.m_name = m_name;
		this.m_brand = m_brand;
		this.m_cpu_power = m_cpu_power;
		this.m_gpu_power = m_gpu_power;
		this.m_isOverclocked = m_isOverclocked;
		this.m_motherboardName = m_motherboardName;
		this.m_hddCount = m_hddCount;
		this.m_RamCount = m_RamCount;
	}

	public String getM_name() {
		return m_name;
	}

	public COMPUTER_BRAND getM_brand() {
		return m_brand;
	}

	public int getM_cpu_power() {
		return m_cpu_power;
	}

	public double getM_gpu_power() {
		return m_gpu_power;
	}

	public boolean isM_isOverclocked() {
		return m_isOverclocked;
	}

	public String getM_motherboardName() {
		return m_motherboardName;
	}

	public int getM_hddCount() {
		return m_hddCount;
	}

	public double getM_RamCount() {
		return m_RamCount;
	}
	
	private AbstractPC(Builder builder) {
		m_name = builder.name;
		m_brand = builder.brand;
		m_cpu_power = builder.cpu_power;
		m_gpu_power = builder.gpu_power;
		m_isOverclocked = builder.isOverclocked;
		m_motherboardName = builder.motherboardName;
		m_hddCount = builder.hddCount;
		m_RamCount = builder.RamCount;
	}
	
	

	@Override
	public String toString() {
		return "AbstractPC [m_name=" + m_name + ", m_brand=" + m_brand + ", m_cpu_power=" + m_cpu_power
				+ ", m_gpu_power=" + m_gpu_power + ", m_isOverclocked=" + m_isOverclocked + ", m_motherboardName="
				+ m_motherboardName + ", m_hddCount=" + m_hddCount + ", m_RamCount=" + m_RamCount + "]";
	}



	public static class Builder {

		private String name;
		private COMPUTER_BRAND brand;
		private int cpu_power;
		private double gpu_power;
		private boolean isOverclocked;
		private String motherboardName;
		private int hddCount;
		private double RamCount;

		public Builder setName(String name) {
			this.name = name;
			return this;
		}

		public Builder setBrand(COMPUTER_BRAND brand) {
			this.brand = brand;
			return this;
		}

		public Builder setCpu_power(int cpu_power) {
			this.cpu_power = cpu_power;
			return this;
		}

		public Builder setGpu_power(double gpu_power) {
			this.gpu_power = gpu_power;
			return this;
		}

		public Builder setOverclocked(boolean isOverclocked) {
			this.isOverclocked = isOverclocked;
			return this;
		}

		public Builder setMotherboardName(String motherboardName) {
			this.motherboardName = motherboardName;
			return this;
		}

		public Builder setHddCount(int hddCount) {
			this.hddCount = hddCount;
			return this;
		}

		public Builder setRamCount(double ramCount) {
			RamCount = ramCount;
			return this;
		}
		
		public AbstractPC create() {
			return new AbstractPC(this);
		}

	}

}
