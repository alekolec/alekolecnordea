package sportowiec;

public interface Sportsman {

	void prepare();

	void doPumps(int number);

	void doSquats(int number);

	void doCrunches(int number);

}
