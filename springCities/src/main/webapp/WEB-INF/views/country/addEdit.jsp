<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<c:import url="../header.jsp" />

<c:if test="${msg ne null}">
			<p>${msg}</p>
		</c:if>
		
		<form action='<c:url value="" />' method="POST">
	
	<div class="input-group">
	  <span class="input-group-addon" id="basic-addon1">Nazwa</span>
	  <input type="text" name="name" value="${country.name}" class="form-control"  aria-describedby="basic-addon1">
	</div>
		
	<button type="submit" class="btn btn-primary" style="background-image: linear-gradient(to bottom,#6d6060 0,#151212 100%)">Dodaj</button>
	
</form>
		
		
<c:import url="../footer.jsp" />