<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<c:import url="../header.jsp" />
	
<div class="well">
<c:if test="${attractions ne null}">
<table class="table">
	<thead>
		<tr>
			<th>#</th>
			<th>Nazwa</th>
			</tr>
	</thead>
	<tbody>
		<c:forEach items="${attractions}" var="attraction">
			<tr>
				 <td>${attraction.id}</td>
				 <td>${attraction.name}</td>
				 <td><a href='<c:url value="/attraction/edit/${attraction.id}" />'class="btn btn-primary" style="background-image: linear-gradient(to bottom,#6d6060 0,#151212 100%)">Edytuj</a>&nbsp;<a href='<c:url value="/attraction/delete/${attraction.id}" />' class="btn btn-primary" style="background-image: linear-gradient(to bottom,#6d6060 0,#151212 100%)">Usuń</a>
			</tr>	 
		</c:forEach>
	</tbody>
	</table>
	</c:if>
</div>
<c:import url="../footer.jsp" />