<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<c:import url="../header.jsp" />

<c:choose>
	<c:when test="${countries.size() == 0}">
		<div class="alert alert-danger">
			<strong>Błąd</strong>
			Musisz najpierw dodać Państwo, aby móc dodać miasto.
		</div>
	</c:when>
	<c:otherwise>

<c:if test="${msg ne null}">
			<p>${msg}</p>
		</c:if>
		
		<form action='<c:url value="" />' method="POST">
	
	<div class="input-group">
	  <span class="input-group-addon" id="basic-addon1">Nazwa</span>
	  <input type="text" name="name" value="${city.name}" class="form-control"  aria-describedby="basic-addon1">
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon" id="basic-addon1">Kod pocztowy</span>
	  <input type="text" name="zip" value="${city.zip}"class="form-control"  aria-describedby="basic-addon1">
	</div>
	
	
	<div class="input-group">
	  <span class="input-group-addon" id="basic-addon1">Populacja</span>
	  <input type="text" name="population" value="${city.population}" class="form-control"  aria-describedby="basic-addon1">
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon" id="basic-addon1">Opis</span>
	  <input type="text" name="description"  value="${city.description}" class="form-control"  aria-describedby="basic-addon1">
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon" id="basic-addon1">Prezydent</span>
	  <input type="text" name="president"  value="${city.cityDescription.president}" class="form-control"  aria-describedby="basic-addon1">
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon" id="basic-addon1">Powierzchnia</span>
	  <input type="text" name="area"  value="${city.cityDescription.area}" class="form-control"  aria-describedby="basic-addon1">
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon" id="basic-addon1">Rok</span>
	  <input type="text" name="year"  value="${city.cityDescription.year}" class="form-control"  aria-describedby="basic-addon1">
	</div>
	
	<div class="input-group">
	  <span class="input-group-addon" id="basic-addon1">Średnie zarobki</span>
	  <input type="text" name="averageSalary"  value="${city.cityDescription.averageSalary}" class="form-control"  aria-describedby="basic-addon1">
	</div>
	
	<select name="country">
		<!--option selected>${countryOfCity.name}</option-->
		<c:forEach items="${countries}" var="country">
			<c:choose>
			<c:when test="${country.id eq countryOfCity.id}">
				<c:set value="selected" var="isSelected" />
			</c:when>
			<c:otherwise>
				<c:set value="" var="isSelected" />
			</c:otherwise>
			</c:choose>
			<option value="${country.id}" ${isSelected}>${country.name}</option>
		</c:forEach>
	</select>
	
	<c:if test="${attractions.size() > 0}">
		<label>Atrakcje w mieście</label>
		<c:forEach items="${attractions}" var="attraction">
		<div class="checkbox">
	    		<label>
	    			<c:forEach items="${city.attractions}" var="cityAttraction">
	    			<c:choose>
	    			<c:when test="${cityAttraction.id eq attraction.id}">
	    			<c:set var="checked" value="checked='checked'"/>
	    			</c:when>
	    			<c:otherwise>
	    			<c:set var="checked" value=""/>
	    			</c:otherwise>
	    			</c:choose>
	    			</c:forEach>
				<input type="checkbox" name="attractions" value="${attraction.id}" ${checked}/>${attraction.name}
			</label>
	  	</div>
	  	</c:forEach>
	  	</c:if>
	

	
	<button type="submit" class="btn btn-primary" style="background-image: linear-gradient(to bottom,#6d6060 0,#151212 100%)">Dodaj</button>
	
		</form>
	</c:otherwise>
</c:choose>
		
		
<c:import url="../footer.jsp" />