<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<c:import url="../header.jsp" />
	
<div class="well">
<c:if test="${cities ne null}">
<table class="table">
	<thead>
		<tr>
			<th>#</th>
			<th>Nazwa</th>
			<th>Kod pocztowy</th>
			<th>Populacja</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${cities}" var="city">
			<tr>
				 <td>${city.id}</td>
				 <td><a href='<c:url value="/city/show/${city.id}" />' style="color: grey">${city.name}</a></td>
				 <td>${city.zip}</td>
				 <td>${city.population}</td>
				 <td><a href='<c:url value="/city/edit/${city.id}" />'class="btn btn-primary" style="background-image: linear-gradient(to bottom,#6d6060 0,#151212 100%)">Edytuj</a>&nbsp;<a href='<c:url value="/city/delete/${city.id}" />' class="btn btn-primary" style="background-image: linear-gradient(to bottom,#6d6060 0,#151212 100%)">Usuń</a>
			</tr>	 
		</c:forEach>
	</tbody>
	</table>
	</c:if>
</div>
<c:import url="../footer.jsp" />