<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<c:import url="../header.jsp" />
		<div class="jumbotron">
  <h1>${city.name}</h1>
  <p>Kod pocztowy: ${city.zip}</p>
  <p>Populacja: ${city.population}</p>
  <p>Opis: ${city.description}</p>
  
  
  
  <p><a class="btn btn-primary btn-lg" style="background-image: linear-gradient(to bottom,#6d6060 0,#151212 100%)" href='<c:url value="/city/all" />' role="button">Wróć</a></p>
</div>

		
	
<c:import url="../footer.jsp" />