<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Witaj na mojej stronie</title>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		
		<!-- jQuery library -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<style>
		.input-group-addon, .input-group-btn {
		min-width: 110px;
		}
		.btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .open>.dropdown-toggle.btn-primary {
		background-color: #000000 !important;
    	color: white;
		}
		.nav > li > a:hover{
   		 background-color:#000000;
		}
		</style>
	</head>
	<body>
	<div class="container">
			<nav class="navbar navbar-inverse">
				<a href="#" class="navbar-brand">Miasta widokowe</a>
				<ul class="nav navbar-nav">
				<li><a href='<c:url value="/" />'>Strona główna</a></li>
				<li><a href='<c:url value="/city/add" />'>Dodaj miasto</a></li>
				<li><a href='<c:url value="/city/all" />'>Wyświetl miasta</a></li>
				<li><a href='<c:url value="/image/add" />'>Dodaj obrazek</a></li>
				<li><a href='<c:url value="/image/all" />'>Wyświetl obrazki</a></li>
				<li><a href='<c:url value="/country/add" />'>Dodaj kraj</a></li>
				<li><a href='<c:url value="/country/all" />'>Wyświetl kraje</a></li>
				<li><a href='<c:url value="/attraction/add" />'>Dodaj atrakcje</a></li>
				
				</ul>
			</nav>
			