<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false"%>  
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
<c:import url="../header.jsp" />
		<div class="jumbotron">
  <h3>${image.name}</h3>
  <h4>${image.city.name}</h4>
 <img src="${image.url}" class="img img-responsive img-rounded">
 
  <hr>
  <p><a class="btn btn-primary btn-lg" style="background-image: linear-gradient(to bottom,#6d6060 0,#151212 100%)" href='<c:url value="/image/all" />' role="button">Wróć</a></p>
</div>

		
	
<c:import url="../footer.jsp" />