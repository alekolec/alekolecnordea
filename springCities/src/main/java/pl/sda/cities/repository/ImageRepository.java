package pl.sda.cities.repository;

import org.springframework.data.repository.CrudRepository;

import pl.sda.cities.entity.Image;

public interface ImageRepository extends CrudRepository<Image, Long>{

}
