package pl.sda.cities.repository;

import org.springframework.data.repository.CrudRepository;

import pl.sda.cities.entity.Attraction;

public interface AttractionRepository extends CrudRepository<Attraction, Long>{

}
