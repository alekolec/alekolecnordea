package pl.sda.cities.repository;

import org.springframework.data.repository.CrudRepository;

import pl.sda.cities.entity.Country;

public interface CountryRepository extends CrudRepository<Country, Long>{

}
