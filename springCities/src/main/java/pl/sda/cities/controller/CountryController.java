package pl.sda.cities.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.sda.cities.entity.Country;
import pl.sda.cities.repository.CountryRepository;

@Controller
@RequestMapping("/country")
public class CountryController {
	
	@Autowired
	CountryRepository countryRepository;
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public String showAll(ModelMap model) {
		List<Country> listOfCountries = (List<Country>) countryRepository.findAll();
		model.addAttribute("countries", listOfCountries);
		return "country/all";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String viewAdd(ModelMap model) {
		return "country/addEdit";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public void addCountry(	@RequestParam("name") String name,
							ModelMap model, HttpServletResponse response, HttpServletRequest req) throws IOException {
		Country country = new Country(name);
		countryRepository.save(country);
		String msg = "Dodano państwo!";
		model.addAttribute("country", country);
		model.addAttribute("msg", msg);
		response.sendRedirect(req.getContextPath() + "/country/all");
	}
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String showEdit(@PathVariable("id") long id, ModelMap model) {
		Country country = countryRepository.findOne(id);
		model.addAttribute("country", country);
		return "country/addEdit";
	}
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public void editCountry(@PathVariable("id") long id,
							  @RequestParam("name") String name,
							ModelMap model, HttpServletResponse response, HttpServletRequest req) throws IOException {
		Country country = countryRepository.findOne(id);
		country.setName(name);
		countryRepository.save(country);
		String msg = "Edycja przebiegła z powodzeniem!";
		model.addAttribute("country", country);
		model.addAttribute("msg", msg);
		response.sendRedirect(req.getContextPath() + "/country/all");
	}
	
	@RequestMapping("/delete/{id}")
	public void delete(@PathVariable("id") long id, ModelMap model, HttpServletResponse response, HttpServletRequest req) throws IOException {
		countryRepository.delete(id);
		model.addAttribute("msg", "Pomyślnie usunięto miasto");
		response.sendRedirect(req.getContextPath() + "/country/all");
	
	}

	
	
}
