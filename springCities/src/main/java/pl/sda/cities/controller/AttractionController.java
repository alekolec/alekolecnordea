package pl.sda.cities.controller;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.sda.cities.entity.Attraction;
import pl.sda.cities.entity.City;
import pl.sda.cities.entity.CityDescription;
import pl.sda.cities.entity.Country;
import pl.sda.cities.repository.AttractionRepository;

@Controller
@RequestMapping("/attraction")
public class AttractionController {
	
	@Autowired
	AttractionRepository attractionRepository;
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public String showAll(ModelMap model) {
		model.addAttribute("attractions", attractionRepository.findAll());
		return "attraction/all";
	}
	
	@RequestMapping(value = "/show/{id}", method = RequestMethod.GET)
	public String showSingle(@PathVariable("id") long id, ModelMap model) {
		Attraction attraction = attractionRepository.findOne(id);
		model.addAttribute("attraction", attraction);
		return "attraction/show";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addAttractionView(ModelMap model) {
		return "attraction/addEdit";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public void addAttraction(	@RequestParam("name") String name,
								ModelMap model, HttpServletResponse response) throws IOException {
		Attraction attraction = new Attraction(name);
		
			
		attractionRepository.save(attraction);
		String msg = "Dodano miasto!";
		model.addAttribute("attraction", attraction);
		model.addAttribute("msg", msg);
		response.sendRedirect("/springCities/attraction/all");
	}
	
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String showEdit(@PathVariable("id") long id, ModelMap model) {
		Attraction attraction = attractionRepository.findOne(id);
		model.addAttribute("attraction", attraction);
		return "attraction/addEdit";
	}
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public String editAttraction(@PathVariable("id") long id,
							@RequestParam("name") String name,
							ModelMap model) {
		
		Attraction attraction = attractionRepository.findOne(id);
		attraction.setName(name);

		attractionRepository.save(attraction);
		
		String msg = "Edycja przebiegła z powodzeniem!";
		model.addAttribute("attraction", attraction);
		model.addAttribute("msg", msg);
		return "city/addEdit";
	}
	
	
	@RequestMapping("/delete/{id}")
	public void delete(@PathVariable("id") long id, ModelMap model, HttpServletResponse response) throws IOException {
		attractionRepository.delete(id);
		model.addAttribute("msg", "Pomyślnie usunięto miasto");
		response.sendRedirect("/springCities/attraction/all");
	
	}

	
}
