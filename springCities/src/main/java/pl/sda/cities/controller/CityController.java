package pl.sda.cities.controller;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.sda.cities.entity.Attraction;
import pl.sda.cities.entity.City;
import pl.sda.cities.entity.CityDescription;
import pl.sda.cities.entity.Country;
import pl.sda.cities.repository.AttractionRepository;
import pl.sda.cities.repository.CityRepository;
import pl.sda.cities.repository.CountryRepository;

@Controller
@RequestMapping("/city")
public class CityController {
	
	@Autowired
	CityRepository cityRepository;
	
	@Autowired
	CountryRepository countryRepository;
	
	@Autowired
	AttractionRepository attractionRepository;
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public String showAll(ModelMap model) {
		List<City> listOfCities = (List<City>) cityRepository.findAll();
		model.addAttribute("cities", listOfCities);
		return "city/all";
	}
	
	@RequestMapping(value = "/show/{id}", method = RequestMethod.GET)
	public String showSingle(@PathVariable("id") long id, ModelMap model) {
		City city = cityRepository.findOne(id);
		model.addAttribute("city", city);
		return "city/show";
	}
	
		
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addCity(ModelMap model) {
		List<Country> listOfCountries = (List<Country>) countryRepository.findAll();
		model.addAttribute("countries", listOfCountries);
		model.addAttribute("attractions", attractionRepository.findAll());
		return "city/addEdit";
	}
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String showEdit(@PathVariable("id") long id, ModelMap model) {
		City city = cityRepository.findOne(id);
		Country country = city.getCountry();
		List<Country> listOfCountries = (List<Country>) countryRepository.findAll();
		model.addAttribute("countries", listOfCountries);
		model.addAttribute("countryOfCity", country);
		model.addAttribute("city", city);
		model.addAttribute("attractions", attractionRepository.findAll());
		return "city/addEdit";
	}
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public String editCity(@PathVariable("id") long id,
							@RequestParam("name") String name,
							@RequestParam("zip") String zip,
							@RequestParam("population") float population,
							@RequestParam("description") String description,
							@RequestParam("country") long countryId,
							@RequestParam("president") String president,
							@RequestParam("area") float area,
							@RequestParam("year") int year,
							@RequestParam("averageSalary") double averageSalary,
							@RequestParam(value = "attractions", required = false) Long[] attractions,
							ModelMap model) {
		
		City city = cityRepository.findOne(id);
		Country actualCountry = city.getCountry();
		List<City> listOfCities = actualCountry.getCities();
		for(City c : listOfCities) {
			if(c.getId() == city.getId()) {
				listOfCities.remove(c);
				break;
			}
		}
				
		countryRepository.save(actualCountry);

		city.setName(name);
		city.setZip(zip);
		city.setPopulation(population);
		city.setDescription(description);

		Country newCountry = countryRepository.findOne(countryId);
		newCountry.getCities().add(city);
		city.setCountry(newCountry);
	
		CityDescription cityDescription = city.getCityDescription();
		if(cityDescription == null) {
			cityDescription = new CityDescription();
		}
		cityDescription.setPresident(president);
		cityDescription.setArea(area);
		cityDescription.setYear(year);
		cityDescription.setAverageSalary(averageSalary);
		cityDescription.setCity(city);
		
		city.setCityDescription(cityDescription);
		
		Set<Attraction> setOfAttractions = new HashSet<>();
		if(attractions != null) {
			for(long attractionId: attractions) {
				Attraction attemp = attractionRepository.findOne(attractionId);
				attemp.getCities().add(city);
				setOfAttractions.add(attemp);
				attractionRepository.save(attemp);
											
			}
		}
		
		
		city.setAttractions(setOfAttractions);
		
		countryRepository.save(actualCountry);
		

		String msg = "Edycja przebiegła z powodzeniem!";
		model.addAttribute("city", city);
		model.addAttribute("countries", countryRepository.findAll());
		model.addAttribute("countryOfCity", city.getCountry());
		model.addAttribute("msg", msg);
		return "city/addEdit";
	}
	
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public void addCity(	@RequestParam("name") String name,
							@RequestParam("zip") String zip,
							@RequestParam("population") float population,
							@RequestParam("description") String description,
							@RequestParam("country") long countryId,
							@RequestParam("president") String president,
							@RequestParam("area") float area,
							@RequestParam("year") int year,
							@RequestParam("averageSalary") double averageSalary,
							@RequestParam(value = "attractions", required = false) Long[] attractions,
							ModelMap model, HttpServletResponse response) throws IOException {
		City city = new City(name,zip,population,description, countryRepository.findOne(countryId));
		CityDescription cityDescription = new CityDescription(president, area, year, averageSalary,city);
		city.setCityDescription(cityDescription);
		
		Set<Attraction> setOfAttractions = new HashSet<>();
		if(attractions != null) {
			for(long attractionId: attractions) {
				Attraction attemp = attractionRepository.findOne(attractionId);
				attemp.getCities().add(city);
				setOfAttractions.add(attemp);
			}
		}
		
		city.setAttractions(setOfAttractions);
		
		countryRepository.save(countryRepository.findOne(countryId));
		String msg = "Dodano miasto!";
		model.addAttribute("city", city);
		model.addAttribute("msg", msg);
		response.sendRedirect("/springCities/city/all");
	}
	
	@RequestMapping("/delete/{id}")
	public void delete(@PathVariable("id") long id, ModelMap model, HttpServletResponse response) throws IOException {
		cityRepository.delete(id);
		model.addAttribute("msg", "Pomyślnie usunięto miasto");
		response.sendRedirect("/springCities/city/all");
	
	}
	
	
	
}
