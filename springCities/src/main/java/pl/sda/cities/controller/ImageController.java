package pl.sda.cities.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.sda.cities.entity.City;
import pl.sda.cities.entity.Image;
import pl.sda.cities.repository.CityRepository;
import pl.sda.cities.repository.ImageRepository;

@Controller
@RequestMapping("/image")
public class ImageController {
	
	@Autowired
	ImageRepository imageRepository;
	
	@Autowired
	CityRepository cityRepository;
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addImageView(ModelMap model) {
		List<City> listOfCities = (List<City>) cityRepository.findAll();
		model.addAttribute("cities", listOfCities);
		return "image/add";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public void addImage(@RequestParam("name") String name,
						   @RequestParam("url") String url,
						   @RequestParam("city") long cityId,
			      			ModelMap model, HttpServletResponse response) throws IOException {
		Image image = new Image(name,url, cityRepository.findOne(cityId));
		imageRepository.save(image);
		model.addAttribute("image", image);
		response.sendRedirect("/springCities/image/all");

	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public void delete(@PathVariable("id") long id, ModelMap model, HttpServletRequest req, HttpServletResponse response) throws IOException {
		Image image = imageRepository.findOne(id);
		City city = image.getCity();
		List<Image> listOfImages = city.getImages();
		listOfImages.remove(image);
		cityRepository.save(city);		
		imageRepository.delete(image);
		model.addAttribute("msg", "Pomyślnie usunięto obrazek");
		response.sendRedirect(req.getContextPath() + "/image/all");
	
	}
	
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public String showAll(ModelMap model) {
		List<Image> listOfImages = (List<Image>) imageRepository.findAll();
		model.addAttribute("images", listOfImages);
		return "image/all";
	}
	
	@RequestMapping(value = "/show/{id}", method = RequestMethod.GET)
	public String showSingle(@PathVariable("id") long id, ModelMap model) {
		Image image = imageRepository.findOne(id);
		model.addAttribute("image", image);
		return "image/show";
	}
	

}
