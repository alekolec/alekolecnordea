package pl.sda.cities.entity;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class City {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String name;
	private String zip;
	private float population;
	private String description;
	@OneToMany (fetch = FetchType.EAGER, mappedBy = "city", cascade = CascadeType.ALL)
	private List<Image> images;
	@ManyToOne (cascade = CascadeType.MERGE)
	private Country country;
	@OneToOne(mappedBy = "city", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private CityDescription cityDescription = new CityDescription();
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Set<Attraction> attractions;
	

	public City() {}
	
	public City(String name, String zip, float population, String description, Country country, CityDescription cityDescription, Set<Attraction> attractions) {
		this.name = name;
		this.zip = zip;
		this.population = population;
		this.description = description;
		this.country = country;
		this.cityDescription = cityDescription;
		this.attractions = attractions;
		
	}
	
	public City(String name, String zip, float population, String description, Country country) {
		this.name = name;
		this.zip = zip;
		this.population = population;
		this.description = description;
		this.country = country;
				
	}


	public City(long id, String name, String zip, float population, String description, List<Image> images) {
		super();
		this.id = id;
		this.name = name;
		this.zip = zip;
		this.population = population;
		this.description = description;
		this.images = images;
	}

	
	public City(String name, String zip, float population, String description) {
		super();
		this.name = name;
		this.zip = zip;
		this.population = population;
		this.description = description;
	}


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public float getPopulation() {
		return population;
	}

	public void setPopulation(float population) {
		this.population = population;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public List<Image> getImages() {
		return images;
	}
	
	public void setImages(List<Image> images) {
		this.images = images;
	}
	
	public Country getCountry() {
		return country;
	}
	
	public void setCountry(Country country) {
		this.country = country;
	}
	
	public CityDescription getCityDescription() {
		return cityDescription;
	}
	
	public void setCityDescription(CityDescription cityDescription) {
		this.cityDescription = cityDescription;
	}
	
	public Set<Attraction> getAttractions() {
		return attractions;
	}
	
	public void setAttractions(Set<Attraction> attractions) {
		this.attractions = attractions;
	}
	

}
