package pl.sda.cities.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Attraction {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String name;
	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "attractions", cascade = CascadeType.MERGE)
	private Set<City> cities;
	
	public Attraction() {}

	public Attraction(long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public Attraction(String name) {
			this.name = name;
	}

	public Attraction(String name, Set<City> cities) {
		super();
		this.name = name;
		this.cities = cities;
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Set<City> getCities() {
		return cities;
	}
	
	public void setCities(Set<City> cities) {
		this.cities = cities;
	}
	
	

}
