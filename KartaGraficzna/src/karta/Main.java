package karta;

public class Main {

	public static void main(String[] args) {

		GraphicsStream streamFullHD = GraphicsStreamFactory.createFullHD();
		GraphicsStream streamHDReady = GraphicsStreamFactory.createHDReady();
		GraphicsStream streamSVGA = GraphicsStreamFactory.createSVGA();

		GraphicsStream zlyStream = new GraphicsStream(RESOLUTION.R600x400, COLORS.B8, FORMAT.F16x10);
		
		GraphicsCard karta1 = new GraphicsCard(zlyStream, 67000000);
		
		System.out.println(karta1.checkStream());
		
		karta1.assignStream(streamHDReady);
		System.out.println(karta1.checkStream());
	}

}
