package karta;

public class GraphicsStream {

	private RESOLUTION resolution;
	private COLORS colors;
	private FORMAT format;
	
	
	public RESOLUTION getResolution() {
		return resolution;
	}
	public void setResolution(RESOLUTION resolution) {
		this.resolution = resolution;
	}
	public COLORS getColors() {
		return colors;
	}
	public void setColors(COLORS colors) {
		this.colors = colors;
	}
	public FORMAT getFormat() {
		return format;
	}
	public void setFormat(FORMAT format) {
		this.format = format;
	}
	
	public GraphicsStream(RESOLUTION resolution, COLORS colors, FORMAT format) {
		this.resolution = resolution;
		this.colors = colors;
		this.format = format;
	}
	

	
}
