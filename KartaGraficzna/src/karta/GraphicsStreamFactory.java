package karta;

public abstract class GraphicsStreamFactory {
	
	public static GraphicsStream createFullHD() {
		return new GraphicsStream(RESOLUTION.R1920x1080, COLORS.B32, FORMAT.F16x9);
	}
	
	public static GraphicsStream createHDReady() {
		return new GraphicsStream(RESOLUTION.R1366x768, COLORS.B16, FORMAT.F16x9);
	}
	
	public static GraphicsStream createSVGA() {
		return new GraphicsStream(RESOLUTION.R800x600, COLORS.B16, FORMAT.F16x9);
	}
	
	public static GraphicsStream createDziwny() {
		return new GraphicsStream(RESOLUTION.R600x400, COLORS.MONO, FORMAT.F16x9);
	}
	
	public static GraphicsStream createWQXGA() {
		return new GraphicsStream(RESOLUTION.R2560x1440, COLORS.B32, FORMAT.F16x9);
	}

}
