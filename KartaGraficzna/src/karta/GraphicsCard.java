package karta;

public class GraphicsCard {
	private GraphicsStream stream;
	private int processingPower;
	int wymaganaMoc;

	public GraphicsCard(GraphicsStream stream, int processingPower) {
		this.stream = stream;
		this.processingPower = processingPower;
		wymaganaMoc = stream.getResolution().getWidth() * stream.getResolution().getHeight() * stream.getColors().getBits();

	}
	
	public int getProcessingPower() {
		return processingPower;
	}
	
	public GraphicsStream getStream() {
		return stream;
	}

	public void assignStream(GraphicsStream stream) {
		this.stream = stream;
	}

	public boolean checkStream() {
		
		double fractionResolution = (double) stream.getResolution().getWidth() / stream.getResolution().getHeight();
		double fractionFormat = (double) stream.getFormat().getWidth() / stream.getFormat().getHeight();

		boolean properFormat = fractionResolution == fractionFormat;

		if (processingPower >= wymaganaMoc) {
			if (properFormat) {
				return true;
			} else if (fractionFormat > fractionResolution) {
				System.out.println("Wi�kszy format, ale ok");
				return true;
			} else {
				return false;
			}
		}
		return false;

	}
	
	public CABLE whichCable() {
		if(wymaganaMoc > 16785408) {
			return CABLE.HDMI;
		} else if(wymaganaMoc > 7680000 ) {
			return CABLE.DVI;
		} else return CABLE.VGA;
		}
	
	
}
