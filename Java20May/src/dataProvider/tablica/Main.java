package dataProvider.tablica;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		DataProvider variable = new ConsoleDataProvider();

		int rozmiarTablicy = variable.nextInt("rozmiar tablicy");

		int[] tablica = new int[rozmiarTablicy];
		
		
		int sum = 0;
		for (int i = 0; i < tablica.length; i++) {
			tablica[i] = variable.nextInt("element");
			sum +=tablica[i];
		}		
		
		
		System.out.println(sum);

	}

}
