package dataProvider.tablica;

public interface DataProvider {
	
	int nextInt(String name);
	String nextString(String name);

}
