package dataProvider.tablica;

import java.util.Scanner;

public class ConsoleDataProvider implements DataProvider {
	
	@Override
	public int nextInt(String name) {
		System.out.println("Podaj " + name + ": ");
		Scanner sc = new Scanner(System.in);
		
		return sc.nextInt(); //metoda ze scannera kt�ra zwraca kolejn� liczb� z wej�cia z konsoli
	}
	
	@Override
	public String nextString(String name) {
		System.out.println("Podaj " + name + ": ");
		Scanner sc = new Scanner(System.in);
		
		return sc.next(); // zwraca kolejne s�owo z konsoli
	}

}
