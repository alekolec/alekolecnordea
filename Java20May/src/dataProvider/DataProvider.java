package dataProvider;

public interface DataProvider {
	
	int nextInt(String name);
	String nextString(String name);

}
