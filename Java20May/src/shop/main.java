package shop;

import java.util.Scanner;

public class main {

	public static void main(String[] args) {
		Shop shop = new Shop();
		Scanner scanner = new Scanner(System.in);
		
				
		shop.addProduct(new Product("Mleko", 3.4, Category.FOOD ));
		shop.addProduct(new Product("Chleb", 1.9, Category.FOOD ));
		shop.addProduct(new Product("Piwo", 2.4, Category.ALCOHOL ));
		shop.addProduct(new Product("Telefon", 109, Category.ELECTRONICS ));
		shop.addProduct(new Product("Sukienka", 294, Category.CLOTHES ));
		
		
		System.out.println("Wybierz produkt");
		String cat = scanner.next();
		shop.showProduct(Category.valueOf(cat));

	}
	
	

}
