package shop;

public class Product {
	private String name;
	private double cena;
	private Category category;
	
	public Product(String name, double cena, Category category) {
		this.name = name;
		this.cena = cena;
		this.category = category;
		
	}
	
	public Category getCategory() {
		return category;
	}
	
	public double getCena() {
		return cena;
	}
	
	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		
		return "Produkt{nazwa: " + name + " cena: " + cena +  "kategoria: " + category;
	}

}
