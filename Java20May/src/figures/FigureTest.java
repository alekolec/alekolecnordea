package figures;

public class FigureTest {

	public static void main(String[] args) {
		Circle circle = new Circle(2);
		Square square = new Square(2);
		Triangle triangle = new Triangle(3,3,3);
		
		System.out.println(circle.countArea());
		System.out.println(square.countArea());
		System.out.println(triangle.countArea());
		
		
		System.out.println(circle.countCircumference());
		System.out.println(square.countCircumference());
		System.out.println(triangle.countCircumference());
		
		

	}

}
