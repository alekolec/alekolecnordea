package listy;

import java.util.ArrayList;
import java.util.List;

import dataProvider.tablica.ConsoleDataProvider;
import dataProvider.tablica.DataProvider;

public class Main {

	public static void main(String[] args) {
		DataProvider variable = new ConsoleDataProvider();

		int rozmiarTablicy = variable.nextInt("rozmiar tablicy");

		List<Double> l = new ArrayList<Double>();

		for (int i = 0; i < rozmiarTablicy; i++) {

			l.add((double) variable.nextInt("element"));

		}

		liczSume(l);
		liczSume2(l);
		liczIloczyn(l);
		liczIloczyn2(l);
		liczSrednia(l);
		liczSrednia2(l);
	}

	public static double liczSume(List<Double> l) {
		double sum = 0;
		for (int i = 0; i < l.size(); i++) {
			sum += l.get(i);
		}
		System.out.println(sum);
		return sum;
	}
	
	public static void liczSume2(List<Double> l) {
		double sum = 0;
		for(double i : l) {
			sum += i;
		}
		System.out.println(sum);
	}

	public static void liczIloczyn(List<Double> l) {
		double iloczyn = 1;
		for (int i = 0; i < l.size(); i++) {
			iloczyn *= l.get(i);
		}
		System.out.println(iloczyn);

	}
	
	public static void liczIloczyn2(List<Double> l) {
		double iloczyn = 1;
		for( double i : l) {
			iloczyn *= i;
		}
		System.out.println(iloczyn);
	}

	public static void liczSrednia(List<Double> l) {
		double srednia = 0;
		double sum = 0;
		for (int i = 0; i < l.size(); i++) {
			sum += l.get(i);
		}
		srednia = sum / l.size();
		System.out.println(srednia);

	}
	
	public static void liczSrednia2(List<Double> l) {
		double srednia = 0;
		double sum = 0; 
		for(double i : l) {
			sum += i;
			
		}
		srednia = sum / l.size();
		System.out.println(srednia);
	}

}
