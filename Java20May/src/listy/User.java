package listy;

import java.util.ArrayList;
import java.util.List;

public class User {
	private String name;
	private String password;
	
	public User(String name, String password) {
		this.name = name;
		this.password = password;
	}
	
	public String getName() {
		return name;
	}
	
	public String getPassword() {
		return password;
	}
	
	public String toString() {
		return "User={login=" + name+", password="+password+"}";
	}

	public static void main(String[] args) {
	
	List<User> userList = new ArrayList<User>();
	
	
	
	
	userList.add(new User("Jan", "sasada"));
	userList.add(new User("Stefan", "ssssss"));
	userList.add(new User("Adam", "afsafded"));
	userList.add(new User("Ilona", "dewd3d32d"));
	userList.add(null);
	
	
	for(User user : userList) {
		System.out.println(user);
	}
	}
	
	
	
	

}
