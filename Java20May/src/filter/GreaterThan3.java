package filter;

import dataProvider.tablica.*;


public class GreaterThan3 implements DataFilter {

	@Override
	public boolean isOK(int number) {
		if (number > 3)
			return true;
		return false;
	}
	
	public static void main(String[] args) {
		DataProvider variable = new ConsoleDataProvider();
		GreaterThan3 filtr = new GreaterThan3();

		int rozmiarTablicy = variable.nextInt("rozmiar tablicy");

		int[] tablica = new int[rozmiarTablicy];
		
			
		for (int i = 0; i < tablica.length; i++) {
			tablica[i] = variable.nextInt("element");
			
		}		
		
		for(int i = 0; i<rozmiarTablicy; i++) {
			if(filtr.isOK(tablica[i])) {
				System.out.println(tablica[i]);
			}
		}
		
		
			}
}
