package filter;

import dataProvider.tablica.ConsoleDataProvider;
import dataProvider.tablica.DataProvider;

public class Person  {
	private int age;
	private String imie;

	public Person(String imie, int age) {
		this.age = age;
		this.imie = imie;
	}

	public String getImie() {
		return imie;
	}

	public int getAge() {
		return age;
	}


}
