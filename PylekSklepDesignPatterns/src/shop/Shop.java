package shop;

import java.util.HashMap;
import java.util.Map;

public class Shop {
	
	private Map<String, Product> products = new HashMap<>();
	
	public void addProduct(Product product) {
		products.put(product.getName(), product);
		
	}
	
	public Product getProduct(String name) {
		return products.get(name);
	}

}
