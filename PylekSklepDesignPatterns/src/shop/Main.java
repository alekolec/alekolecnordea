package shop;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {
	public static void main(String[] args) {

		Shop shop = new Shop();

		shop.addProduct(new Product("Mas�o", 5, "Pyszniutkie"));
		shop.addProduct(new Product("Chleb", 2, "Oliwski"));
		shop.addProduct(new Product("Rower", 2000, "G�rski"));
		shop.addProduct(new Product("Samochod", 30000, "Audi"));

		Random r = new Random();
		List<Product> products = new ArrayList<>();
		String[] names = { "Mas�o", "Chleb", "Rower", "Samochod" };
		for (int i = 0; i < 1000; i++) {
			String name = names[r.nextInt(4)];  // losuj� element tablicy, 4 jest exclusive, czyli 0,1,2,3
			products.add(shop.getProduct(name)); //pobieram produkt ze sklepu i dodaj� do listy

		}

		for (Product p : products) {
			System.out.println(p.getName() + " - " + p.getPrice() + "z�");
		}

	}

}
