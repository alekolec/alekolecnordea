package students.database;

import java.io.Serializable;

public class Student implements Serializable{
	
	private long  indexNumber;
	private String name;
	private String surname;
	
	
	public long getIndexNumber() {
		return indexNumber;
	}
	
	public void setIndexNumber(long indexNumber) {
		this.indexNumber = indexNumber;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public Student() {
		
	}

	public Student(long indexNumber, String name, String surname) {
		this.indexNumber = indexNumber;
		this.name = name;
		this.surname = surname;
	}

	@Override
	public String toString() {
		return "Student [indexNumber=" + indexNumber + ", name=" + name + ", surname=" + surname + "]";
	}
	
	

}
