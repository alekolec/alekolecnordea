package students.database;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

public class DatabaseNativeSerializer implements DatabaseSerializer {
	
	
	@Override
	public void save(StudentDatabase database) throws FileNotFoundException, IOException {
		ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream("studenci.txt"));
		stream.writeObject(database);
		stream.close();
		
		
	}
	
	
	@Override
	public StudentDatabase load() throws FileNotFoundException, IOException, ClassNotFoundException {
		
		ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("studenci.txt"));
		StudentDatabase studenci = (StudentDatabase) inputStream.readObject();
		inputStream.close();
		
		for (Student student : studenci.showAll()) {
			System.out.println(student);
		}
		
		
		return studenci;
	}

}
