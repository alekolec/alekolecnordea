package students.database;

import java.io.FileNotFoundException;
import java.io.IOException;

public class StudentDatabaseTest {

	public static void main(String[] args) {
		StudentDatabase baza = new StudentDatabase();
		DatabaseSerializer serializator = new DatabaseOwnFormatSerializer();
		
		baza.add("Jan", "Kowalski");
		baza.add("Adam", "Nowak");
		
		try {
			serializator.save(baza);
		} catch (FileNotFoundException e) {
			System.out.println("brak pliku");
		} catch (IOException e) {
			
		}
		
		StudentDatabase loaded;
		
		try {
			loaded = serializator.load();
			for(Student student : loaded.showAll()) {
			System.out.println(student.getName());
		}
		} catch (FileNotFoundException e) {
			System.out.println("brak pliku");
		} catch (ClassNotFoundException e) {
			System.out.println("blad 1");
		} catch (IOException e) {
			System.out.println("blad 2");
		}
		
		

	}

}
