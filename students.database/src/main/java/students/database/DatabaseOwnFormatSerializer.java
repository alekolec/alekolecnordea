package students.database;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DatabaseOwnFormatSerializer implements DatabaseSerializer {

	@Override
	public void save(StudentDatabase database) throws FileNotFoundException, IOException {
		PrintStream out = new PrintStream("studenci-myformat.txt");

		for (Student student : database.getBazaStudentow().values()) {
			out.println(student.getName() + " " + student.getSurname() + " " + student.getIndexNumber());

		}
		out.close();
	}

	@Override
	public StudentDatabase load() throws FileNotFoundException, IOException, ClassNotFoundException {

		Scanner scanner = new Scanner(new File("studenci-myformat.txt"));
		
		List<Student> studenci = new ArrayList<Student>();
		
		while (scanner.hasNext()) {
			
			Student student = new Student();
			
			student.setName(scanner.next());
			student.setSurname(scanner.next());
			student.setIndexNumber(scanner.nextLong());
			
			studenci.add(student);
		}
		

		return new StudentDatabase(studenci);
	}

}
