package students.database;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StudentDatabase implements Serializable {
	
	Map <Long, Student> bazaStudentow = new HashMap <Long, Student> ();
	long indexNumber = 10001;
	
	public StudentDatabase() {}
	
	public StudentDatabase(List <Student> students) {
		long temp = 0;
		for (Student student : students) {
			if (student.getIndexNumber() > temp) {
				temp = student.getIndexNumber();
			}
			bazaStudentow.put(student.getIndexNumber(), student);
			
		} 
		indexNumber = temp + 1;
	}
	
	public void add(String name, String surname) {
		bazaStudentow.put(indexNumber, new Student(indexNumber, name, surname));
		indexNumber++;
		System.out.println("Dodaj� studenta o  nr " + (indexNumber -1));
	}
	
	public void remove(long index) {
		bazaStudentow.remove(index);
		System.out.println("Usuwam studenta o nr " + index);
	}
	
	public Student get(long index) {
		//System.out.println(bazaStudentow.get(index));
		Student student = bazaStudentow.get(index);
		if (student == null) {
			NoSuchStudentException e = new NoSuchStudentException(); //obiekt jak ka�dy inny
			throw e;
		}
		
		return student;
	}
	
	public List <Student> showAll() {
		return new ArrayList<>(bazaStudentow.values());
		}
	
	
	public Map<Long, Student> getBazaStudentow() {
		return bazaStudentow;
	}
	

}
