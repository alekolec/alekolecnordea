package students.database;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface DatabaseSerializer {

	void save(StudentDatabase database) throws FileNotFoundException, IOException;

	StudentDatabase load() throws FileNotFoundException, IOException, ClassNotFoundException;

}