package students.database;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class StudentsConsoleInterface {

	public static void main(String[] args) {
		System.out.println("Baza student�w");

		StudentDatabase baza = new StudentDatabase();
		DatabaseSerializer serializator = new DatabaseNativeSerializer();

		Scanner scanner = new Scanner(System.in);

		while (true) {
			System.out.println("Podaj dzia�anie");
			String line = scanner.next();

			if (line.equals("add")) {
				System.out.println("Podaj imi� i nazwisko");
				String name = scanner.next();
				String surname = scanner.next();
				baza.add(name, surname);

			} else if (line.equals("remove")) {
				System.out.println("Podaj indeks");
				long index = scanner.nextLong();
				if (baza.getBazaStudentow().containsKey(index)) {
					baza.remove(index);
				} else
					System.out.println("Nie ma takiego studenta");

			} else if (line.equals("get")) {
				System.out.println("Podaj indeks");
				long index = scanner.nextLong();
				if (baza.getBazaStudentow().containsKey(index)) {
					baza.get(index);
				} else
					System.out.println("Nie ma takiego studenta");
			} else if (line.equals("showAll")) {
				baza.showAll();
			} else if(line.equals("save")) {
				try {
					serializator.save(baza);
				} catch (Exception e) {
					System.out.println("Plik nie istnieje");
				}
			} else if(line.equals("load")) {
				try {
					serializator.load();
				} catch (FileNotFoundException e) {
					System.out.println("Plik nie istnieje");
				} catch (ClassNotFoundException e) {
					System.out.println("B��d");
				} catch (IOException e) {
					System.out.println("B��d");
				}
			} else {
			
				System.out.println("Podaj add, remove, show or showAll");
			}

		}

	}
}
