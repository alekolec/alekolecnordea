package students.database;

import org.junit.Test;

public class StudentDatabaseTests {

	@Test
	public void addTest() {
		StudentDatabase baza = new StudentDatabase();
		baza.add("Jan", "Kowalski");
		baza.add("Kamil", "Nowak");

		assert baza.showAll().size() == 2;
	}

	@Test
	public void getTest() {
		StudentDatabase baza = new StudentDatabase();
		baza.add("Jan", "Kowalski");
		baza.add("Kamil", "Nowak");

		assert baza.get(1).getName().equals("Kamil");
	}

	@Test
	public void removeTest() {
		StudentDatabase baza = new StudentDatabase();
		baza.add("Jan", "Kowalski");
		baza.add("Kamil", "Nowak");
		baza.remove(0);
		
		assert baza.showAll().size() == 1;
		
	}
	

}
