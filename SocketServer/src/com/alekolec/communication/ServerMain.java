package com.alekolec.communication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class ServerMain {

	public static void main(String[] args) {
		try {
			Scanner consoleStream = new Scanner(System.in);
			String replay = "";
			ServerSocket socketPolaczenia = new ServerSocket(10001);
			String otrzymana = "dummy";
			
			while (!otrzymana.equals("quit")) {
				Socket otwartySocket = socketPolaczenia.accept();
				InputStream kanalWejscia = otwartySocket.getInputStream();
				OutputStream kanalwyjscia = otwartySocket.getOutputStream();
				
				BufferedReader reader = new BufferedReader(new InputStreamReader(kanalWejscia));

				while (!otrzymana.equals("")) {
					otrzymana = reader.readLine();
					System.out.println(otrzymana);
					PrintWriter writer = new PrintWriter(kanalwyjscia);
					System.out.println("Odpisz na ta wiadomosc leniwcu !!");
					
					replay = consoleStream.nextLine();
					writer.println(replay);
					writer.flush();
					
					if(otrzymana.equals("")){
						System.out.println("TO KONIEC");
					}
				}
				reader.close();
				otwartySocket.close();
			}
			// reader.close();
			System.out.println("wyszedlem z while!");
			socketPolaczenia.close();

		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
			ioe.printStackTrace();
		}
	}

}
