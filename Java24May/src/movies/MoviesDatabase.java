package movies;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class MoviesDatabase {
	
	Map<String, Movie> filmy = new HashMap<String, Movie>();
	
	
	public void addMovie(Movie movie) {
		filmy.put(movie.getTitle(), movie);
	}
	
	public Movie getMovie(String title) {
		
		return filmy.get(title);
	}
	
	public void printALlMovies() {
		for (Entry<String, Movie> entry: filmy.entrySet()) {
			System.out.println(entry.getValue());
		}
	}
	
//	public void printAllMovies(MovieType type) {
//		for (Entry<String, Movie> entry: filmy.entrySet()) {
//			if (type == entry.getValue().getType()) {
//				System.out.println(entry.getValue());
//			}
//		}
//	}
	
	public void printAllMovies(MovieType type) {
		for( Movie movie : filmy.values()) {
			if (type == movie.getType()) {
				System.out.println(filmy.values());
		}
	}
	

}
}
