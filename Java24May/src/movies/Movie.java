package movies;

import java.util.Date;

public class Movie {
	private String title;
	private MovieType type;
	private Date date;
	private String surname;
	
	public Movie (String title, MovieType type, Date date, String surname) {
		this.title = title;
		this.type = type;
		this.date = date;
		this.surname = surname;
	}
	
	public String getTitle() {
		return title;
	}
	
	public MovieType getType() {
		return type;
	}
	
	public Date getDate() {
		return date;
	}
	
	public String getSurname() {
		return surname;
	}
	
	@Override
	public String toString() {
		return "Tytul: " + title + " Typ: " + type + " Data wydania: " + date + " Nazwisko re�ysera: " + surname;
	}
}
