package movies;

import java.util.Date;

public class Main {

	public static void main(String[] args) {
		
		MoviesDatabase bazaFilmow = new MoviesDatabase();
		
		bazaFilmow.addMovie(new Movie("Harry Potter", MovieType.ACTION, new Date(2000, 5, 12), "Columbus"));
		bazaFilmow.addMovie(new Movie("Czekolada", MovieType.DRAMA, new Date(1996, 6, 18), "Rezyser"));
		bazaFilmow.addMovie(new Movie("Dramat", MovieType.DRAMA, new Date(200, 10, 2), "Dramatyczny"));
		bazaFilmow.addMovie(new Movie("Najstraszniejszy", MovieType.HORROR, new Date(2009, 11, 8), "Najgro�niejszy"));
		bazaFilmow.addMovie(new Movie("Heheszki", MovieType.COMEDY, new Date(1999, 3, 3), "Pan Zabawny"));
		bazaFilmow.addMovie(new Movie("Ojej", MovieType.COMEDY, new Date(1956, 2, 1), "Ups"));
		
		
		System.out.println("Wydrukuj czekolade");
		System.out.println(bazaFilmow.getMovie("Czekolada"));
		
		System.out.println("Wydrukuj wszystkie");
		bazaFilmow.printALlMovies();
		
		System.out.println("Wydrukuj dramaty");
		bazaFilmow.printAllMovies(MovieType.DRAMA);
		
		

	}

}
