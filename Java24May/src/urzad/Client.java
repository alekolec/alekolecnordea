package urzad;

public class Client {
	private Issue issue;
	private String pesel;
	
	public Client(Issue issue, String pesel) {
		this.issue = issue;
		this.pesel = pesel;
	}
	
	public Issue getIssue() {
		return issue;
	}
	
	public String getPesel() {
		return pesel;
	}
	

}
