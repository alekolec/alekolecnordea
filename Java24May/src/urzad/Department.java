package urzad;

import java.util.ArrayList;
import java.util.List;

public class Department {

	List<Office> department = new ArrayList<Office>();
	
	public List<Office> getDepartment() {
		return department;
	}

	public void addOffice(Office office) {
		department.add(office);

	}
	
	public Office getOffice(int index) {
		
		return department.get(index);
}
	
	

}