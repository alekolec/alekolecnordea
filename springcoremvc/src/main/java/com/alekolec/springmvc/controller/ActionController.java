package com.alekolec.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/action")
public class ActionController {

	@RequestMapping("/add")
	public String add() {
		return "add";
	}
	
	@RequestMapping("/show")
	public String show() {
		return "show";
	}
	
}
