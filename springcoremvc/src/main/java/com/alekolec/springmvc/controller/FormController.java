package com.alekolec.springmvc.controller;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.alekolec.springmvc.model.LoginData;

@Controller
public class FormController {
	
	List<LoginData> listOfLoginData = new LinkedList<>();
	
	@RequestMapping("/init")
	public String init(ModelMap model) {
		if(listOfLoginData.isEmpty()) {
			listOfLoginData.add(new LoginData("admin", "admin"));
			listOfLoginData.add(new LoginData("tester","tester"));
		}
		model.addAttribute("listOfLoginData", listOfLoginData);
		return "logindata";
	}

	@RequestMapping(value = "/showform", method = RequestMethod.GET)
	public String showform() {
		return "form";
	}
	
	@RequestMapping(value = "/showform", method = RequestMethod.POST)
	public String postShowForm(@RequestParam("login") String login,
							   @RequestParam("password") String password,
							   ModelMap model) {
		listOfLoginData.add(new LoginData(login, password));
		model.addAttribute("login", login.toUpperCase());
		model.addAttribute("password", password.toUpperCase());
		return "form";
	}
}
