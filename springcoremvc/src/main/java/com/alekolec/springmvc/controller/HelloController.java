package com.alekolec.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/")
public class HelloController {
	
	@RequestMapping("/witam")
	public String welcome(ModelMap model) {
		String msg = "To jest informacja z kontrolera";
		model.addAttribute("message", msg);
		return "welcome";
		
	}
	
	@RequestMapping("/pokaz") 
	public ModelAndView pokaz() {
		String msg = "To jest informacja z metody model-and-view";
		return new ModelAndView("welcome", "message", msg);
		
	}
	
	@RequestMapping("/drukuj/{id}") 
	public ModelAndView print(@PathVariable("id") long id) {
		ModelAndView mav = new ModelAndView("welcome");
		mav.addObject("message", "Twój id to: " + id);
		
		return mav;
				
	}

}
