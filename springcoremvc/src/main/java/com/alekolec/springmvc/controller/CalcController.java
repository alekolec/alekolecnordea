package com.alekolec.springmvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/action")
public class CalcController {
	
	@RequestMapping("/dodaj/{liczbaA}/do/{liczbaB}")
	public ModelAndView add(@PathVariable("liczbaA") int liczbaA,
					  @PathVariable("liczbaB") int liczbaB) {
		
		ModelAndView mav = new ModelAndView("calc");
		int suma = liczbaA + liczbaB;
		mav.addObject("message", liczbaA + " + " + liczbaB + " = " + suma);
		
		return mav;
	}

}
