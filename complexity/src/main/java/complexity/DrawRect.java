package complexity;

public class DrawRect {

	public static void main(String[] args) {
		drawRect(3);
		drawRect2(3);
		System.out.println("");
		drawRect3(3);
		
	}
	
	//n * n + n = n^2 + n > T(n) = n^2 (drugiego stopnia, bierze si� najwi�kszy stopie�, to n nic nie zmienia

	public static void drawRect(int n) {

		for (int i = 0; i < n; i++) {

			for (int j = 0; j < n; j++) {
				System.out.print("*");
			}

			System.out.println("");
		}

	}
	
	//nadal n^2, bo stringi  -> "" + "a"; "a"+"a"; "aa" + "a" -> przepisujemy w k�ko do nowej tablicy
	public static void drawRect2(int n) {
		String str = "";
		for(int j = 0; j<n; j++) {
			str += "*";
			
		}
		
		for(int i = 0; i<n; i++) {
			System.out.println(str);
		}
	}
	
	//tu zlozonosc n
	public static void drawRect3(int n) {
		StringBuilder str = new StringBuilder();
		for(int j = 0; j<n; j++) {
			str.append("*");
			
		}
		String stars = str.toString();
		
		for(int i = 0; i<n; i++) {
			System.out.println(stars);
		}
	}

}
