
public class Metody {

	public static void main(String[] args) {
		dodajIWyswietl(10, 6);
		int a = dodaj(10, 6);
		System.out.println(a);

		pomnoz(2, 4);
		System.out.println(pomnoz(2, 4));

		System.out.println(najmniejsza(1, 100, 1344));

		int[] t1 = { 3, 4, 5, 6, 10, -5 };

		System.out.println(minTablica(t1));
		System.out.println(minTablica(new int[] {1,2,3,4,5}));

	}

	private static void dodajIWyswietl(int a, int b) {
		System.out.println(a + b);
	}

	private static int dodaj(int a, int b) {
		int wynik = a + b;
		return wynik;
	}

	private static int pomnoz(int a, int b) {
		int iloczyn = a * b;
		return iloczyn;
	}

	private static int mniejsza(int a, int b) {
		if (a > b) {
			return b;
		} else {
			return a;
		}
	}

	private static int najmniejsza(int a, int b, int c) {
		return mniejsza(a, mniejsza(b, c));

	}

	private static int minTablica(int[] t1) {
		if (t1 == null || t1.length == 0) {
			return Integer.MAX_VALUE;
		}

		int wynik = t1[0];
		for (int i = 1; i < t1.length; i++) {
			wynik = mniejsza(wynik, t1[i]);
		}
		return wynik;

	}
}