package family;

public class Family {

	public static void main(String[] args) {

		Mother mother = new Mother("Basia");
		Father father = new Father("Stasiu");
		Son son = new Son("Antoni");
		Daughter daughter = new Daughter("Celinka");

	
		introduce(mother);
		introduce(father);
		introduce(daughter);
		introduce(son);
	}

	public static void introduce(Mother czesia) {
		System.out.println("Jestem mama i mam na imie " + czesia.getName());
	}

	public static void introduce(Father father) {
		System.out.println("Jestem tata i mam na imie " + father.getName());
	}

	public static void introduce(Son son) {
		System.out.println("Jestem synem i mam na imie " + son.getName());
	}

	public static void introduce(Daughter daughter) {
		System.out.println("Jestem corka i mam na imie " + daughter.getName());
	}

}
