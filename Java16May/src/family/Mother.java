package family;

public class Mother {
	private String name;

	public Mother(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
