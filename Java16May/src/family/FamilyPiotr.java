package family;

public class FamilyPiotr {

	public static void main(String[] args) {
		Mother matka = new Mother("Matka Teresa");
		Father ojciec = new Father("Ojciec Pio");
		Son syn = new Son("Syn marnotrawny");
		Daughter corka = new Daughter("Corka tatusia");
		
		introduce(matka);
		introduce(ojciec);
		introduce(syn);
		introduce(corka);

	}
	
	public static void introduce(Mother obiekt){
		System.out.println("Hej, jestem matka, "+obiekt.getName());
	}
	
	public static void introduce(Father obiekt){
		System.out.println("Hej, jestem ojciec, "+obiekt.getName());
	}
	
	public static void introduce(Son obiekt){
		System.out.println("Hej, jestem syn, "+obiekt.getName());
	}
	
	public static void introduce(Daughter obiekt){
		System.out.println("Hej, jestem corka, "+obiekt.getName());
	}

}
