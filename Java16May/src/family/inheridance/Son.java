package family.inheridance;

public class Son extends FamilyMember {

	public Son(String name) {
		super(name, false); // wywo�uje konstruktor z klasy FamilyMember
	}

	public void introduce() {
		System.out.println("Jestem synem i mam na imie " + this.getName());
	}
}
