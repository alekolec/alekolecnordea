package family.inheridance;

public class Daughter extends FamilyMember {

	public Daughter(String name) {
		super(name, false); // wywo�uje konstruktor z klasy FamilyMember
	}
	
	public void introduce() {
		System.out.println("Jestem corka i mam na imie " + this.getName());
	}

}
