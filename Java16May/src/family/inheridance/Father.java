package family.inheridance;

//Ojciec jest cz�onkiem rodziny
public class Father extends FamilyMember {

	public Father(String name) {
		super(name, true); // wywo�uje konstruktor z klasy FamilyMember
	}

	public void introduce() {
		System.out.println("Jestem tata i mam na imie " + this.getName());
	}
}
