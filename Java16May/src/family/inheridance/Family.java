package family.inheridance;

public class Family {

	public static void main(String[] args) {

		Mother mother = new Mother("Basia");
		Father father = new Father("Stasiu");
		Son son = new Son("Antoni");
		Daughter daughter = new Daughter("Celinka");

		FamilyMember[] family = { father, mother, son, daughter };

		for (FamilyMember familyMember : family) {
			if (familyMember.isAdult()) {
				System.out.println("I'm an adult");
			} else {
				System.out.println("I'm not an adult");

			}
			familyMember.introduce();
		}

	}

}
