package family.inheridance;

public class Mother extends FamilyMember {

	public Mother(String name) { // parametry konstruktora Mother
		super(name,true); // parametry konstruktora FamilyMember
	}

	public void introduce() {
		System.out.println("Jestem mama i mam na imie " + this.getName());
	}
}
