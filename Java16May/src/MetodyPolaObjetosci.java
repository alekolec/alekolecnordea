
public class MetodyPolaObjetosci {

	public static void main(String[] args) {
		
//		System.out.println(poleKwadratu(2) + "(4)");
//		System.out.println(poleSzescianu(2) + "(24)");
//		System.out.println(poleKola(3));
//		System.out.println(objetoscWalca(2,1));
//		System.out.println(objetoscStozka(2,3));
//		System.out.println(objetoscSzescianu(3));
//		System.out.println(objetoscOstroslupa(2,3));

	}
	
	private static double poleKwadratu(double a) {
		return a * a;
	}
	
	private static double poleSzescianu(double a) {
		return 6 * poleKwadratu(a);
	}
	
//	private static double poleKola(double r) {
//		return Math.PI*r*r;
//	}
	
//	private static double objetoscWalca(double r, double h) {
//		return poleKola(r) * h;
//	}
	
//	private static double objetoscStozka(double r, double h) {
//		return poleKola(r) * h/3;
//	}
	
	private static double objetoscSzescianu(double a) {
		return poleKwadratu(a)*a;
	}

	private static double objetoscOstroslupa(double a, double h) {
		return poleKwadratu(a)*h/3;
	}
}
