
public class Przeciazanie {

	public static void main(String[] args) {
		show(1);
		show(1.0);
		show("Jan");
		

	}
	
	private static void show(int liczba) {
		System.out.println("int = " + liczba);
	}
	
	private static void show(double liczba) {
		System.out.println("double = " + liczba);
	}
	
	private static void show(String text) {
		System.out.println("String = " + text);
	}

}
