package osoba;

public class Osoba {
	private String imie;
	private int wiek;
	
	public Osoba() {
		
	}

	Osoba(String imie, int wiek) {
		this.imie = imie;
		this.wiek = wiek;
	}

	public String getImie() {
		return this.imie;
	}

	public int getWiek() {
		return this.wiek;
	}

	public void setImie(String imie) {
		this.imie = imie;
	}

	public void setWiek(int wiek) {
		this.wiek = wiek;
	}

	public void przedstawSie() {
		System.out.println("Jestem " + imie + " i mam " + wiek + " lat");
	}

}
