package osoba;

public class main {

	public static void main(String[] args) {
		Osoba jan = new Osoba("Jan", 24);
		Osoba stefan = new Osoba("Stefan", 55);
		Osoba adam = new Osoba("Adam", 7);
		
		new Osoba();

		jan.przedstawSie();
		stefan.przedstawSie();
		adam.przedstawSie();
	}

}
