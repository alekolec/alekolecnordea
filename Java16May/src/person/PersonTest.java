package person;

public class PersonTest {

	public static void main(String[] args) {
		
		Person [] osoby = new Person [3];
		osoby[0] = new Person("Jan", "Kowalski", 24);
		osoby[1] = new Person("Aleksandra", "Koleczka", 26);
		osoby[2] = new Person("Franciszek", "Nowak", 67);
		
		for(Person osoba : osoby) {
			System.out.println(osoba);
		}
		
		Person ola = new Person("Aleksanda", "Kepczynska", 26);
		System.out.println(ola.getName() + ola.getSurname() + ola.getAge());
		ola.setName("Katarzyna");
		System.out.println(ola.toString());

	}

}
