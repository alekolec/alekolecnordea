package teddybear;

public class MisiePrzykladObiektowosci {

	public static void main(String[] args) {
		TeddyBear uszatek = new TeddyBear("Uszatek");
		TeddyBear stefan = new TeddyBear("Stefan");
		TeddyBear koralgol = new TeddyBear("Koralgol");

		System.out.println(uszatek.getImie());
		System.out.println(stefan.getImie());
		System.out.println(koralgol.getImie());
		uszatek.setImie("nanana");
		System.out.println(uszatek.getImie());

		uszatek.przedstawSie();

	}

}
