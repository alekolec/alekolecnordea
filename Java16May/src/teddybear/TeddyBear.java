package teddybear;

public class TeddyBear {
	private String imie;
	
	public TeddyBear(String imie) {
		this.imie = imie;
	}
	
	public String getImie() {
		return this.imie;
	}
	
	public void setImie(String imie) {
		this.imie = imie;
	}

	public void przedstawSie() {
		System.out.println("Jestem misiem o imieniu " + this.imie);
	}
}
