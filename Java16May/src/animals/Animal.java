package animals;

public interface Animal {
	int a = 5;
	
	// metoda jest abstrakcyjna i publiczna z definicji
	//bo znajduje si� w interfejsie
	
	void makeNoise(); 

}
