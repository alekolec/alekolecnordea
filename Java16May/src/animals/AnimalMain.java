package animals;

public class AnimalMain {

	public static void main(String[] args) {
		
		Animal [] animals = {new Cat() };
		
		for (Animal animal : animals) {
			animal.makeNoise();
		}

	}

}
