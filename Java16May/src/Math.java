
public class Math {

	public static void main(String[] args) {
		System.out.println(max(3,5) + "(5)");
		System.out.println(max(3l,5l) + "(5)");
		System.out.println(max(3.0f,5.0f) + "(5)");
		System.out.println(max(3.0,5.0) + "(5)");
		
		System.out.println(min(3,5));
		System.out.println(min(3l,5l));
		System.out.println(min(3.0f,5.0f));
		System.out.println(min(3.0,5.0));
		
		System.out.println(abs(3));
		System.out.println(abs(3l));
		System.out.println(abs(3.0f));
		System.out.println(abs(3.0));

	}
	
	private static int max(int a, int b) {
		if (a > b) {
			return a;
		} else {
			return b;
		}
	}
	
	private static long max(long a, long b) {
		if (a > b) {
			return a;
		} else {
			return b;
		}
	}
	
	private static float max(float a, float b) {
		if (a > b) {
			return a;
		} else {
			return b;
		}
	}
	
	private static double max(double a, double b) {
		if (a > b) {
			return a;
		} else {
			return b;
		}
	}
	
	private static int min(int a, int b) {
		if (a < b) {
			return a;
		} else {
			return b;
		}
	}
	
	private static long min(long a, long b) {
		if (a < b) {
			return a;
		} else {
			return b;
		}
	}
	
	private static float min(float a, float b) {
		if (a < b) {
			return a;
		} else {
			return b;
		}
	}
	
	private static double min(double a, double b) {
		if (a < b) {
			return a;
		} else {
			return b;
		}
	}
	
	private static int abs(int a) {
		if (a>=0) {
			return a;
		} return a* (-1);
	}
	
	private static long abs(long a) {
		if (a>=0) {
			return a;
		} return a* (-1);
	}
	
	private static float abs(float a) {
		if (a>=0) {
			return a;
		} return a* (-1);
	}
	
	private static double abs(double a) {
		if (a>=0) {
			return a;
		} return a* (-1);
	}
	
	private static long pow(int a, int b) {
		long result = 1;
		
		for(int i=0; i<b; i++){
			result *=a;
		}
		return result;
		}
	
	private static long pow(long a, long b) {
		long result = 1;
		
		for(int i=0; i<b; i++){
			result *=a;
		}
		return result;
		}
	
	private static double pow(float a, float b) {
		long result = 1;
		
		for(int i=0; i<b; i++){
			result *=a;
		}
		return result;
		}
	
	private static double pow(double a, double b) {
		long result = 1;
		
		for(int i=0; i<b; i++){
			result *=a;
		}
		return result;
		}
	
	
	}
	



