package instruments;

public class Triangle extends Instruments{
	
	public Triangle(String wlasciciel) {
		super("dzyn", wlasciciel);
	}
	
	public void play() {
		System.out.println("Jestem trojkatem i gram " + this.getSound());
	}

}
