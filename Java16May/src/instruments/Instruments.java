package instruments;

public abstract class Instruments {
	private String sound;
	private String wlasciciel;
	
	public Instruments(String sound, String wlasciciel) {
		this.sound = sound;
		this.wlasciciel = wlasciciel;
	}
	
	public String getSound() {
		return sound;
	}
	
	public String getWlasciciel() {
		return wlasciciel;
	}
	
	public abstract void play();
	
	public void znajdzWlasciciela() {
		System.out.println("Nale�� do " + this.getWlasciciel());
	}

}
