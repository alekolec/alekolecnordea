package instruments;

public class InstrumentsPlay {

	public static void main(String[] args) {

		Piano piano = new Piano("Mozart");
		Piano piano1 = new Piano("Chopin", 88);
		Guitar guitar = new Guitar("Bednarek");
		Violin violin = new Violin("Skrzypaczka");
		Triangle triangle = new Triangle("Ktokolwiek");

		Instruments[] instruments = { piano, piano1, guitar, violin, triangle };

		for (Instruments instrument : instruments) {
			instrument.play();
			instrument.znajdzWlasciciela();
		}

	}

}
