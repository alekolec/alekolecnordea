<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:import url="header.jsp" />

<div class="well">

<table class="table">
	<thead>
		<tr>
			<th>#</th>
			<th>Imię</th>
			<th>Nazwisko</th>
			<th>Płeć</th>
			<th>Email</th>
			<th>Login</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${listOfUsers}" var="user">
			<tr>
				 <td>${user.id}</td>
				 <td><a href="./show/${user.id}" style="color: grey">${user.name}</a></td>
				 <td>${user.lastname}</td>
				 <td>${user.sex}</td>
				 <td>${user.email}</td>
				 <td>${user.login}</td>
				 <td><a href="./edit/${user.id}" class="btn btn-primary" style="background-image: linear-gradient(to bottom,#6d6060 0,#151212 100%)">Edytuj</a>&nbsp;<a href="./delete/${user.id}" class="btn btn-primary" style="background-image: linear-gradient(to bottom,#6d6060 0,#151212 100%)">Usuń</a>
			</tr>	 
		</c:forEach>
	</tbody>
	</table>
</div>


<c:import url="footer.jsp" />