<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:import url="header.jsp" />
<div class="well" style="min-height: 500px">
<h1>Witaj!</h1>

<h4>Jeśli chcesz zarejestrować użytkownika, kliknij "Zarejestruj". By przejrzeć wszystkich - "Pokaż wszystkich"</h4>
<!-- p>${message}</p -->
<hr>
<hr>

<img class="icon icons8-User" width="200" height="200" style="display: block; margin: auto" src="https://maxcdn.icons8.com/Share/icon/Dusk_Wired/Users//add_user_group_man_man1600.png" />
</div>
<c:import url="footer.jsp" />