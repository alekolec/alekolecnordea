<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:import url="header.jsp" />
<div class="well">
<div class ="container">
<form action="./register" method= "POST">

<div class="col-md-6">

<div class="row">
<div class="col-md-12">
	<div class="input-group"  style="margin-bottom: 5px">
	  	<span class="input-group-addon" id="basic-addon1">Id</span>
	  	<input type="text" name="id" class="form-control" aria-describedby="basic-addon1">
	</div>
</div>
</div>	
	

	<div class="input-group" style="margin-bottom: 5px">
	  <span class="input-group-addon" id="basic-addon1">Imię</span>
	  <input type="text" name="name" class="form-control" aria-describedby="basic-addon1">
	
</div>
	
	<div class="input-group" style="margin-bottom: 5px">
	  <span class="input-group-addon" id="basic-addon1">Nazwisko</span>
	  <input type="text" name="lastname" class="form-control" aria-describedby="basic-addon1">
	</div>
	
	<div class="input-group" style="margin-bottom: 5px">
	<span class="input-group-addon" id="basic-addon1">Płeć</span>

	<select class="selectpicker" name="sex" class="form-control">
  <option>MALE</option>
  <option>FEMALE</option>

</select>

	</div>
	
	<div class="input-group" style="margin-bottom: 5px">
	  <span class="input-group-addon" id="basic-addon1">Email</span>
	  <input type="text" name="email" class="form-control" aria-describedby="basic-addon1">
	</div>
	
	<div class="input-group" style="margin-bottom: 5px">
	  <span class="input-group-addon" id="basic-addon1">Login</span>
	  <input type="text" name="login" class="form-control" aria-describedby="basic-addon1">
	</div>
	<button type="submit" class="btn btn-primary" style="background-image: linear-gradient(to bottom,#6d6060 0,#151212 100%)">Zarejestruj</button>

</div>
<div class="col-md-6" style="min-height: 200px">
<img class="icon icons8-User" width="200" height="200" src="https://maxcdn.icons8.com/Share/icon/Users//user1600.png" />
</div>
	</form>
</div>
</div>
<c:import url="footer.jsp" />