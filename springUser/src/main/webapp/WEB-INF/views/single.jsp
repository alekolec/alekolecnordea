<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:import url="header.jsp" />
<div class="well" style="min-height: 300px">
<div class="col-md-4">
<c:if test = "${user.sex eq 'MALE'}">
<img class="icon icons8-User" width="200" height="200" src="https://maxcdn.icons8.com/Share/icon/Users//user1600.png" />
</c:if>
<c:if test = "${user.sex eq 'FEMALE'}">
<img class="icon icons8-User" width="200" height="200" src="http://icons.iconarchive.com/icons/iconsmind/outline/512/Female-icon.png" />
</c:if>
</div>
<div class="col-md-4">
<h3 class="card-title">${user.name} ${user.lastname} </h3>
<p> Płeć: ${user.sex} </p>
<p> Email: ${user.email} </p>
<p> Login: ${user.login} </p>
</div>
</div>
<c:import url="footer.jsp" />