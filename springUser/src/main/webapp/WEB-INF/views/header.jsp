<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

	<head>
		<title>Użytkownicy</title>
		<meta charset="UTF-8">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
		
		<!-- jQuery library -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<style>
		.input-group-addon, .input-group-btn {
		min-width: 100px;
		}
		.btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .open>.dropdown-toggle.btn-primary {
		background-color: #000000 !important;
    	color: white;
		}
		.nav > li > a:hover{
   		 background-color:#000000;
		}
		</style>
	</head>
	<body>
		<div class="container">
		<div class="page-header">
		<h1>ZAREJESTRUJ<small> użytkownika <img width="50" height="50" src="http://media.istockphoto.com/vectors/pen-icon-outlined-vector-id590163402" /></small></h1>
		</div>
			<nav class="navbar" style="background-image: linear-gradient(to bottom,#6d6060 0,#151212 100%)">
				<a href="<c:url value="/main" />" class="navbar-brand" style="color: white" >Użytkownicy</a>
				<ul class="nav navbar-nav">
				<li><a href="<c:url value="/main" />" style="color: white">Strona główna</a></li>
				<li><a href="<c:url value="/register" />"style="color: white">Zarejestruj</a></li>
				<li><a href="<c:url value="/random" />"style="color: white">Zarejestruj losowego</a></li>
				<li><a href="<c:url value="/all" />"style="color: white">Pokaż wszystkich</a></li>
				</ul>
			</nav>
			
		