package com.alekolec.springUser.util;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class HttpConnector {
	
	private static final String userAgent = "Ola/1.0";
	
	public static String get(String address) {
		String ret = "";
		try {
			URL url = new URL(address);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("User-Agent", userAgent);
			
			Scanner s = new Scanner(new InputStreamReader(connection.getInputStream()));
			
			while(s.hasNextLine()) {
				ret += s.nextLine();
			}
			
		}catch (IOException e) {
			System.out.println(e.getMessage());
			
		}
		return ret;
	}

}
