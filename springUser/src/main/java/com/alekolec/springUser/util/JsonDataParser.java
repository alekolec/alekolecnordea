package com.alekolec.springUser.util;

import java.util.LinkedList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.alekolec.springUser.model.User;
import com.alekolec.springUser.model.UserSex;

public class JsonDataParser {
	public static String get(String key, String json) {
		JSONParser jp = new JSONParser();
		String ret = "";
		try {
			JSONObject jo = (JSONObject) jp.parse(json);
			ret = (String) jo.get(key);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ret;
	}
	
	public static List<User> converToList(String json) {
		List<User> listOfUsers = new LinkedList<>();
		JSONParser jp = new JSONParser();
		JSONObject jo;
		try {
			jo = (JSONObject) jp.parse(json);
			JSONArray results = (JSONArray) jo.get("results");
			for(int i = 0; i < results.size(); i++) {
				jo = (JSONObject) results.get(i);
				JSONObject userDetails = (JSONObject) jo.get("name");
				String userName = userDetails.get("first").toString();
				String userLastName = userDetails.get("last").toString();
				String userGender =  jo.get("gender").toString().toUpperCase();
				JSONObject userUsername = (JSONObject) jo.get("login");
				String userLogin = userUsername.get("username").toString();				
				String userEmail = jo.get("email").toString();
				JSONObject userLocation = (JSONObject) jo.get("location");
				Long userId = Long.parseLong(userLocation.get("postcode").toString());
					
				listOfUsers.add(new User(userId,userName,userLastName,UserSex.valueOf(userGender), userEmail, userLogin));
			}
			
		} catch (ParseException e) {
			System.out.println(e.getMessage());
		}
		return listOfUsers;
	}

}
