package com.alekolec.springUser.controller;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.alekolec.springUser.model.User;
import com.alekolec.springUser.model.UserSex;
import com.alekolec.springUser.util.HttpConnector;
import com.alekolec.springUser.util.JsonDataParser;



@Controller
public class UserController {
	
	List<User> listOfUsers = new LinkedList<>();
	
	@RequestMapping(value = "/main", method = RequestMethod.GET)
	public String showMain() {
		return "main";
	}
	
	@RequestMapping(value = "/random", method = RequestMethod.GET)
	public String randomUser(ModelMap model) {
		return "randomUser";
	}
	
	@RequestMapping(value = "/random", method = RequestMethod.POST)
	public String randomUserList(@RequestParam("howMany") int howMany, ModelMap model) {
		String json = HttpConnector.get("https://randomuser.me/api?results="+howMany);
		List <User> randomUsers = JsonDataParser.converToList(json);
		listOfUsers.addAll(randomUsers);
		return "main";
	}
	
	
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String register() {
		return "register";
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String registerUser(@RequestParam("id") long id,
							   @RequestParam("name") String name,
							   @RequestParam("lastname") String lastname,
							   @RequestParam("sex") String sexString,
							   @RequestParam("email") String email,
							   @RequestParam("login") String login,
							   ModelMap model) {
		UserSex sex = UserSex.valueOf(sexString);
		listOfUsers.add(new User(id,name,lastname,sex,email,login));
		model.addAttribute("id", id);
		model.addAttribute("name", name);
		model.addAttribute("lastname", lastname);
		model.addAttribute("sex", sex);
		model.addAttribute("email", email);
		model.addAttribute("login", login);
		
		return "register";
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public void delete(@PathVariable("id") long id, HttpServletResponse response) throws IOException {
		for(User user : listOfUsers) {
			if(user.getId() == id) {
				listOfUsers.remove(user);
				break;
			}
		}
		
		response.sendRedirect("/springUser/all");
		//return "main";
	}
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String edit(@PathVariable("id") long id, ModelMap model) {
		for(User user : listOfUsers) {
			if(user.getId() == id) {
				model.addAttribute("user", user);
				break;
			}
		}
		
		return "edit";
	}
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public void editPost(@PathVariable("id") long id, 
						    @RequestParam("name") String name,
						    @RequestParam("lastname") String lastname,
							@RequestParam("sex") String sexString,
							@RequestParam("email") String email,
							@RequestParam("login") String login,
						ModelMap model, HttpServletResponse response) throws IOException {
		for(User user : listOfUsers) {
			if(user.getId() == id) {
				user.setName(name);
				user.setLastname(lastname);
				user.setSex(UserSex.valueOf(sexString));
				user.setEmail(email);
				user.setLogin(login);
				model.addAttribute("user", user);
				break;
			}
		}
		response.sendRedirect("/springUser/all");
		//return "all";
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public String showAll(ModelMap model) {
		model.addAttribute("listOfUsers", listOfUsers);
			
		return "all";
	}
	
	@RequestMapping(value = "/show/{id}", method = RequestMethod.GET)
	public String show(@PathVariable("id") long id, ModelMap model) {
		for(User user : listOfUsers) {
			if(user.getId() == id) {
				model.addAttribute("user", user);
				break;
			}
		}
			
		return "single";
	}

}
