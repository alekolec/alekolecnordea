package com.alekolec.springUser.model;

public class User {
	private long id;
	private String name;
	private String lastname;
	private UserSex sex;
	private String email;
	private String login;
	
	
	public User(){}


	public User(long id, String name, String lastname, UserSex sex, String email, String login) {
		super();
		this.id = id;
		this.name = name;
		this.lastname = lastname;
		this.sex = sex;
		this.email = email;
		this.login = login;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getLastname() {
		return lastname;
	}


	public void setLastname(String lastname) {
		this.lastname = lastname;
	}


	public UserSex getSex() {
		return sex;
	}


	public void setSex(UserSex sex) {
		this.sex = sex;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getLogin() {
		return login;
	}


	public void setLogin(String login) {
		this.login = login;
	}
	
	

}
