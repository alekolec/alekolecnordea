package ticketGenerator;

import java.time.LocalDateTime;

public class Ticket {

	private LocalDateTime generationTime;
	private int id;

	public Ticket(int id) {
		generationTime = LocalDateTime.now();
		this.id = id;
	}

	public LocalDateTime getGenerationTime() {
		return generationTime;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Ticket [generationTime=" + generationTime + ", id=" + id + "]";
	}

}
