package ticketGenerator;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class TicketGenerator {
	
	private Map<Integer, Ticket> mapa = new HashMap<Integer,Ticket>();
	
	int id = 0;
	public int generateTicket() {
		
		id++;
		mapa.put(id, new Ticket(id) ) ;
		
		return id;
		
	}
	
	public void drukujDate(int id) {
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
	
	System.out.println(sdf.format(Timestamp.valueOf(mapa.get(id).getGenerationTime())));
	}
	
	public long howMuchTimeHaveIBeenWaiting_Goddamn(int id) {
		Timestamp t = Timestamp.valueOf(mapa.get(id).getGenerationTime());
		return System.currentTimeMillis() - t.getTime();
	}
	
	public void imGivingUp(int id) {
		mapa.remove(id);
	}
	
	public void printAll() {
		for (Ticket ticket : mapa.values()) {
			System.out.println(ticket);
		}
	}

}
