import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Bank {

	BankAccount account = new BankAccount();
	private ExecutorService service = Executors.newFixedThreadPool(15);
	
	public void add(double howMuch) {
		BankOrder order= new BankOrder(account, OPERATION.ADD, howMuch );
		service.submit(order);
	}
	public void sub(double howMuch) {
		BankOrder order= new BankOrder(account, OPERATION.SUB, howMuch );
		service.submit(order);
	}
	public void balance() {
		System.out.println("Na koncie: " + account.getMoney());
	}

}
