
public class BankAccount {
	double money;
	
	public double getMoney() {
		return money;
	}
	
	public  void setMoney(double money) {
		this.money = money;
	}
	
	public synchronized void substractFromAccount(double howMuch){
		money-=howMuch;
	}
	
	public synchronized void addToAccount(double howMuch) {
		money+=howMuch;
	}
	
	public void getBalance() {
		System.out.println("Na konce: " + money);;
	}

}
