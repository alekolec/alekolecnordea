import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
        Bank b = new Bank();
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < 10000; i++) {
            if (i % 2 == 0) {
                b.sub(5);
            } else {
                b.add(5);
            }
        }
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            String[] splits = line.split(" ");
            if (splits[0].equals("add")) {
                b.add(Double.parseDouble(splits[1]));
            } else if (splits[0].equals("sub")) {
                b.sub(Double.parseDouble(splits[1]));
            } else if (splits[0].equals("balance")) {
                b.balance();
            }
        }
    }
}
