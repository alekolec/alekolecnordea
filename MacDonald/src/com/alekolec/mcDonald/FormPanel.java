package com.alekolec.mcDonald;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JButton;
import java.awt.GridLayout;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import java.awt.Insets;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class FormPanel extends JPanel {
	private JTextField textField;

	/**
	 * Create the panel.
	 */
	public FormPanel() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 0);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] {0};
		gbl_panel.rowHeights = new int[] {0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0};
		gbl_panel.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0, 1.0};
		panel.setLayout(gbl_panel);
		
		JLabel lblNewLabel = new JLabel("Dodaj zam\u00F3wienie");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.gridwidth = 2;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		panel.add(lblNewLabel, gbc_lblNewLabel);
		
		JComboBox comboBox = new JComboBox();
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.insets = new Insets(0, 0, 5, 5);
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 0;
		gbc_comboBox.gridy = 1;
		panel.add(comboBox, gbc_comboBox);
		comboBox.addItem("McChicken");
		comboBox.addItem("McZestaw");
		comboBox.addItem("Frytki");
		comboBox.addItem("Shake");
		comboBox.addItem("Hamburger");
		comboBox.addItem("Cheeseburger");
		comboBox.addItem("BigMc");
		
		
		
		textField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 0);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 1;
		panel.add(textField, gbc_textField);
		textField.setColumns(10);
		
		JButton addProduct = new JButton("ADD");
		GridBagConstraints gbc_addProduct = new GridBagConstraints();
		gbc_addProduct.fill = GridBagConstraints.BOTH;
		gbc_addProduct.weightx = 1.0;
		gbc_addProduct.gridwidth = 2;
		gbc_addProduct.insets = new Insets(0, 0, 5, 0);
		gbc_addProduct.gridx = 0;
		gbc_addProduct.gridy = 2;
		panel.add(addProduct, gbc_addProduct);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.weighty = 1.0;
		gbc_scrollPane.weightx = 1.0;
		gbc_scrollPane.gridwidth = 2;
		gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 3;
		panel.add(scrollPane, gbc_scrollPane);
		
		JList list = new JList();
		scrollPane.setViewportView(list);
		
		JButton addOrder = new JButton("ADD");
		GridBagConstraints gbc_addOrder = new GridBagConstraints();
		gbc_addOrder.fill = GridBagConstraints.BOTH;
		gbc_addOrder.weightx = 1.0;
		gbc_addOrder.gridx = 1;
		gbc_addOrder.gridy = 4;
		panel.add(addOrder, gbc_addOrder);
		
		JButton clear = new JButton("CLEAR");
		GridBagConstraints gbc_clear = new GridBagConstraints();
		gbc_clear.insets = new Insets(0, 0, 0, 5);
		gbc_clear.weightx = 1.0;
		gbc_clear.fill = GridBagConstraints.BOTH;
		gbc_clear.gridx = 0;
		gbc_clear.gridy = 4;
		panel.add(clear, gbc_clear);

	}

}
