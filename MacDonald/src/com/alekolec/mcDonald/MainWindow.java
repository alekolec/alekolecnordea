package com.alekolec.mcDonald;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridLayout;
import javax.swing.JPanel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class MainWindow {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 553, 335);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel formPanel = new FormPanel();
		frame.getContentPane().add(formPanel);
		
		JPanel queuePanel = new JPanel();
		frame.getContentPane().add(queuePanel);
		GridBagLayout gbl_queuePanel = new GridBagLayout();
		gbl_queuePanel.columnWidths = new int[]{258, 0};
		gbl_queuePanel.rowHeights = new int[]{130, 0};
		gbl_queuePanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_queuePanel.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		queuePanel.setLayout(gbl_queuePanel);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 0;
		queuePanel.add(scrollPane, gbc_scrollPane);
		
		JList list = new JList();
		scrollPane.setViewportView(list);
		
		JPanel workingPanel = new JPanel();
		frame.getContentPane().add(workingPanel);
		GridBagLayout gbl_workingPanel = new GridBagLayout();
		gbl_workingPanel.columnWidths = new int[]{1, 0};
		gbl_workingPanel.rowHeights = new int[]{1, 0};
		gbl_workingPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_workingPanel.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		workingPanel.setLayout(gbl_workingPanel);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_2 = new GridBagConstraints();
		gbc_scrollPane_2.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_2.gridx = 0;
		gbc_scrollPane_2.gridy = 0;
		workingPanel.add(scrollPane_2, gbc_scrollPane_2);
		
		JList list_2 = new JList();
		scrollPane_2.setViewportView(list_2);
		
		JPanel readyPanel = new JPanel();
		frame.getContentPane().add(readyPanel);
		GridBagLayout gbl_readyPanel = new GridBagLayout();
		gbl_readyPanel.columnWidths = new int[]{0, 0};
		gbl_readyPanel.rowHeights = new int[]{0, 0};
		gbl_readyPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_readyPanel.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		readyPanel.setLayout(gbl_readyPanel);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
		gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_1.gridx = 0;
		gbc_scrollPane_1.gridy = 0;
		readyPanel.add(scrollPane_1, gbc_scrollPane_1);
		
		JList list_1 = new JList();
		scrollPane_1.setViewportView(list_1);
	}

}
