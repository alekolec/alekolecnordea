package ticketCalc;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class CalcGroupTicketPriceTest {

	@Test
	public void fivePeopleShouldPayTenPercent() {
		List <Person> group = new ArrayList<Person>();
		group.add(new Person(2));
		group.add(new Person(2));
		group.add(new Person(2));
		group.add(new Person(2));
		group.add(new Person(4));
		
		Assert.assertEquals(Person.buyGroupTicket(group), 1 );
	}
}
