package ticketCalc;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class CalcTicketPriceTest {
	private Person person;
	private int expectedPrice;
	
	public CalcTicketPriceTest(Person person, int expectedPrice) {
		this.person = person;
		this.expectedPrice = expectedPrice;
	}
	
	@Test
	public void testTicketPrice(){
		Assert.assertEquals(Person.buyTicket(person), expectedPrice);
		
	}
	
	
	
	@Parameters(name = "{index} Should {0} pay that much {1}")
	public static Collection<Object[]> dataProvider(){
		return Arrays.asList(new Object[][]{
			{new Person(3),0},
			{new Person(6),2},
			{new Person(26),2},
			{new Person(66),4},
			{new Person(67),4},
			{new Person(68),1}
			
							});
	}
	

}
