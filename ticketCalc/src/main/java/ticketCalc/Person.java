package ticketCalc;

import java.util.List;

public class Person {
	int age;

	public int getAge() {
		return age;
	}

	public Person(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Person [age=" + age + "]";
	}

	public static int buyTicket(Person person) {
		int price = 0;
		
		int age = person.getAge();
		if(age <= 3) {
			return price;
		}
		else if (age <= 26) {
			price = 2;
			return price;
		}
		else if (age <= 67) {
			price = 4;
			return price;
		}
		else  {
			price = 1;
			return price;
		}
		
	}

	public static int buyGroupTicket(List<Person>group) {
		int price = 0;
		
				
		if(group.size()>10) {
			for(int i=0; i<group.size(); i++) {
			price += buyTicket(group.get(i));
		}
			price = (int) (price * 0.8);
		return price;
		
	}
		else if(group.size()>4) {
			for(int i=0; i<group.size(); i++) {
			price += buyTicket(group.get(i));
		}
			price = (int) (price * 0.9);
			return price;
		} else {
			for(int i=0; i<group.size(); i++) {
			price += buyTicket(group.get(i));
		}
			
			return price;

}
	}
}

