<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
	<h2>Hello World!</h2>

	<a href="./Servlet">Pokaż servlet</a>
	<a href="./second.jsp">Pokaż drugi plik</a>
	<a href="./string.jsp">String</a>
	<hr>
	<c:if test="${sessionScope.loggedin}">
	<span>Witaj ${sessionScope.login}!</span>
	</c:if>
	<c:choose>
		<c:when test="${sessionScope.loggedin}">
			<a href="./LoginServlet">Wyloguj</a>
			
		</c:when>
		<c:otherwise>
		<form action="./LoginServlet" method="POST">
				<input type="text" name="login" placeholder="Podaj login" /> <input
					type="password" name="pass" />
				<button type="submit">Zaloguj</Button>
			</form>
		</c:otherwise>
	</c:choose>