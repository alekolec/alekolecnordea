<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="header.jsp"/>
	
	<c:set var="page" value="index"/>
	
	<c:choose>
	
		<c:when test="${page eq 'index'}">
			<p>Jestem w indeksie</p>
		</c:when>
		<c:when test="${page eq 'second' }">
			<p>To druga strona</p>
		</c:when>
		<c:otherwise>
			<p>Nie znam takiej strony</p>
		</c:otherwise>
	
	</c:choose>
	
	
	<c:forEach var="counter" begin="5" end="10">
	<p>${counter}</p>
	</c:forEach>
	
	<c:forTokens items="www.pawel.pl" delims="." var="address">
	<p>${address}</p>
	</c:forTokens>
	
	<c:url value="/second.jsp" var="secondFile"/>
	<p>${secondFile}</p>
	<a href="./">Strona główna</a>
	
<c:import url="footer.jsp"/>
