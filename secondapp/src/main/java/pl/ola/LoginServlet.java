package pl.ola;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//zmodyfikujmy dane w sesji
		//-loggedin - ustawiamy na false
		request.getSession().setAttribute("loggedin", false);
		//login  usuwamy z sesji
		request.getSession().removeAttribute("login");
		//wyswietlamy index.jsp
		response.sendRedirect("index.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String login = request.getParameter("login");
		String password = request.getParameter("pass");
		if (login.equals("admin") && password.equals("admin1")) {
			request.getSession().setAttribute("loggedin", true);
			request.getSession().setAttribute("login", login);
			response.sendRedirect("second.jsp");
		} else {
		response.sendRedirect("error.jsp");
		}
		
	}

}
