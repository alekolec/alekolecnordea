import java.time.LocalDateTime;

public class VehicleInfo {
	
	private String numer_rejestracyjny;
	private CarType typ;
	private LocalDateTime dataWjazdu;
	
	
	public String getNumer_rejestracyjny() {
		return numer_rejestracyjny;
	}
	public void setRejestracja(String numer_rejestracyjny) {
		this.numer_rejestracyjny = numer_rejestracyjny;
	}
	public CarType getTyp() {
		return typ;
	}
	public void setTyp(CarType typ) {
		this.typ = typ;
	}
	public LocalDateTime getDataWjazdu() {
		return dataWjazdu;
	}
	public void setDataWjazdu(LocalDateTime dataWjazdu) {
		this.dataWjazdu = dataWjazdu;
	}
	@Override
	public String toString() {
		return "VehicleInfo [numer_rejestracyjny=" + numer_rejestracyjny + ", typ=" + typ + ", dataWjazdu=" + dataWjazdu
				+ "]";
	}
	public VehicleInfo(String numer_rejestracyjny, CarType typ, LocalDateTime dataWjazdu) {
		super();
		this.numer_rejestracyjny = numer_rejestracyjny;
		this.typ = typ;
		this.dataWjazdu = dataWjazdu;
	}

	
	
	

}
