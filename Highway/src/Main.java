import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		HighWay autostrada = new HighWay();

		System.out.println("Podaj dzia�anie, rejestracj� i typ pojazdu:");
		while (scanner.hasNextLine()) {

			String line = scanner.nextLine();
			String[] splits = line.split(";");

			if (line.equals("quit")) {
				break;
			} else if (splits[0].equals("enter")) {
				parseSplits(splits, autostrada);
				continue;	
			} else if (splits[0].equals("leave")) {
				autostrada.vehicleLeave(splits[1]);
				continue;
			} else if (line.equals("print")) {
				autostrada.print();
				continue;
			} else if(splits[0].equals("check")) {
				autostrada.searchVehicle(splits[1]);
				continue;
			}

	
		}

	}

	private static void parseSplits(String[] splits, HighWay autostrada) {

		if (splits.length < 3) {
			System.err.println("Niewlasciwa ilosc parametrow.");
			return;
		}

		String rejestracja = null;
		rejestracja = splits[1];

		String typ = null;
		CarType cartype = null;
		try {
			typ = splits[2];
			cartype = CarType.valueOf(typ);
		} catch (IllegalArgumentException iae) {
			System.err.println("Z�y typ pojazdu, dost�pne: CAR, TRUCK, MOTORCYCLE");
			return;
		}

		autostrada.vehicleEntry(rejestracja, cartype);
	}
}
