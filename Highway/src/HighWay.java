import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class HighWay {

	Map<String, VehicleInfo> cars = new HashMap<String, VehicleInfo>();

	public void vehicleEntry(String numer_rejestracyjny, CarType typ) {

		VehicleInfo car = new VehicleInfo(numer_rejestracyjny, typ, LocalDateTime.now());
		cars.put(numer_rejestracyjny, car);
		System.out.println("Na autostrad� wjecha� pojazd o nr rejestracji: " + car.getNumer_rejestracyjny()
				+ ". Czas wjazdu: " + car.getDataWjazdu());

	}

	public VehicleInfo searchVehicle(String numer_rejestracyjny) {

		System.out.println(cars.get(numer_rejestracyjny));
		return cars.get(numer_rejestracyjny);

	}

	public void vehicleLeave(String numer_rejestracyjny) {

		
		if (cars.get(numer_rejestracyjny) != null) {
			Duration duration = Duration.between(cars.get(numer_rejestracyjny).getDataWjazdu(), LocalDateTime.now());

			long czasNaAutostradzie = duration.getSeconds();

			double oplata = czasNaAutostradzie * 0.03;
			
			System.out.println("Pojazd o nr rejestracji: " + numer_rejestracyjny + " wyje�d�a");
			System.out.println("Do zaplaty: ");
			System.out.println(oplata);
			cars.remove(numer_rejestracyjny);
		}
	}

 void print() {
		for (VehicleInfo vehicle : cars.values()) {
			System.out.println(vehicle);
		}
	}
}
