package memory;

public class QuadraticEquationOperations {

	public static double calcDelta(double a, double b, double c) {
		return b * b - 4 * a * c;
	}

	public static double calcX1(double a, double b, double c) throws NegativDeltaException {
		double delta = calcDelta(a, b, c);
		
			if (delta < 0) {
				throw new NegativDeltaException(delta);
			}
			return (-b - Math.sqrt(delta)) / (2 * a);
		
	}

	public static double calcX2(double a, double b, double c) throws NegativDeltaException {
		double delta = calcDelta(a, b, c);
		if (delta < 0) {
			throw new NegativDeltaException(delta);
		}
		return (-b + Math.sqrt(delta)) / (2 * a);
	}

	public static void main(String[] args) {

		try {
			double result = calcX2(1, 4, 9);
			System.out.println(result);

		} catch (NegativDeltaException exception) {
			System.out.println("Delta jest ujemna");
		}

	}
}
