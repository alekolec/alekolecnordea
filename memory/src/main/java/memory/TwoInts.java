package memory;

public class TwoInts {
	private int a;
	private int b;
	
	
	public TwoInts(int a, int b) {
		super();
		this.a = a;
		this.b = b;
	}
	
	public double divide() {
		return (double) this.a/this.b;
	}

	 
	public static void main(String[] args) {
	   TwoInts ints = new TwoInts(5,2);
	   double result = ints.divide();
	   System.out.println(result);
	}

	

}
