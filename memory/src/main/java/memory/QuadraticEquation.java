package memory;

public class QuadraticEquation {
    private double a;
    private double b;
    private double c;
    
    public QuadraticEquation(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double calcDelta() {
        return this.b*this.b - 4*this.a*this.c;
    }
    
    public double calcX1() {
        return (-b - Math.sqrt(calcDelta()))/(2*a);
    }
    
    public double calcX2() {
        return (-b + Math.sqrt(calcDelta()))/(2*a);
    }
    
    public static void main(String[] args) {
		QuadraticEquation rownanie = new QuadraticEquation(1,4,3);
		double result = rownanie.calcX2();
		System.out.println(result);
	}
}