package memory;

public class TwoIntsOperations {
	public static double divide(int a, int b) {
		return (double) a/b;
	}
	
	public static void main(String[] args) {
		 
		   int first = 5;
		   int second = 2;
		   double result = TwoIntsOperations.divide(first, second);
		   System.out.println(result);
		}


}
