package memory;

public class NegativDeltaException extends RuntimeException{
	
	public NegativDeltaException(double delta) {
		super("Delta jest ujemna (" + delta + ")");
	}

}
