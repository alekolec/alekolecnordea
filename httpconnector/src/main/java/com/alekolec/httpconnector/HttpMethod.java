package com.alekolec.httpconnector;

public enum HttpMethod {
	GET("GET"), PUT("PUT"), PATCH("PATCH"), POST("POST"), DELETE("DELETE"), OPTIONS("OPTIONS");

	String descr;

	private HttpMethod(String descr) {
		this.descr = descr;
	}

	public String getDescr() {
		return descr;
	}

}
