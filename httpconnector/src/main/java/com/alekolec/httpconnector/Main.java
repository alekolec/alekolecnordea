package com.alekolec.httpconnector;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Main {

	public static void main(String[] args) throws IOException, ParseException {
		
		//GET
		URL url = new URL("http://palo.ferajna.org/sda/wojciu/json.php");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		// rzutowanie, bo moze byc https
		
		String userAgent = "Ola/1.0";

		connection.setRequestMethod("GET");
		connection.setRequestProperty("UserAgent", userAgent);
		String result = "";
		int responseCode = connection.getResponseCode();

		if (responseCode == 200) {
			InputStreamReader reader = new InputStreamReader(connection.getInputStream());
			Scanner s = new Scanner(reader);

			while (s.hasNextLine()) {
				result += s.nextLine();
			}
		}

		System.out.println(result);
		
		//POST
		URL postURL = new URL("http://palo.ferajna.org/sda/wojciu/json.php");
		HttpURLConnection postConnection = (HttpURLConnection) postURL.openConnection();
		
		postConnection.setRequestMethod("POST");
		postConnection.setRequestProperty("User-Agent", userAgent);
		
		
		
		String queryString = "login=admin";
		
		//tylko dla POST
		postConnection.setDoOutput(true);  //oczekujemy odpowiedzi
		DataOutputStream dos = new DataOutputStream(postConnection.getOutputStream());
		dos.writeBytes(queryString);
		dos.flush();
		dos.close();
		//koniec tylko dla POST
		
		Scanner sc = new Scanner(new InputStreamReader(postConnection.getInputStream()));
		
		String postRet = "";
		
		while(sc.hasNextLine()) {
			postRet += sc.nextLine();
		}
		
		System.out.println(postRet);
		
		JSONParser jp = new JSONParser();
		JSONObject jo = (JSONObject) jp.parse(postRet);
		
		System.out.println("Status: " + jo.get("status"));

	}

}
