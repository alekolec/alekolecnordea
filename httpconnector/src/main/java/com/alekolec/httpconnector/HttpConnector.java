package com.alekolec.httpconnector;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class HttpConnector {

	private URL obj;
	private HttpURLConnection con;
	private String ua = "Ola/1.0";

	public String sendGET(String url) {
		String result = "";
		try {
			obj = new URL(url);
			con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("UserAgent", ua);

			int responseCode = con.getResponseCode();

			if (responseCode == 200) {
				InputStreamReader reader = new InputStreamReader(con.getInputStream());
				Scanner s = new Scanner(reader);

				while (s.hasNextLine()) {
					result += s.nextLine();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

		return result;
	}

	public String sendPOST(String url, String params) {
		String result = "";
		try {
			obj = new URL(url);
			con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("UserAgent", ua);

			con.setDoOutput(true); // bedziemy przesylali parametry do serwera
			DataOutputStream dos = new DataOutputStream(con.getOutputStream());
			dos.writeBytes(params); // bajt po bajcie
			dos.flush();
			dos.close();

			Scanner sc = new Scanner(new InputStreamReader(con.getInputStream()));

			while (sc.hasNextLine()) {
				result += sc.nextLine();
			}
			

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

		return result;
	}

	public String send(HttpMethod method, String url, String params) {
		String result = "";
		try {
			obj = new URL(url);
			con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod(method.getDescr());
			con.setRequestProperty("UserAgent", ua);

			
				if (method.equals(HttpMethod.POST)) {
					
					con.setDoOutput(true);
					DataOutputStream dos = new DataOutputStream(con.getOutputStream());
					dos.writeBytes(params);
					dos.flush();
					dos.close();
				}
				
				if (con.getResponseCode() == 200) {
				InputStreamReader reader = new InputStreamReader(con.getInputStream());
				Scanner s = new Scanner(reader);

				while (s.hasNextLine()) {
					result += s.nextLine();
				}
				
			}
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

		return result;
	}
}
