package com.alekolec.httpconnector;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Main2 {

	public static void main(String[] args) {

		HttpConnector hc = new HttpConnector();

		String[] loginy = new String[8];
		loginy[0] = "login=admin";
		loginy[1] = "login=adam";
		loginy[2] = "login=operator";
		loginy[3] = "login=test";
		loginy[4] = "login=tester";
		loginy[5] = "login=oper";
		loginy[6] = "login=asd";
		loginy[7] = "login=admin2";

		for (int i = 0; i < loginy.length; i++) {
			String result = hc.send(HttpMethod.POST, "http://palo.ferajna.org/sda/wojciu/json.php", loginy[i]);
			JSONParser jp = new JSONParser();
			try {
				JSONObject jo = (JSONObject) jp.parse(result);
				System.out.println("Status: " + jo.get("status"));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
//-------------ZADANIE 3, 4 -------------------------//
		// hc.sendPOST("http://database.dev4.it:54000/users",
		// "name=Ola&lastname=Koleczka");

		String resultGet = hc.send(HttpMethod.GET, "http://database.dev4.it:54000/users", "");
		JSONParser jp = new JSONParser();
		try {
			JSONArray ja = (JSONArray) jp.parse(resultGet);
			for (int i = 0; i < ja.size(); i++) {
				JSONObject jo = (JSONObject) ja.get(i);
				System.out.print("Name: " + jo.get("name") + " ");
				System.out.println(("\t Last name:" + jo.get("lastname")));
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

		// -----------------ZADANIE 5---------------------//

		String randomUser = hc.send(HttpMethod.GET, "https://randomuser.me/api/", "");
		try {
			JSONParser jpUser = new JSONParser();
			JSONObject jo = (JSONObject) jpUser.parse(randomUser);
			JSONArray ja = (JSONArray) jo.get("results");

			JSONObject jooo = (JSONObject) ja.get(0);
			JSONObject userDetails = (JSONObject) jooo.get("name");
			System.out.println(userDetails.get("first") + " " + userDetails.get("last"));

		} catch (ParseException e) {
			e.printStackTrace();
		}

		// --------------ZADANIE 6 -------------------------//
		String gdyniaPower = hc.send(HttpMethod.GET, "http://otwartedane.gdynia.pl/portal/data/iso37120/7/5/data.json",
				"");
		try {
			JSONParser jpGdynia = new JSONParser();
			JSONObject joMain = (JSONObject) jpGdynia.parse(gdyniaPower);
			JSONObject jmeta = (JSONObject) joMain.get("meta");
			System.out.print(jmeta.get("zasob") + ": ");

			JSONArray jdata = (JSONArray) joMain.get("data");
			JSONObject jdataDetails = (JSONObject) jdata.get(0);
			System.out.println(jdataDetails.get("wartosc"));

		} catch (ParseException e) {
			e.printStackTrace();
		}
	
	
	//----------------ZADANIE 7----------------------------------//
	String gdyniaEvents = hc.send(HttpMethod.GET, "http://planer.info.pl/api/rest/events.json?start_date=2017-08-08&end_date=2017-08-09", "");
	System.out.println(gdyniaEvents);
	
	try {
		JSONParser jpEvents = new JSONParser();
		JSONArray joMain = (JSONArray) jpEvents.parse(gdyniaEvents);
		System.out.println(joMain);
		/*for(int i = 0; i<joMain.size(); i++) {
		JSONObject  joEvent = (JSONObject) joMain.get(i);
		System.out.println(joEvent);
		}*/

	} catch (ParseException e) {
		e.printStackTrace();
	}
	}
}
