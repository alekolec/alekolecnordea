package exceptions;

import java.util.Scanner;

public class ArrayOfNumbers {

	public static void main(String[] args) {
		int[] numbers = { 0, 10, 20 };

		Scanner scanner = new Scanner(System.in);

		
		do {
			System.out.println("Podaj indeks tablicy");
			int index = scanner.nextInt();

			if (index < numbers.length && index >= 0) {
				System.out.println("Liczba o indeksie " + index + " to " + numbers[index]);
				break;
			}
			else {
				System.out.println("Index poza zakresem");
			}

		} while (true);

	}
}