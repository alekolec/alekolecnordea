package exceptions;

import java.util.Scanner;

public class ArrayOfNumbersTryCatch {

		public static void main(String[] args) {
			int[] numbers = { 0, 10, 20 };

			Scanner scanner = new Scanner(System.in);

			do {
				System.out.println("Podaj indeks tablicy");
				 int index = scanner.nextInt();
				 try {
					System.out.println("Liczba o indeksie " + index + " to " + numbers[index]);
					break;			
				} catch (ArrayIndexOutOfBoundsException exception) {
					System.out.println("Za du�y");
				}
				
			} while (true);

		}
	}

	
