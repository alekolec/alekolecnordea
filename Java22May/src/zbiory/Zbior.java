package zbiory;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Zbior {

	public static void main(String[] args) {

		Set<Integer> set = new TreeSet<Integer>();

		set.add(1);
		set.add(2);
		set.add(1);
		set.add(3);
		set.add(4);
		set.add(2);
		set.add(2);
		set.add(3);
		set.add(2);
		set.add(4);
		set.add(5);

		System.out.println("Ilo�� element�w: " + set.size());

		System.out.println("Elementy:");
		for (int element : set) {
			System.out.println(element);
		}

		set.remove(0);
		set.remove(1);

		System.out.println("Ilo�� element�w: " + set.size());

		System.out.println("Elementy:");
		for (int element : set) {
			System.out.println(element);
		}
		
		
	}

}
