package kolejki;

import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

public class Queue {
	
		
	public static void main(String[] args) {
		
		PriorityQueue<Integer> kolejka = new PriorityQueue<Integer>();
		List<Integer> lista = new LinkedList<Integer>();
		
		kolejka.add(1);
		kolejka.add(21);
		kolejka.add(2);
		kolejka.add(10);
		kolejka.add(12);
		
		lista.add(1);
		lista.add(21);
		lista.add(2);
		lista.add(10);
		lista.add(12);
		
		System.out.println("Elementy kolejki");
		for(int element : kolejka) {
			System.out.println(element);
		}
		
		System.out.println("Elementy listy");
		for(int element : lista) {
			System.out.println(element);
		}
		
		
		
	}

}
