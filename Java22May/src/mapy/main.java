package mapy;

public class main {

	public static void main(String[] args) {
		
		Uczelnia polibuda = new Uczelnia();
		
		polibuda.dodajStudenta(new Student(10000001l, "Jan", "Kowalski"));
		polibuda.dodajStudenta(new Student(10000002l, "Adam", "Nowak"));
		polibuda.dodajStudenta(new Student(10000003l, "Stefan", "Sroka"));
		polibuda.dodajStudenta(new Student(10000004l, "Igor", "Sowa"));
		polibuda.dodajStudenta(new Student(10000005l, "Alojzy", "Wr�bel"));
		polibuda.dodajStudenta(new Student(10000006l, "Anna", "Orze�"));
		
		System.out.println("Czy istnieje");
		polibuda.czyIstniejeStudent(10000003l);
		
		System.out.println("Pobierz studenta");
		polibuda.pobierzStudenta(10000004l);
		
		System.out.println("Liczba student�w");
		polibuda.liczbaStudentow();
		
		
		System.out.println("Pokaz wszystkich");
		Uczelnia.pokazWszystkich();
		

	}

}
