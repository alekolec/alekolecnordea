package mapy;


import java.util.HashMap;
import java.util.Map;

public class Uczelnia {
	
	private static Map<Long, Student> listaStudentow = new HashMap<Long, Student>();
	
	public void dodajStudenta (Student student) {
		listaStudentow.put(student.getNumerIndeksu(), student);
	}
	
	public boolean czyIstniejeStudent(long numerIndeksu) {
		System.out.println(listaStudentow.containsKey(numerIndeksu));
		return  (listaStudentow.containsKey(numerIndeksu));
		
		}
	
	public Student pobierzStudenta(long numerIndeksu) {
		System.out.println(listaStudentow.get(numerIndeksu));
		return listaStudentow.get(numerIndeksu);
	}
	
	public int liczbaStudentow() {
		System.out.println(listaStudentow.size());
		return listaStudentow.size();
	}
	
	//Drukuje si� ca�a mapa, wi�c w jednej linii
	
//	public static Collection<Student> pokazWszystkich() {
		
//		System.out.println(listaStudentow.values());
//		return listaStudentow.values();
//	
//	}
	
	//Wydrukowanie ka�dego z osobna
	public static void pokazWszystkich() {
		for (Student student : listaStudentow.values()) {
			System.out.println(student);
			
		}
	}
}

