package mapy;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class Student {
	private long numerIndeksu;
	private String imie;
	private String nazwisko;

	public Student(long numerIndeksu, String imie, String nazwisko) {
		this.numerIndeksu = numerIndeksu;
		this.imie = imie;
		this.nazwisko = nazwisko;
	}

	public String getImie() {
		return imie;
	}

	public String getNazwisko() {
		return nazwisko;
	}

	public long getNumerIndeksu() {
		return numerIndeksu;
	}
	
	@Override
	public String toString() {
		
		return "Imie: " + imie + " Nazwisko: " + nazwisko ;
	}


	public static void main(String[] args) {

		Map<Integer, Student> studenci = new TreeMap<Integer, Student>();

		Student student1 = new Student(1001, "Ola", "Lisek");
		Student student2 = new Student(1002, "Kasia", "Chytrusek");
		Student student3 = new Student(1003, "Stefan", "Dzik");
		Student student4 = new Student(1004, "Alojzy", "Jele�");
		Student student5 = new Student(1005, "Jowita", "Sowa");
		Student student6 = new Student(100200, "Jan", "Je�");
		Student student7 = new Student(100400, "Czes�aw", "�bik");


		studenci.put(1001, student1);
		studenci.put(1002, student2);
		studenci.put(1003, student3);
		studenci.put(1004, student4);
		studenci.put(1005, student5);
		studenci.put(100200, student6);
		studenci.put(100400, student7);

		System.out.println("Czy istnieje student o indeksie 100200?");
		if (studenci.containsKey(100200))
			System.out.println("Istnieje");
		
		System.out.println("Student o numerze indeksu 100400 to: ");
		System.out.println(studenci.get(100400).getImie() + " " + studenci.get(100400).getNazwisko());
		
		
		System.out.println("Ilu jest student�w?");
		System.out.println(studenci.size());
		
		for (Student s : studenci.values()) {
			System.out.println(s);
			
		}

	}
}