package krolestwa;

public class OrcArcher extends AbstractCharacter implements IWarrior{
	
	int arrowCount;
	
	public OrcArcher (int arrowCount) {
		super();
		this.arrowCount = arrowCount;
		
	}

	@Override
	public void attack(AbstractCharacter character) {
		System.out.println("Atak strzelcem!");
		if (arrowCount >0) 
		{
			character.decreaseHealth(15);
			arrowCount -= 1;
		System.out.println("-15 zycia!");
		} 
		else { System.out.println("Nie mam strzal!!");
	}
}
}
