package krolestwa;

import java.util.ArrayList;
import java.util.List;

public class OrcKingdom implements IKingdom{
	
	private String name;
	
	List<AbstractCharacter> army = new ArrayList <AbstractCharacter>();
	
	public OrcKingdom(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void die () {
		army.remove(0);
	}
	
	@Override
	public String toString() {
		
		return "Krolestwo: " + this.getName() + " ilosc wojska: " + army.size();
	}
	
	@Override
	public List<AbstractCharacter> getArmy() {
		
		
		return army;
	}
	
	@Override
	public void addWarrior() {
		army.add(new OrcWarrior());
		
	}
	
	@Override
	public void addArcher() {
		army.add(new OrcArcher(15));
		
	}
	
	@Override
	public void addMage() {
		army.add(new OrcMage());
		
	}
	
	

}
