package krolestwa;

import java.util.List;

public interface IKingdom {
	
	public List<AbstractCharacter> getArmy();
	public void addWarrior();
	public void addArcher();
	public void addMage();
	public void die();

}
