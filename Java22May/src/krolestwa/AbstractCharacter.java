package krolestwa;

public abstract class AbstractCharacter implements IWarrior {
int health;
int energy;

public AbstractCharacter() {
	int health = 100;
	int energy = 100;
}

public AbstractCharacter(int health, int energy) {
	this.health = health;
	this.energy = energy;
}

public int decreaseHealth(int points) {
	return health - points;
}

public int decreaseEnergy(int points) {
	return energy - points;
}

public int getEnergy() {
	return energy;
}

public int getHealth() {
	return health;
}


}
