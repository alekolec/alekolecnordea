
public class Main {

	public static void main(String[] args) {
	
		AbstractBike kross = BikeFactory.createKROSS5Gears();
		AbstractBike merida = BikeFactory.createMERIDA6Gears();
		AbstractBike iniana = BikeFactory.createINIANA3Gears();
		AbstractBike felt = BikeFactory.createFELT6Gears();
		AbstractBike goetze = BikeFactory.createGOETZE1Gear();
		
		System.out.println(kross);
		System.out.println(merida);
		System.out.println(iniana);
		System.out.println(felt);
		System.out.println(goetze);

	}

}
