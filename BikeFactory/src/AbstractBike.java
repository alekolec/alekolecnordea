
public class AbstractBike {
	private String brand;
	private int places;
	private int gears;
	private BikeType type;
	
	
	public AbstractBike(String brand, int places, int gears, BikeType type) {
		super();
		this.brand = brand;
		this.places = places;
		this.gears = gears;
		this.type = type;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public int getPlaces() {
		return places;
	}
	public void setPlaces(int places) {
		this.places = places;
	}
	public int getGears() {
		return gears;
	}
	public void setGears(int gears) {
		this.gears = gears;
	}
	public BikeType getType() {
		return type;
	}
	public void setType(BikeType type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "AbstractBike [brand=" + brand + ", places=" + places + ", gears=" + gears + ", type=" + type + "]";
	}
	
	

}
