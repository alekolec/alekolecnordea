package com.alekolec.pralnia;

public interface ITrybPracy {

void przygotujDoPrania() throws InterruptedException;
void pranie()throws InterruptedException;
void czynnościPoPraniu() throws InterruptedException;
}
