package com.alekolec.pralnia;


public class Washer implements Runnable {

	private ITrybPracy wybranyProgram;
	private Thread watek;
	private IListener listener;

	public ITrybPracy getWybranyProgram() {
		return wybranyProgram;
	}

	public void setWybranyProgram(ITrybPracy wybranyProgram) {
		this.wybranyProgram = wybranyProgram;
	}
	
	

	public Washer(IListener listener) {
		super();
		this.listener = listener;
	}

	public void run() {
		try {
			wybranyProgram.przygotujDoPrania();
			listener.phaseFinished(1);
			//listener.moveProgressBar(20);
			wybranyProgram.pranie();
			listener.phaseFinished(2);
			//listener.moveProgressBar(80);
			wybranyProgram.czynnościPoPraniu();
			listener.phaseFinished(3);
			//listener.moveProgressBar(100);
		} catch (Exception e) {
			System.out.println("Interrupted");
			listener.statusUpdate("Pranie przerwane");
		}

	}

	public void stop() {
		watek.interrupt();
		System.out.println("Stop");

	}

	public void start() {
		watek = new Thread(this);
		watek.start();

		System.out.println("Start");
	}

}
