package com.alekolec.pralnia;

public interface IListener {
	void phaseFinished(int phase);
	void statusUpdate(String message);
	void moveProgressBar(int step);
}
