package com.alekolec.pralnia;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import com.alekolec.programy.*;

import java.awt.event.ActionListener;
import java.util.Enumeration;
import java.awt.event.ActionEvent;
import javax.swing.JProgressBar;

public class PralkaPanel extends JPanel implements IListener {

	/**
	 * Create the panel.
	 */
	private Washer washer;
	private JRadioButton radioSzybkie;
	private JRadioButton radioNormalne;
	private JRadioButton radioOdwirowywanie;
	private JRadioButton radioEko;
	private JRadioButton radioDelikatne;
	private JCheckBox statusWstepne;
	private JCheckBox statusPranie;
	private JCheckBox statusOdwirowywanie;
	private JLabel status;
	private ButtonGroup radioGroup;
	private JButton start;
	private JButton stop;
	private JProgressBar progressBar;

	public PralkaPanel() {
		washer = new Washer(this);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		JLabel lblNewLabel = new JLabel("Pralka");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 0;
		add(lblNewLabel, gbc_lblNewLabel);

		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 1;
		add(panel, gbc_panel);
		panel.setLayout(new GridLayout(0, 1, 0, 0));

		JLabel lblProgram = new JLabel("Program");
		panel.add(lblProgram);

		radioSzybkie = new JRadioButton("Pranie szybkie");
		panel.add(radioSzybkie);

		radioNormalne = new JRadioButton("Pranie normalne");
		panel.add(radioNormalne);

		radioOdwirowywanie = new JRadioButton("Odwirowywanie");
		panel.add(radioOdwirowywanie);

		radioEko = new JRadioButton("Pranie ekonomiczne");
		panel.add(radioEko);

		radioDelikatne = new JRadioButton("Pranie delikatne");
		panel.add(radioDelikatne);
		
		progressBar = new JProgressBar();
		panel.add(progressBar);
		progressBar.setStringPainted(true);
		

		JLabel lblStatusPrania = new JLabel("Status prania");
		panel.add(lblStatusPrania);

		statusWstepne = new JCheckBox("Pranie wst\u0119pne");
		panel.add(statusWstepne);
		statusWstepne.setEnabled(false);

		statusPranie = new JCheckBox("Pranie");
		panel.add(statusPranie);
		statusPranie.setEnabled(false);

		statusOdwirowywanie = new JCheckBox("Odwirowywanie");
		panel.add(statusOdwirowywanie);
		statusOdwirowywanie.setEnabled(false);

		status = new JLabel("Status:");
		panel.add(status);

		JPanel panel_1 = new JPanel();
		panel.add(panel_1);
		panel_1.setLayout(new GridLayout(1, 2, 0, 0));

		start = new JButton("Start");
		start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (radioSzybkie.isSelected()) {
					washer.setWybranyProgram(new PranieSzybkie(PralkaPanel.this));
				} else if (radioDelikatne.isSelected()) {
					washer.setWybranyProgram(new PranieDelikatne(PralkaPanel.this));
				} else if (radioNormalne.isSelected()) {
					washer.setWybranyProgram(new PranieNormalne(PralkaPanel.this));
				} else if (radioOdwirowywanie.isSelected()) {
					washer.setWybranyProgram(new Odwirowywanie(PralkaPanel.this));
				} else {
					washer.setWybranyProgram(new PranieEkonomiczne(PralkaPanel.this));
				}
				washer.start();
				
				Enumeration<AbstractButton> washingButtons = radioGroup.getElements();
                while (washingButtons.hasMoreElements()) {
                    washingButtons.nextElement().setEnabled(false);
                }
                start.setEnabled(false);
                stop.setEnabled(true);
			}
		});
		panel_1.add(start);

		stop = new JButton("Stop");
		stop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Enumeration<AbstractButton> washingButtons = radioGroup.getElements();
                while (washingButtons.hasMoreElements()) {
                    washingButtons.nextElement().setEnabled(true);
                }
                start.setEnabled(true);
                stop.setEnabled(false);
                
                statusWstepne.setSelected(false);
                statusPranie.setSelected(false);
                statusOdwirowywanie.setSelected(false);
                progressBar.setValue(0);
				washer.stop();
				
			}
		});
		panel_1.add(stop);

		radioGroup = new ButtonGroup();
		radioGroup.add(radioSzybkie);
		radioGroup.add(radioOdwirowywanie);
		radioGroup.add(radioDelikatne);
		radioGroup.add(radioNormalne);
		radioGroup.add(radioEko);

		radioSzybkie.setSelected(true);

	}

	public void phaseFinished(int phase) {
		if (phase == 1) {
			statusWstepne.setSelected(true);
		} else if (phase == 2) {
			statusWstepne.setSelected(true);
			statusPranie.setSelected(true);
		} else if (phase == 3) {
			statusWstepne.setSelected(true);
			statusPranie.setSelected(true);
			statusOdwirowywanie.setSelected(true);
		
		
		}

	}

	public void statusUpdate(String message) {
		status.setText(message);

	}

	public void moveProgressBar(int step) {
		progressBar.setValue(step);
		
	}

}
