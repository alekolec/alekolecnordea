package com.alekolec.pralnia;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridLayout;
import javax.swing.JPanel;

public class OkienkoPralnia {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OkienkoPralnia window = new OkienkoPralnia();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public OkienkoPralnia() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel panel1 = new PralkaPanel();
		frame.getContentPane().add(panel1);
		
		JPanel panel2 = new PralkaPanel();
		frame.getContentPane().add(panel2);
		
		JPanel panel3 = new PralkaPanel();
		frame.getContentPane().add(panel3);
	}

}
