package com.alekolec.programy;

import com.alekolec.pralnia.IListener;
import com.alekolec.pralnia.ITrybPracy;

public class PranieDelikatne implements ITrybPracy {

	IListener listener;
	int suma = 10;

	public PranieDelikatne(IListener listener) {
		super();
		this.listener = listener;
	}

	public void przygotujDoPrania() throws InterruptedException {

		listener.statusUpdate("Rozpoczynam delikatne pranie");
		Thread.sleep(5000);
		listener.moveProgressBar(5);
		listener.statusUpdate("Delikatnie nalewam wody do pralki");
		Thread.sleep(7000);
		listener.moveProgressBar(10);
		listener.statusUpdate("Przygotowywanie zako�czone");
		Thread.sleep(3000);

	}

	public void pranie() throws InterruptedException {

		for (int i = 0; i < 10; i++) {

			listener.statusUpdate("Delikatnie nalewam wody");
			Thread.sleep(3000);
			listener.statusUpdate("Pranie");
			Thread.sleep(2000);
			listener.statusUpdate("Delikatnie wypuszczam wod�");
			Thread.sleep(1000);
			listener.moveProgressBar(suma += 6);

		}

	}

	public void czynno�ciPoPraniu() throws InterruptedException {

		listener.statusUpdate("Sprawdzam przepe�nienie");
		Thread.sleep(2000);
		listener.statusUpdate("Rozpoczynam wirowanie");
		for (int i = 0; i < 5; i++) {
			listener.statusUpdate("Wirowanie 1000obr/min");
			Thread.sleep(2000);
			listener.statusUpdate("Zwalniam");
			listener.moveProgressBar(suma += 3);
		}

		listener.statusUpdate("Koniec");
		listener.moveProgressBar(100);
	}

}
