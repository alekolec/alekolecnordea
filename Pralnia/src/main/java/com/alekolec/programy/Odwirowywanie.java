package com.alekolec.programy;

import com.alekolec.pralnia.IListener;
import com.alekolec.pralnia.ITrybPracy;

public class Odwirowywanie implements ITrybPracy {

	IListener listener;

	public Odwirowywanie(IListener listener) {
		super();
		this.listener = listener;
	}

	public void przygotujDoPrania() throws InterruptedException {

		listener.statusUpdate("Rozpoczynam odwirowywanie");
		Thread.sleep(5000);
		listener.moveProgressBar(5);
		listener.statusUpdate("Przygotowywanie zakończone");
		Thread.sleep(3000);
		listener.moveProgressBar(10);
	}

	public void pranie() throws InterruptedException {
		int suma = 10;
		for (int i = 0; i < 10; i++) {

			listener.statusUpdate("Wirowanie 1000obr/min");
			Thread.sleep(3000);
			listener.statusUpdate("Wirowanie 800obr/min");
			Thread.sleep(2000);
			listener.statusUpdate("Wirowanie 500obr/min");
			Thread.sleep(1000);
			listener.statusUpdate("Wirowanie 0obr/min");
			listener.moveProgressBar(suma += 7);
		}
		listener.statusUpdate("Wirowanie zakończone");
		listener.moveProgressBar(suma += 10);
	}

	public void czynnościPoPraniu() throws InterruptedException {
		listener.statusUpdate("Koniec");
		listener.moveProgressBar(100);
	}

}
