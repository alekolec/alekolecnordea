package com.alekolec.programy;

import com.alekolec.pralnia.IListener;
import com.alekolec.pralnia.ITrybPracy;

public class PranieNormalne implements ITrybPracy {

	IListener listener;
	int suma = 10;

	public PranieNormalne(IListener listener) {
		super();
		this.listener = listener;
	}

	public void przygotujDoPrania() throws InterruptedException {

		listener.statusUpdate("Rozpoczynam normalne pranie");
		Thread.sleep(5000);
		listener.moveProgressBar(5);
		listener.statusUpdate("Nalewam wody do pralki");
		Thread.sleep(7000);
		listener.moveProgressBar(10);
		listener.statusUpdate("Przygotowywanie zakończone");
		Thread.sleep(3000);

	}

	public void pranie() throws InterruptedException {
		for (int i = 0; i < 10; i++) {

			listener.statusUpdate("Nalewam wody");
			Thread.sleep(3000);
			listener.statusUpdate("Pranie");
			Thread.sleep(2000);
			listener.statusUpdate("Wypuszczanie wody");
			Thread.sleep(1000);
			listener.moveProgressBar(suma += 7);

		}

	}

	public void czynnościPoPraniu() throws InterruptedException {

		listener.statusUpdate("Sprawdzam przepełnienie");
		Thread.sleep(2000);
		listener.statusUpdate("Rozpoczynam wirowanie");
		listener.statusUpdate("Wirowanie 100obr/min");
		Thread.sleep(1000);
		listener.moveProgressBar(suma += 3);
		listener.statusUpdate("Wirowanie 200obr/min");
		Thread.sleep(1000);
		listener.moveProgressBar(suma += 3);
		listener.statusUpdate("Wirowanie 400obr/min");
		Thread.sleep(1000);
		listener.moveProgressBar(suma += 3);
		listener.statusUpdate("Wirowanie 800obr/min");
		Thread.sleep(2000);
		listener.moveProgressBar(suma += 3);
		listener.statusUpdate("Wirowanie 1000obr/min");
		Thread.sleep(2000);
		listener.moveProgressBar(suma += 3);

		listener.statusUpdate("Koniec");
		listener.moveProgressBar(100);
	}

}
