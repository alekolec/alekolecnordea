package com.alekolec.programy;

import com.alekolec.pralnia.IListener;
import com.alekolec.pralnia.ITrybPracy;

public class PranieEkonomiczne implements ITrybPracy {

	IListener listener;
	int suma = 10;

	public PranieEkonomiczne(IListener listener) {
		super();
		this.listener = listener;
	}

	public void przygotujDoPrania() throws InterruptedException {
		listener.statusUpdate("Rozpoczynam ekonomiczne pranie");
		listener.moveProgressBar(5);
		Thread.sleep(5000);
		listener.statusUpdate("Nalewam ma�o wody do pralki");
		Thread.sleep(7000);
		listener.moveProgressBar(10);
		listener.statusUpdate("Przygotowywanie zako�czone");
		Thread.sleep(3000);

	}

	public void pranie() throws InterruptedException {
		for (int i = 0; i < 10; i++) {

			listener.statusUpdate("Nalewam ma�o wody");
			Thread.sleep(3000);
			listener.statusUpdate("Pranie");
			Thread.sleep(2000);
			listener.statusUpdate("Wypuszczanie ma�ej ilo�ci wody");
			Thread.sleep(1000);
			listener.moveProgressBar(suma += 6);

		}

	}

	public void czynno�ciPoPraniu() throws InterruptedException {

		listener.statusUpdate("Sprawdzam przepe�nienie");
		Thread.sleep(2000);
		listener.statusUpdate("Rozpoczynam wirowanie");
		listener.statusUpdate("Wirowanie 100obr/min");
		Thread.sleep(1000);
		listener.moveProgressBar(suma += 3);
		listener.statusUpdate("Wirowanie 200obr/min");
		Thread.sleep(1000);
		listener.moveProgressBar(suma += 3);
		listener.statusUpdate("Wirowanie 400obr/min");
		Thread.sleep(1000);
		listener.moveProgressBar(suma += 3);
		listener.statusUpdate("Wirowanie 800obr/min");
		Thread.sleep(2000);
		listener.moveProgressBar(suma += 3);
		listener.statusUpdate("Wirowanie 1000obr/min");
		Thread.sleep(2000);
		listener.moveProgressBar(80);

		listener.statusUpdate("Koniec");
		listener.moveProgressBar(100);
	}
}
