package pl.sdacademy.moviedb;

import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import pl.sdacademy.moviedb.entity.Genre;
import pl.sdacademy.moviedb.entity.Movie;
import pl.sdacademy.moviedb.util.HibernateUtil;

public class Main {

	public static void main(String[] args) {
		System.out.println("Test");

		Scanner scanner = new Scanner(System.in);
		System.out.println("Podaj tytu�");
		String title = scanner.nextLine();
		System.out.println("Podaj rok");
		int year = Integer.parseInt(scanner.nextLine());
		System.out.println("Podaj czas trwania");
		double duration = Double.parseDouble(scanner.nextLine());
		System.out.println("Podaj opis");
		String description = scanner.nextLine();
		System.out.println("Podaj gatunek");
		String genre = scanner.nextLine();

		Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);

		Session session= HibernateUtil.openSession();
		
		Transaction t = null;
		try {
			t = session.beginTransaction();
			List<Genre> gens = session.createQuery("from Genre where name = '" + genre + "'").list();
			Movie movie = new Movie();
			if (gens.size() > 0) {
				System.out.println("Znalaz�em: " + gens.get(0).getName());
				Genre existingGenre = gens.get(0);
				movie.setGenre(existingGenre);

			} else {
				System.out.println("Nie znalaz�em");
				Genre newGenre = new Genre();
				newGenre.setName(genre);
				movie.setGenre(newGenre);
			}

			movie.setTitle(title);
			movie.setYear(year);
			movie.setDuration(duration);
			movie.setDescription(description);

			session.save(movie);
			t.commit();

		} catch (Exception e) {
			if (t != null)
				t.rollback();
		}
		session.close();

	}

}
