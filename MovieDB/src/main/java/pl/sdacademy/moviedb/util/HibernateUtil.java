package pl.sdacademy.moviedb.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

	private static final SessionFactory sf = new Configuration().configure().buildSessionFactory();

	private HibernateUtil() {
	}

	public static Session openSession() {
		Session s = null;
		if (sf.isOpen()) {
			s = sf.getCurrentSession();
		} else {
		s = sf.openSession();
	}
		return s;
	}
}
