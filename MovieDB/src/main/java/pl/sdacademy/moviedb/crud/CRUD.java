package pl.sdacademy.moviedb.crud;

import java.util.List;

public interface CRUD<T> {
	
	public void insertOrUpdate(T obj);
	public void delete(T obj);
	public T select(int id);
	public List<T> select();

}
