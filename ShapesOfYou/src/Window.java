import java.util.ArrayList;
import java.util.List;

public class Window {

	List<Shapes> ksztalty = new ArrayList<Shapes>();

	public void addShape(Shapes shape) {
		ksztalty.add(shape);
	}

	public void removeShape(Shapes shape) {
		ksztalty.remove(shape);
	}
	
	public void printAll() {
		for (Shapes ksztalt : ksztalty) {
				ksztalt.print();
			}
		}
	}


