
public class Point extends Shapes{

	
	
	public Point(double x, double y) {
		super(x, y);
		
	}

	@Override
	public void print() {
		System.out.println("Point [X= " + getX() + ", Y= " + getY() + "]");
	}
	
	
}