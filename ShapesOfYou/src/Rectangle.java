
public class Rectangle extends Shapes{
	private double height;
	private double width;

	public Rectangle(double x, double y, double height, double width) {
		super(x, y);
		this.height = height;
		this.width = width;
		
	}
	
	public double getHeight() {
		return height;
	}
	
	public double getWidth() {
		return width;
	}
	
	@Override
	public void print() {
		System.out.println("Rectangle [X= " + getX() + ", Y= " + getY() + ", Height= " + getHeight() + ", Width= " + getWidth()+ "]");
		
	}
	
	

}
