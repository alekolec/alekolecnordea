
public class Main {

	public static void main(String[] args) {
		
		Window okno = new Window();
		
		Shapes punkt1 = new Point(1,2);
		Shapes punkt2 = new Point(1.34, 2.45);
		Shapes punkt3 = new Point(2.456, 1.23445);
		
		Shapes prostokat1 = new Rectangle(1.3455, 2.35, 1.345, 3.4566);
		Shapes prostokat2 = new Rectangle(2.344, 1.23, 1.234, 2.334);
		
		okno.addShape(punkt1);
		okno.addShape(punkt2);
		okno.addShape(punkt3);
		okno.addShape(prostokat1);
		okno.addShape(prostokat2);
		
		okno.printAll();
		System.out.println(" ");
		System.out.println(" ");
		
		okno.removeShape(punkt2);
		
		okno.printAll();
		

	}

}
