package com.alekolec.hiber.dao;

import java.util.List;

public interface AbstractDAO <T>{
	boolean insert(T obj);
	boolean delete(T obj);
	boolean update(T obj);
	T get(long id);
	List<T> get();

}
