package com.alekolec.hiber.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StaleStateException;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.alekolec.hiber.entity.Animal;

public class AnimalDAO implements AbstractDAO<Animal> {
	
	SessionFactory sessionFactory;
	Session session;
	
	

	public AnimalDAO() {
		this.sessionFactory = new Configuration().configure().buildSessionFactory();
		this.session = sessionFactory.openSession();
	}

	public boolean insert(Animal obj) {
		Transaction t = session.beginTransaction();
		session.save(obj);
		t.commit();
		return obj.getId() != 0;
	}

	public boolean delete(Animal obj) {
		
			Transaction t = session.beginTransaction();
			session.delete(obj);
			t.commit();
		
		
		return true;
	}

	public boolean update(Animal obj) {
		try {
			Transaction t = session.beginTransaction();
		
		session.update(obj);
		t.commit();
		session.flush();
		} catch (StaleStateException ex) {
			ex.getMessage();
			return false;
		}
		return true;
	}

	public Animal get(long id) {
		Animal animal = session.load(Animal.class, id);
		
		return animal;
	}

	public List<Animal> get() {
		List<Animal> animals =  session.createQuery("from Animal").list();
		return animals;
	}

	public static void main(String[] args) {
		AnimalDAO dao = new AnimalDAO();
		Animal an = new Animal("nana");
		//dao.delete(an);
		dao.update(an);
	}
}
