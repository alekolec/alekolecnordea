package com.alekolec.hiber;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.alekolec.hiber.entity.Animal;
import com.alekolec.hiber.entity.AnimalType;

public class App {

	public static void main(String[] args) {
		System.out.println("Hello World!");
		Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		
		Transaction t = session.beginTransaction();
		
		AnimalType animalType = new AnimalType();
		animalType.setType("Mammal");
		
		Animal animal = new Animal();
		animal.setName("Wilk");
		
		List<Animal> anims = new LinkedList<>();
		anims.add(animal);
		
		animal.setAnimalType(animalType);
		
		session.save(animalType);
		session.save(animal);
		System.out.println(animal.getId());
		
		t.commit();
		
		AnimalType mammal=session.load(AnimalType.class, animalType.getId());
		System.out.println(mammal);
		System.out.println(mammal.getAnimals());	
		
		
		session.close();
		
		/*Animal animal =  new Animal("S�o�");
		
		//insert
		Session session = sessionFactory.openSession();
		Transaction t = session.beginTransaction();
		
	    session.save(animal);
	    session.save(new Animal("Kot"));
	    session.save(new Animal("Pies"));
		
		t.commit();
		
		
		//pobranie
		
		Animal myAnimal = session.load(Animal.class, 2L);
		System.out.println(myAnimal);
		
		
		//update
		t  = session.beginTransaction();
		myAnimal.setName("Super S�o�");
		session.save(myAnimal);
		t.commit();
		
		//pobierabue wielu
		List<Animal> animals = session.createQuery("from Animal").list();
		for(Animal anim : animals) {
			System.out.println(anim);
		}
		
		//delete
		t = session.beginTransaction();
		session.delete(myAnimal);
		
		t.commit();
			
		session.close();
		*/
		
		
	}

}
