package com.alekolec.hiber.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "animal")
public class Animal {
	@Id   //jak primaryKey
	@GeneratedValue(strategy = GenerationType.SEQUENCE)  //w jaki sposob ma sie generowac
	@Column(name="id")
	private long id;
	@Column(name="name")
	private String name;
	@ManyToOne(cascade = CascadeType.ALL)
	private AnimalType animalType;
	
	public Animal(){}

	public Animal(String name) {
		this.name = name;
	}

	public Animal(long id, String name) {
		this.id = id;
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public AnimalType getAnimalType() {
		return animalType;
	}
	
	public void setAnimalType(AnimalType animalType) {
		this.animalType = animalType;
	}

	@Override
	public String toString() {
		return "Animal [id=" + id + ", name=" + name + ", animalType=" + animalType + "]";
	}
	


	
	
}
