package pl.home.trainings.test.calc.basic;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SubtractTest extends BaseTest {


	@Test
	public void oneMinusZeroShouldReturnOne() {
		assertEquals("1", calculator.Subtract(1, 0));
	}
	
	@Test
	public void hundredMinusHundredShouldReturnZero() {
		assertEquals("0", calculator.Subtract(100, 100));
	}
	
	@Test
	public void tenMinusFourShouldReturnSix() {
		assertEquals("6", calculator.Subtract(10, 4));
	}
	
	@Test
	public void twoMinusOneShouldResultOne() {
		assertEquals("2", calculator.Subtract(2, 1));
	}

}
