import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import javax.print.attribute.standard.MediaSize.Other;

public class ClientRunnable implements Runnable {

	private PrintWriter writer;
	private BufferedReader reader;
	private IMessageReceiver receiver;
	
	private String hostName;
	private int port;
		

	public ClientRunnable(String hostName, int port, IMessageReceiver receiver) {
		super();
		this.hostName = hostName;
		this.port = port;
		this.receiver = receiver;
	}
	
	public void run() {
		try {
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Socket socket = new Socket(hostName, port);
			receiver.changeStatus("Connected!");
			
			OutputStream os = socket.getOutputStream();
			writer = new PrintWriter(os);

			reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String orzymanaWiadomosc = null;
			do {
				orzymanaWiadomosc = reader.readLine();
				System.out.println(orzymanaWiadomosc);
				receiver.messageReceived(orzymanaWiadomosc);
				receiver.changeStatus("New message!");

			} while (orzymanaWiadomosc != null);

			writer.close();
			reader.close();
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
			ioe.printStackTrace();
		}

	}
	public void sendMessage(String tresc){
		writer.println(tresc);
		writer.flush();
		receiver.changeStatus("Message sent!");
	}

}
