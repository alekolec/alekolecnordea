
public interface IMessageReceiver {

	void messageReceived(String msg);
	void changeStatus(String status);
}
