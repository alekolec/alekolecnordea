package calculator;

import static org.junit.Assert.assertEquals;

import org.junit.Ignore;
import org.junit.Test;

public class KataCalcTest {

	@Ignore
	//@Test(expected = RuntimeException.class)
	public void threeParametersShouldReturnException(){
		KataCalc.add("1,2,3");
	}
	
	@Test
	public void twoParametersShouldBeHandled(){
		KataCalc.add("1,2");
	}
	
	@Ignore
	public void emptyStringShouldReturnZero(){
		assertEquals(0, KataCalc.add(""));
	}
	
	@Test (expected = RuntimeException.class)
	public void emptyStringShouldReturnException(){
		KataCalc.add("");
	}
	
	@Test
	public void twoPlusThreeShouldBeFive(){
		assertEquals(5, KataCalc.add("2,3"));
	}
	
	@Test
	public void twoPlusNothingShouldBeTwo(){
		assertEquals(2, KataCalc.add("2"));
	}
	
	@Test
	public void twoNewLineAndFourShouldReturnSix(){
		assertEquals(6, KataCalc.add("2\n4"));
	}
	@Test
	public void AttwoDolarAndFourShouldReturnSix(){
		assertEquals(6, KataCalc.add("@2$4"));
	}
	
	@Test
	public void oneMinusNumberShouldReturnException(){
		try {
			KataCalc.add("@2$4*-2");
		} catch (Exception e) {
			assertEquals("Negatives not allowed: [-2]", e.getMessage());
			return;
			// TODO: handle exception
		}
	}
	
	@Test
	public void fewMinusNumberShouldReturnExceptionWithList(){
		try {
			KataCalc.add("@2$4*-2^-3");
			//KataCalc.add("");
		} catch (Exception e) {
			assertEquals("Negatives not allowed: [-2, -3]", e.getMessage());
			return;
			// TODO: handle exception
		}
	}
	
	@Test
	public void thousandShouldBeIgnored(){
		assertEquals(8, KataCalc.add("@2$4&1000^2"));
	}
}
