package calculator;

import java.util.ArrayList;
import java.util.List;

public class KataCalc {

	/**
	 * adding two number
	 * 
	 * @param numbers
	 *            parameters splited by comma, maximum amount of parameters is
	 *            two, minimum zero
	 * @return sum of numbers
	 * @throws runtimeException
	 *             when incorect amount of numbers
	 */

	public static int add(String numbers) {
		if (numbers.isEmpty()) {
			throw new RuntimeException("Method work only on filled string");
		}

		String[] splitedLine = numbers.split("[^0-9|-]");

		List<Integer> minusNumbers = new ArrayList<>();

		// SEARCH MINUS NUMBER
		for (String string : splitedLine) {
			if (!string.isEmpty() && Integer.parseInt(string) < 0) {
				minusNumbers.add(Integer.parseInt(string));
			}
		}
		// THROW EXCEPTION WITH LIST
		if (!minusNumbers.isEmpty()) {
			throw new RuntimeException("Negatives not allowed: " + minusNumbers);
		}

		int sum = 0;
		for (String string : splitedLine) {
			if (!string.isEmpty()) {
				if (Integer.parseInt(string) < 1000) {
					sum += Integer.parseInt(string);
				}
			}
		}
		return sum;
	}
}