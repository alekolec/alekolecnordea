package pl.alekolec.moviedatabase.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Movie {
	@Id
	@GeneratedValue
	private long id;
	private String title;
	private long year;
	private String genre;
	private String image;
	
	public Movie(){}
	
	
	public Movie(long id, String title, long year, String genre) {
		super();
		this.id = id;
		this.title = title;
		this.year = year;
		this.genre = genre;
	}


	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public long getYear() {
		return year;
	}
	public void setYear(long year) {
		this.year = year;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	
	public void setImage(String imgURL) {
		this.image = imgURL;
	}
	
	public String getImage() {
		return image;
	}


	@Override
	public String toString() {
		return "Movie [id=" + id + ", title=" + title + ", year=" + year + ", genre=" + genre + "]";
	}
	
	

}
