package pl.alekolec.moviedatabase.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.Transaction;

import pl.alekolec.moviedatabase.entity.Movie;
import pl.alekolec.moviedatabase.util.HibernateUtil;

/**
 * Servlet implementation class DeleteMovieServlet
 */
public class DeleteMovieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteMovieServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		long id = Long.valueOf(request.getParameter("id"));
		
		Session session = HibernateUtil.openSession();
		Transaction t = session.beginTransaction();
		Movie movie = session.load(Movie.class, id); 
		session.delete(movie);
		t.commit();
		response.sendRedirect("./ViewServlet");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
