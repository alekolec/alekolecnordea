package pl.alekolec.moviedatabase.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.Transaction;

import pl.alekolec.moviedatabase.entity.Movie;
import pl.alekolec.moviedatabase.util.HibernateUtil;

/**
 * Servlet implementation class MovieServlet
 */
public class MovieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MovieServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		request.setCharacterEncoding("utf-8");
		String title = request.getParameter("title");
		long year = Long.valueOf(request.getParameter("year"));
		String genre = request.getParameter("genre");
		String image = request.getParameter("image");
		
		Movie movie = new Movie();
		movie.setTitle(title);
		movie.setYear(year);
		movie.setGenre(genre);
		movie.setImage(image);
		
		Session session = HibernateUtil.openSession();
		Transaction t = session.beginTransaction();
		session.save(movie);
		t.commit();
		
		session.close();
		request.setAttribute("msg", "Film zosta� dodany poprawnie");
		request.getRequestDispatcher("add.jsp").forward(request, response);
	}

}
