package pl.alekolec.moviedatabase.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.Transaction;

import pl.alekolec.moviedatabase.entity.Movie;
import pl.alekolec.moviedatabase.util.HibernateUtil;

/**
 * Servlet implementation class EditMovieServlet
 */
public class EditMovieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditMovieServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		long id = Long.valueOf(request.getParameter("id"));
		
		Session session = HibernateUtil.openSession();
		Movie movie = session.load(Movie.class, id); 
		request.setAttribute("movie", movie);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
		session.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
				//doGet(request, response);
				request.setCharacterEncoding("utf-8");
				long id = Long.valueOf(request.getParameter("id"));
				String title = request.getParameter("title");
				long year = Long.valueOf(request.getParameter("year"));
				String genre = request.getParameter("genre");
				String image = request.getParameter("image");
				Session session = HibernateUtil.openSession();
				Movie movie = session.get(Movie.class, id);
				
				
				movie.setTitle(title);
				movie.setYear(year);
				movie.setGenre(genre);
				movie.setImage(image);
				
				
				Transaction t = session.beginTransaction();
				session.update(movie);
				t.commit();
								
				session.close();
				request.setAttribute("msg", "Film zosta� zaktualizowany");
				request.getRequestDispatcher("./ViewServlet").forward(request, response);
	}

}
