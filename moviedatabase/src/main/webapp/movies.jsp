<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:import url="header.jsp" />
<div class="well">


<div class="row">
<div class="col-md-3">
   
    <img class="img-responsive img-rounded" src="${movie.image}" alt="Poster">
</div>
    
    <div class="col-md-9">
        
        <h2 class="card-title">${movie.title}</h2>
        
        <p class="card-text">Rok produkcji: ${movie.year}, Genre: ${movie.genre}</p>
        <br>
        <p class="card-text">Genre: ${movie.genre}</p>
        <br>
        <a href="./ViewServlet" class="btn btn-primary">Cofnij</a>
    </div>

</div>
</div>
	
<c:import url="footer.jsp" />