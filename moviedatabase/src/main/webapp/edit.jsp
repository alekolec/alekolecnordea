<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:import url="header.jsp" />

<c:if test="${msg ne null}">
<div class="alert alert-success">
	<p>${msg}</p>
</div>
</c:if>

<form action="./EditMovieServlet" method="POST">
	<input type="hidden" name="id" value="${movie.id}">
	<div class="input-group">
	  <span class="input-group-addon" id="basic-addon1">Tytuł: </span>
	  <input type="text" name="title" class="form-control" value="${movie.title}" aria-describedby="basic-addon1">
	</div>
		<br>
	<div class="input-group">
	  <span class="input-group-addon" id="basic-addon1">Rok produkcji: </span>
	  <input type="text" name="year" class="form-control" value="${movie.year}" aria-describedby="basic-addon1">
	</div>
		<br>
	<div class="input-group">
	  <span class="input-group-addon" id="basic-addon1">Gatunek: </span>
	  <input type="text" name="genre" class="form-control" value="${movie.genre}" aria-describedby="basic-addon1">
	</div>
		<br>
		<div class="input-group">
	  <span class="input-group-addon" id="basic-addon1">Obrazek: </span>
	  <input type="text" name="image" class="form-control" value="${movie.image}" aria-describedby="basic-addon1">
	</div>
		<br>
	
	<button class="btn btn-success center-block">Zatwierdź</button>
	<br>
</form>
			
<c:import url="footer.jsp" />