<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:import url="header.jsp" />
<c:if test="${msg ne null}">
<div class="alert alert-success">
	<p>${msg}</p>
</div>
</c:if>
<div class="well">

<table class="table">
	<thead>
		<tr>
			<th>#</th>
			<th>Tytuł filmu</th>
			<th>Rok produkcji</th>
			<th>Gatunek</th>
			<th>Akcje</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${movies}" var="movie">
			<tr>
				 <td>${movie.id}</td>
				 <td><a href="./SingleMovieServlet?id=${movie.id}">${movie.title}</a></td>
				 <td>${movie.year}</td>
				 <td>${movie.genre}</td>
				 <td><a href="./EditMovieServlet?id=${movie.id}" class="btn btn-primary">Edytuj</a>&nbsp;<a href="./DeleteMovieServlet?id=${movie.id}" class="btn btn-primary">Usuń</a>
			</tr>	 
		</c:forEach>
	</tbody>
	</table>
</div>			
<c:import url="footer.jsp" />

