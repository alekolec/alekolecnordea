import org.junit.Test;

import com.alekolec.dp.car.CarFactory;
import com.alekolec.dp.car.ICar;

import org.junit.*;

public class AbstractFactoryTest {
	
	@Test
	public void test1() {
		
		ICar car = CarFactory.createBMWM3();
		
		Assert.assertEquals(250, car.getMaxSpeed(),0);
		Assert.assertEquals(0.8, car.getAccel(),0);
	}

}
