package com.alekolec.dp.car;

public interface ICar {
	
	void drive();
	double getAccel();
	double getMaxSpeed();

}
