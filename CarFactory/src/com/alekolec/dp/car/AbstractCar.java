package com.alekolec.dp.car;

class AbstractCar implements ICar {

	private String brand;
	private double maxSpeed;
	private double accel;
	
	public AbstractCar(String brand, double maxSpeed, double accel) {
		super();
		this.brand = brand;
		this.maxSpeed = maxSpeed;
		this.accel = accel;
	}
	
	public String getBrand() {
		return brand;
	}
	
	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	public double getMaxSpeed() {
		return maxSpeed;
	}
	
	public void setMaxSpeed(double maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	
	public double getAccel() {
		return accel;
	}
	
	public void setAccel(double accel) {
		this.accel = accel;
	}

	@Override
	public void drive() {
		System.out.println("Samochod " + brand + " o maxSpeed: " + maxSpeed + " jedzie sobie");
		
	}
	
	
	
}
