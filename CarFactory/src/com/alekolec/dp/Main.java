package com.alekolec.dp;

import com.alekolec.dp.car.CarFactory;
import com.alekolec.dp.car.ICar;

public class Main {

	public static void main(String[] args) {

		ICar car = CarFactory.createBMWM3();
		ICar carCivic = CarFactory.createHondaCivic();

		car.drive();
		carCivic.drive();
	}

}
