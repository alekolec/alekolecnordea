
public class Porownanie {

	public static void main(String[] args) {

		int[] array1 = new int[3];
		array1[0] = 1;
		array1[1] = 2;
		array1[2] = 3;

		int[] array2 = new int[2];
		array2[0] = 1;
		array2[1] = 2;

		
		if (compare(array1, array2) == true){
			System.out.println("true");
		} else System.out.println("false");

	}

	private static boolean compare(int[] t1, int[] t2) {
		boolean compareResult = true;
		int sum = 0;

		if (t1.length != t2.length) {
			return false;
		}

			for (int i = 0; i < t1.length; i++) {
				if (t1[i] == t2[i]) {
					sum = sum + 1;
				}

			}
		
			
		compareResult = (sum == t1.length);
		
		return compareResult;

	}

}
