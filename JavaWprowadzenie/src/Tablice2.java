import java.util.Scanner;

public class Tablice2 {

	public static void main(String[] args) {
		

		int[] t1 = { 1, 3, 5, 10 };

		System.out.println(t1[0]);
		System.out.println(t1[1]);
		System.out.println(t1[2]);
		System.out.println(t1[3]);

		for (int i = 0; i < t1.length; i++) {
			System.out.println(t1[i]);
		}

		for (int i = t1.length - 1; i >= 0; i--) {
			System.out.println(t1[i]);
		}

		for (int i : t1) {
			System.out.println(i);
		}
	}

}
