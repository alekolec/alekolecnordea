
public class TablicaImion {

	public static void main(String[] args) {

		String[] imiona = { "Jan", "Adam", "Ewa", "Angelika", "Sara", "Stefan" };
		
		System.out.println("Po kolei wszystkie:");
		for (int i = 0; i < imiona.length; i++) {
			System.out.println(imiona[i]);
		}

		System.out.println("Po kolei wszystkie for each:");
		for (String i : imiona) {
			System.out.println(i);
		}

		System.out.println("Co drugie:");
		for (int i = 0; i < imiona.length; i += 2) {
			System.out.println(imiona[i]);
		}
		
		System.out.println("Co drugie inaczej:");
		for(int i = 0; i <imiona.length; i++) {
			if(i%2==0) {
				System.out.println(imiona[i]);
			}
		}

		System.out.println("Imiona na A:");
		for (int i = 0; i < imiona.length; i++) {
			if (imiona[i].charAt(0) == 'A') {
				System.out.println(imiona[i]);
			}
		}
		
		System.out.println("Imiona na A inaczej:");
		for (String imie: imiona) {
			if (imie.startsWith("A")) {
				System.out.println(imie);
			}
		}


	}

}
