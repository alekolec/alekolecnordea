package com.alekolec.sda.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class Zgadywanka {

	private JFrame frame;
	private JTextField textField;
	private int liczba;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Zgadywanka window = new Zgadywanka();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Zgadywanka() {
		initialize();
		Random random = new Random();
		liczba = random.nextInt(10);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel centerPanel = new JPanel();
		frame.getContentPane().add(centerPanel, BorderLayout.CENTER);
		centerPanel.setLayout(new GridLayout(4, 1, 0, 0));
		
		JLabel lblNewLabel = new JLabel("ZGADYWANKA");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		centerPanel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		centerPanel.add(lblNewLabel_1);
		
		textField = new JTextField();
		centerPanel.add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("SPRAWD\u0179");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String liczbaText = Integer.toString(liczba);
				if(textField.getText().equals(liczbaText)) {
					lblNewLabel_1.setText("WYGRA�E�");
				}else {
					lblNewLabel_1.setText("PRZEGRA�E�");
				}
			}
		});
		centerPanel.add(btnNewButton);
	}

}
