package com.alekolec.sda.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.JSplitPane;
import javax.swing.BoxLayout;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.awt.event.ActionEvent;

public class MemorySizeBetter {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private List<Integer> list = new ArrayList<>();
	private JLabel label_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MemorySizeBetter window = new MemorySizeBetter();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MemorySizeBetter() {
		initialize();
	}
	
	public List<Integer> getList() {
		return list;
	}
	
	public void add(int amount) {
		for (int i = 0; i < amount; i++) {
			Random random = new Random();
			list.add(random.nextInt(10000));
		}
	}
	
	public void recurrentExecuteMethod(int deep) {
		System.out.println("Wywo�uj� si� " + deep + "razy");
		label_3.setText("Dotarlismy do: " + deep);
		if (deep == 1) {
			return;
		} else {
			recurrentExecuteMethod(deep - 1);
		}
//		}catch(StackOverflowError e) {
//			label_3.setText("Dotarlismy do: " + deep);
//		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 598, 355);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel CenterPanel = new JPanel();
		frame.getContentPane().add(CenterPanel, BorderLayout.CENTER);
		CenterPanel.setLayout(new GridLayout(1, 2, 0, 0));

		JPanel LeftPanel = new JPanel();
		CenterPanel.add(LeftPanel);
		LeftPanel.setLayout(new GridLayout(2, 1, 0, 0));

		JPanel panel_1 = new JPanel();
		LeftPanel.add(panel_1);
		panel_1.setLayout(new GridLayout(1, 2, 0, 0));

		JPanel panel_2 = new JPanel();
		panel_1.add(panel_2);
		panel_2.setLayout(new GridLayout(3, 1, 0, 0));

		JLabel lblNewLabel = new JLabel("Dost\u0119pna wolna pami\u0119\u0107");
		panel_2.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Maksymalna pamie\u0107");
		panel_2.add(lblNewLabel_1);

		JLabel lblZajmowanaPami = new JLabel("Zajmowana pami\u0119\u0107");
		panel_2.add(lblZajmowanaPami);

		JPanel panel_3 = new JPanel();
		panel_1.add(panel_3);
		panel_3.setLayout(new GridLayout(3, 1, 0, 0));

		JLabel label = new JLabel("");
		panel_3.add(label);

		JLabel label_1 = new JLabel("");
		panel_3.add(label_1);

		JLabel label_2 = new JLabel("");
		panel_3.add(label_2);

		label_3 = new JLabel("");
		LeftPanel.add(label_3);

		JPanel RightPanel = new JPanel();
		CenterPanel.add(RightPanel);
		RightPanel.setLayout(new GridLayout(7, 1, 0, 0));

		JLabel lblCoChceszZrobi = new JLabel("Co chcesz zrobi\u0107?");
		lblCoChceszZrobi.setHorizontalAlignment(SwingConstants.CENTER);
		RightPanel.add(lblCoChceszZrobi);

		JPanel panel = new JPanel();
		RightPanel.add(panel);
		panel.setLayout(new GridLayout(0, 2, 0, 0));

		JButton btnNewButton = new JButton("ADD");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int liczba = Integer.parseInt(textField.getText());
				add(liczba);

			}
		});
		panel.add(btnNewButton);

		textField = new JTextField();
		panel.add(textField);
		textField.setColumns(10);

		JButton btnNewButton_1 = new JButton("PRINT SPACE");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				long textAvailable = Runtime.getRuntime().freeMemory();
				label.setText(Long.toString(textAvailable));

				long textMaximum = Runtime.getRuntime().maxMemory();
				label_1.setText(Long.toString(textMaximum));

				long textUsed = Runtime.getRuntime().totalMemory();
				label_2.setText(Long.toString(textUsed));

			}
		});
		RightPanel.add(btnNewButton_1);

		JButton btnNewButton_2 = new JButton("CLEAR");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getList().clear();
			}
		});
		RightPanel.add(btnNewButton_2);

		JButton btnNewButton_3 = new JButton("COLLECT");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Runtime.getRuntime().gc();
			}
		});
		RightPanel.add(btnNewButton_3);

		JPanel panel_4 = new JPanel();
		RightPanel.add(panel_4);
		panel_4.setLayout(new GridLayout(0, 2, 0, 0));

		JButton btnRecursive = new JButton("RECURSIVE");
		btnRecursive.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int liczba = Integer.parseInt(textField_1.getText());
				recurrentExecuteMethod(liczba);

			}
		});
		panel_4.add(btnRecursive);

		textField_1 = new JTextField();
		panel_4.add(textField_1);
		textField_1.setColumns(10);
	}

}
