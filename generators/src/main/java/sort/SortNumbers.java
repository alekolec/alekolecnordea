package sort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortNumbers {
	
	public static void main(String[] args) {
		
		List<Integer> numbers = new ArrayList<>();
		numbers.add(4);
		numbers.add(5);
		numbers.add(1);
		numbers.add(3);
		numbers.add(7);
		
		
		System.out.println("Liczby");
		
		for(int i : numbers) {
			System.out.println(i);
		}
		
		
		Collections.sort(numbers);
		System.out.println("Liczby posortowane");
		for(int i : numbers) {
			System.out.println(i);
		}
		
		Collections.shuffle(numbers);
		System.out.println("Liczby ponownie przemieszane");
		for(int i : numbers) {
			System.out.println(i);
		}
		
	}

}
