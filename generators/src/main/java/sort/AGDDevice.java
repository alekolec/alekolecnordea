package sort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AGDDevice implements Comparable<AGDDevice> {
	private String model;
	private int price;

	public AGDDevice(String model, int price) {

		this.model = model;
		this.price = price;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	@Override
	public int compareTo(AGDDevice o) {
		if (price > o.price) {
			return 1;
		}
		if (price < o.price) {
			return -1;
		}
		return 0;
	}

	public static void main(String[] args) {

		List<AGDDevice> sprzety = new ArrayList<>();
		sprzety.add(new AGDDevice("Pralka", 1200));
		sprzety.add(new AGDDevice("Suszarka", 99));
		sprzety.add(new AGDDevice("Telewizor", 3200));
		sprzety.add(new AGDDevice("Zmywarka", 900));
		sprzety.add(new AGDDevice("Prostownica", 87));
		sprzety.add(new AGDDevice("Ekspres", 320));

		System.out.println("Sprzety:");

		for (AGDDevice sprzet : sprzety) {
			System.out.println(sprzet.getModel() + " " + sprzet.getPrice());
		}
		System.out.println("");
		Collections.sort(sprzety);
		System.out.println("Posortowane sprz�ty");
		for (AGDDevice sprzet : sprzety) {
			System.out.println(sprzet.getModel() + " " + sprzet.getPrice());
		}

		System.out.println("");
		System.out.println("Dro�szy z dwoch");
		System.out.println(max(sprzety.get(2), sprzety.get(4)).getModel());
		
		
		Comparator<AGDDevice> comparator = new Comparator<AGDDevice> () {
			
			@Override
			public int compare(AGDDevice o1, AGDDevice o2) {
			
				return o1.model.compareTo(o2.model);
			}
		};
		
		System.out.println("");
		Collections.sort(sprzety, comparator);
				
		System.out.println("Posortowane sprz�ty");
		for (AGDDevice sprzet : sprzety) {
			System.out.println(sprzet.getModel() + " " + sprzet.getPrice());
		}
		
	}

	public static AGDDevice max(AGDDevice a1, AGDDevice a2) {
		if (a1.price > a2.price) {
			return a1;
		} else if (a1.price < a2.price) {
			return a2;
		} else {
			return null;

		}
	}
}