package sort;

public class ObjectMath {

//	public static <T> T get(T i) {
//		return i;
//	}

	public static <T extends Comparable<T>> T max(T o1, T o2) {

		if (o1.compareTo(o2) > 0) {
			return o1;
		} else if (o1.compareTo(o2) < 0) {
			return o2;
		}
		return null;
	}

	public static void main(String[] args) {
		

		AGDDevice agdDevice1 = new AGDDevice("Pralka", 1000);
		AGDDevice agdDevice2 = new AGDDevice("Telewizor", 2000);

		System.out.println(max(agdDevice1, agdDevice2).getModel());

	}
}
