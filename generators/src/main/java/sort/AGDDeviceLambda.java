package sort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AGDDeviceLambda {

	public static void main(String[] args) {
		
		List<AGDDevice> devices = new ArrayList<>();
		devices.add(new AGDDevice("Pralka", 1200));
		devices.add(new AGDDevice("Suszarka", 99));
		devices.add(new AGDDevice("Telewizor", 3200));
		devices.add(new AGDDevice("Zmywarka", 900));
		devices.add(new AGDDevice("Prostownica", 87));
		devices.add(new AGDDevice("Ekspres", 320));
		
		System.out.println("Sprzety");
		
		
		Collections.sort(devices, (o1,o2) -> {
			return o1.getModel().compareTo(o2.getModel());
		});
		
		for(AGDDevice agdDevice : devices) {
			System.out.println(agdDevice.getModel());
		}
	}
	
}
