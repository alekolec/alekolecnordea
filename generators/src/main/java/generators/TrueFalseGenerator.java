package generators;

public class TrueFalseGenerator {

	int counter = 0;

	public boolean generate() {
		counter++;
		if (counter % 2 == 0)
			return false;
		return true;
	}

	public static void main(String[] args) {
		TrueFalseGenerator generator = new TrueFalseGenerator();

		for (int i = 0; i < 10; i++) {
			System.out.println(generator.generate());
		}

	}

}
