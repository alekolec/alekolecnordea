package generators;

import java.util.Random;

public class BoundRandomNumbers {
	private int min;
	private int range;

	private Random random = new Random();

	public BoundRandomNumbers(int min, int max) {
		this.min = min;
		range = max - min + 1;

	}

	public int generate() {
	
		return random.nextInt(range) + min;
	}

	public static void main(String[] args) {

		BoundRandomNumbers generator = new BoundRandomNumbers(6, 10);

		for (int i = 0; i < 10; i++) {
			System.out.println(generator.generate());
		}

	}

}
