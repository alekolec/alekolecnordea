package generators;

import java.util.Random;

public class RandomLettersGenerator {
	
	private Random random = new Random();
		
	private static final int RANGE = 'z' - 'a';
	
	public char generate() {
	
		return (char)(random.nextInt(RANGE)+'a') ;
	}

	
	public static void main(String[] args) {
		
		RandomLettersGenerator generator = new RandomLettersGenerator();
		
		for (int i = 0; i < 10; i++) {
			System.out.println(generator.generate());
		}
		
	}
}
