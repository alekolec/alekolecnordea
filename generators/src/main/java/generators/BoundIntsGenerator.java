package generators;

public class BoundIntsGenerator {
	private int max;
	int generateNumber;

	public BoundIntsGenerator(int min, int max) {
		this.generateNumber = min;
		this.max = max;
	}

	public int generate() {

		// if (generateNumber >= this.min && generateNumber < this.max)

		// tutaj mo�na rzuci� wyj�tek je�li jest poza zakresem - odwrotny if

		return generateNumber++;
	}

	public boolean hasNext() {

		return generateNumber <= this.max;

	}

	public static void main(String[] args) {
		BoundIntsGenerator generator = new BoundIntsGenerator(6, 10);

		while (generator.hasNext()) {

			System.out.println(generator.generate());
		}

	}

}
