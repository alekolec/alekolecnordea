package generators.iter;

public interface Generator<GeneratedValueType> {
	
	GeneratedValueType generate();

}
