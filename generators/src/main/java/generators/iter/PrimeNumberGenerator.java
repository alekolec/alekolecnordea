package generators.iter;

public class PrimeNumberGenerator implements Generator<Integer> {

	int generatedNumber = 2;

	public Integer generate() {

		do{
			generatedNumber++;
		}
		while (!isPrime(generatedNumber));
			
		return generatedNumber;
	}

	public boolean isPrime(int n) {
		if (n < 2) {
			return false;
		}
		for (int i = 2; i < n; i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;

	}
	
	public static void main(String[] args) {
				
		GeneratorsOperations.printTenElements(new PrimeNumberGenerator());
		
	}

}