package generators.iter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class GeneratorsOperations {

	public static void printTenElements(Generator generator) {
		for (int i = 0; i < 10; i++) {
			System.out.println(generator.generate());
		}
	}

	public static int sum(Generator<Integer> generator) {
		int sum = 0;
		Integer generatedNumber;

		while (true) {
			generatedNumber = generator.generate();

			if (generatedNumber != null) {
				sum += generatedNumber;
			} else {
				break;
			}
		}

		return sum;
	}

	public static int sum(Generator<Integer> generator, int amount) {
		int sum = 0;

		for (int i = 0; i < amount; i++) {
			sum += generator.generate();
		}

		return sum;
	}
	
	public static void save(Generator generator) throws FileNotFoundException {
		FileOutputStream output = new FileOutputStream("10elementow.txt");
		
	}

}
