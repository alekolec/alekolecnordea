package generators.iter;

import java.util.Iterator;

public class RandomWordsGenerator implements Iterable<String> {

	// ZALEŻNOŚCI, DEPENDENCIES
	private Iterable<Integer> wordLenghtGenerator;
	private Iterable<Character> charGenerator;

	public RandomWordsGenerator(int min, int max, int amount) {

		wordLenghtGenerator = new BoundRandomNumbers(min, max, amount);
		charGenerator = new RandomLettersGenerator(Integer.MAX_VALUE);

	}
	
	public RandomWordsGenerator(Iterable<Integer> wordLengthGenerator, Iterable<Character> charGenerator) {

		this.wordLenghtGenerator = wordLengthGenerator;
		this.charGenerator = charGenerator;

	}


	public Iterator<String> iterator() {
		// TODO Auto-generated method stub
		return new RandomWordsIterator();
	}
	
	class RandomWordsIterator implements Iterator<String> {

		Iterator<Integer> lenghts = wordLenghtGenerator.iterator();
		
		public boolean hasNext() {
			return lenghts.hasNext();
		}

		public String next() {
			int dlugosc = lenghts.next();
			String napis = "";

			Iterator<Character> iterator = charGenerator.iterator();
			for (int i = 0; i <= dlugosc; i++) {
				napis += iterator.next();

			}
			return napis;
		}
		
	}

	public static void main(String[] args) {
		
		RandomWordsGenerator generator = new RandomWordsGenerator(3, 5, 7);
		
		for(String i : generator) {
			System.out.println(i);
		}

		

	}

}
