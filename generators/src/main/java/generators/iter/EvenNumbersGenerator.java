package generators.iter;

import java.util.Iterator;

public class EvenNumbersGenerator implements Iterable<Integer> {

	private final int amount;

	public EvenNumbersGenerator(int amount) {
		this.amount = amount;
	}

	public Iterator<Integer> iterator() {

		return new EvenNumbersIterator();
	}

	class EvenNumbersIterator implements Iterator<Integer> {

		private int generateNumber = 0;
		private int counter;

		public boolean hasNext() {

			return counter < amount;
		}

		public Integer next() {
			generateNumber = generateNumber + 2;
			counter++;
			return generateNumber;
		}
	}

	public static void main(String[] args) {

		EvenNumbersGenerator generator = new EvenNumbersGenerator(4);

		for (int i : generator) {
			System.out.println(i);
		}

		for (int i : generator) {
			System.out.println(i);
		}

	}

}
