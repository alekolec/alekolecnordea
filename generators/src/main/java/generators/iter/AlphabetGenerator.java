package generators.iter;

import java.util.Iterator;

public class AlphabetGenerator implements Iterable<Character> {

	private int amount;

	public AlphabetGenerator(int amount) {
		this.amount = amount;
	}

	public Iterator<Character> iterator() {

		return new AlphabetIterator();
	}

	class AlphabetIterator implements Iterator<Character> {
		private char litera = 'a';
		private int counter;

		public boolean hasNext() {

			return counter < amount;
		}

		public Character next() {
			if (litera > 'z') {
				litera = 'a';
			}
			counter++;
			return litera++;
		}

	}

	public static void main(String[] args) {

		AlphabetGenerator generator = new AlphabetGenerator(5);

		for (char i : generator) {
			System.out.println(i);
		}
		
		System.out.println("");
		
		for (char i : generator) {
			System.out.println(i);
		}

	}
}
