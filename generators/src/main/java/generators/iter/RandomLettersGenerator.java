package generators.iter;

import java.util.Iterator;
import java.util.Random;

public class RandomLettersGenerator implements Iterable<Character>{
	
	private int amount;	
	
	
		
	public RandomLettersGenerator(int amount) {
		this.amount = amount;
	}


	public Iterator<Character> iterator() {
		
		return new RandomLetterIterator();
	}
	
	class RandomLetterIterator implements Iterator<Character> {
		private Random random = new Random(11);
		
		private int counter = 0;
		private static final int RANGE = 'z' - 'a';
		
		public boolean hasNext() {
			
			return counter<amount;
		}
		
		public Character next() {
			counter++;
			return (char)(random.nextInt(RANGE)+'a') ;
		}
	}

	
	public static void main(String[] args) {

		RandomLettersGenerator generator = new RandomLettersGenerator(5);
		
		for(int i : generator) {
			System.out.println(i);
		}
		
		for(int i : generator) {
			System.out.println(i);
		}
		
	}
}
