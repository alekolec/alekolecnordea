package generators.iter;

import java.util.Iterator;

public class GeometricSerieGenerator implements Iterable<Integer>{
	private int roznica;
	private int liczbaPoczatkowa;
	private int amount;
	
	
	public GeometricSerieGenerator(int liczbaPoczatkowa, int roznica, int amount) {
		this.roznica = roznica;
		this.liczbaPoczatkowa = liczbaPoczatkowa;
		this.amount = amount;
	}
		
	public Iterator<Integer> iterator() {
		
		
		return new GeometricSerieIterator();
	}
	
	class GeometricSerieIterator implements Iterator<Integer> {
		int generateNumber = liczbaPoczatkowa;
		private int counter;
		
		public boolean hasNext() {
			
			return counter<amount;
		}

		public Integer next() {
			generateNumber *=roznica;
			counter++;
			return generateNumber/roznica;
		}
		
	}
	
	public static void main(String[] args) {
		
		GeometricSerieGenerator generator = new GeometricSerieGenerator(3, 3, 5);
		
		
	

		
		
	}

	
	
}
