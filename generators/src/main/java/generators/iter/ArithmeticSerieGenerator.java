package generators.iter;

import java.util.Iterator;

public class ArithmeticSerieGenerator implements Iterable<Integer> {
	private int roznica;
	private int liczbaPoczatkowa;
	private int amount;

	public ArithmeticSerieGenerator(int liczbaPoczatkowa, int roznica, int amount) {
		this.liczbaPoczatkowa = liczbaPoczatkowa;
		this.roznica = roznica;
		this.amount = amount;
	}

	public Iterator<Integer> iterator() {

		return new ArithmeticSerieIterator();
	}

	class ArithmeticSerieIterator implements Iterator<Integer> {
		private int counter;
		private int generateNumber = liczbaPoczatkowa;

		public boolean hasNext() {
		
			return counter<amount;
		}

		public Integer next() {
			generateNumber += roznica;
			counter++;
			return generateNumber - roznica;
		}
	}

	public static void main(String[] args) {
		
		ArithmeticSerieGenerator generator = new ArithmeticSerieGenerator(2, 3, 5);
		
		for(int i : generator) {
			System.out.println(i);
		}
		
		System.out.println("");
		
		for(int i : generator) {
			System.out.println(i);
		}

	}

}
