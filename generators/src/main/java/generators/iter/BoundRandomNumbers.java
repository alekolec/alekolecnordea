package generators.iter;

import java.util.Iterator;
import java.util.Random;

public class BoundRandomNumbers implements Iterable<Integer> {
	private int min;
	private int range;
	private int amount;

	public BoundRandomNumbers(int min, int max, int amount) {
		this.min = min;
		this.amount = amount;
		range = max - min + 1;
	}

	public Iterator<Integer> iterator() {
	
		return new BoundRandomNumbersIterator();
	}

	class BoundRandomNumbersIterator implements Iterator<Integer> {

		private Random random = new Random(10);

		private int counter;

		public boolean hasNext() {
			return counter < amount;
		}

		public Integer next() {
			counter++;
			return random.nextInt(range) + min;
		}

	}

	public static void main(String[] args) {

		BoundRandomNumbers generator = new BoundRandomNumbers(3, 8, 5);
		
		for(int i : generator) {
			System.out.println(i);
		}
		
		System.out.println("");
		
		for(int i : generator) {
			System.out.println(i);
		}
		
	}

}
