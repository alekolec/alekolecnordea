package generators.iter;

import java.util.Iterator;

public class BoundIntsGenerator implements Iterable<Integer> {
	private int max;
	private int min;

	public BoundIntsGenerator(int min, int max) {
		this.min = min;
		this.max = max;
	}

	public Iterator<Integer> iterator() {

		return new BoundIntsIterator();
	}

	class BoundIntsIterator implements Iterator<Integer> {
		private int generateNumber = min;
		
		public boolean hasNext() {

			return generateNumber <= max;
		}

		public Integer next() {
			return generateNumber++;
		}
	}

	public static void main(String[] args) {

		BoundIntsGenerator generator = new BoundIntsGenerator(6, 10);

		for (int i : generator) {
			System.out.println(i);
		}
		
		for (int i : generator) {
			System.out.println(i);
		}
	}
}
