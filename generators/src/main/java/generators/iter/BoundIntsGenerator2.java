package generators.iter;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class BoundIntsGenerator2 implements Iterable<Integer> {
	private List<Integer> numbers = new LinkedList<Integer>();

	public BoundIntsGenerator2(int min, int max) {	
		for(int i = min ; i<= max; i++) {
			numbers.add(i);
		}
	}

	public Iterator<Integer> iterator() {
		return numbers.iterator();
	}

	public static void main(String[] args) {

		BoundIntsGenerator2 generator = new BoundIntsGenerator2(6, 10);
		
		for (int i : generator) {
			System.out.println(i);
		}
		
		for (int i : generator) {
			System.out.println(i);
		}
	}
}
