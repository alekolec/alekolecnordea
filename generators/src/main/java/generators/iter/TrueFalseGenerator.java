package generators.iter;

public class TrueFalseGenerator implements Generator<Boolean>{

	int counter = 0;

	public Boolean generate() {
		counter++;
		if (counter % 2 == 0)
			return false;
		return true;
	}

	public static void main(String[] args) {
		
		GeneratorsOperations.printTenElements(new TrueFalseGenerator());

	}

}
