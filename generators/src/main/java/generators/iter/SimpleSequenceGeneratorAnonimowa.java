package generators.iter;

import java.util.Iterator;

public class SimpleSequenceGeneratorAnonimowa implements Iterable<Integer> {

	private final int amount;

	public SimpleSequenceGeneratorAnonimowa(int amount) {
		this.amount = amount;
	}

	public Iterator<Integer> iterator() {

		return new Iterator<Integer>() {

			private int generateNumber = 0;

			public boolean hasNext() {
				return generateNumber < amount;
			}

			public Integer next() {
				generateNumber++;
				return generateNumber;

			}
		};
	}



	public static void main(String[] args) {

		SimpleSequenceGeneratorAnonimowa generator = new SimpleSequenceGeneratorAnonimowa(10);
		
		for(int i : generator) {
			System.out.println(i);
		}
		
		for(int i : generator) {
			System.out.println(i);
		}

	}
}
