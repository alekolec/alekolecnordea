package generators.iter;

import java.util.Iterator;

public class MultipleNumberGenerator implements Iterable<Integer>{
	private int liczba;
	
	private int amount;

	public MultipleNumberGenerator(int liczba, int amount) {
		this.liczba = liczba;
		this.amount = amount;
	}
	
	public Iterator<Integer> iterator() {
		
		return new MultipleNumberIterator();
	}
	
	class MultipleNumberIterator implements Iterator<Integer> {
		private int counter = 0;
		private int generateNumber;
		
		public boolean hasNext() {
			
			return counter<amount;
		}
		
		public Integer next() {
			generateNumber = liczba + generateNumber;
			counter++;
			return generateNumber;
		}
	}
	

	public static void main(String[] args) {
	
		MultipleNumberGenerator generator = new MultipleNumberGenerator(3, 4);
		
		for(int i : generator) {
			System.out.println(i);
		}
		
		for(int i : generator) {
			System.out.println(i);
		}
	}

}
