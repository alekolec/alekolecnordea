package generators.iter;

import java.util.Iterator;

public class SimpleSequenceGenerator implements Iterable<Integer> {

	private final int amount;

	public SimpleSequenceGenerator(int amount) {
		this.amount = amount;
	}

	public Iterator<Integer> iterator() {

		return new SimpleSequenceIterator(); // bo implementuje ten sam
												// interfejs, jak Animal=newCat
	}

	class SimpleSequenceIterator implements Iterator<Integer> {

		private int generateNumber = 0;

		public boolean hasNext() {
			return generateNumber < amount;
		}

		public Integer next() {
			generateNumber++;
			return generateNumber;

		}
	}

	public static void main(String[] args) {

		SimpleSequenceGenerator generator = new SimpleSequenceGenerator(10);
		
		for(int i : generator) {
			System.out.println(i);
		}
		
		for(int i : generator) {
			System.out.println(i);
		}

	}
}
