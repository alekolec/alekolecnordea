package generators;

public class ArithmeticSerieGenerator {
	private int roznica;
	private int generateNumber;
	
	
	public ArithmeticSerieGenerator(int liczbaPoczatkowa, int roznica) {
		this.generateNumber = liczbaPoczatkowa;
		this.roznica = roznica;
	}

	
	public int generate() {
		generateNumber += roznica;
		return generateNumber - roznica;
	}
	
	public static void main(String[] args) {
		ArithmeticSerieGenerator generator = new ArithmeticSerieGenerator(3,5);
		
		for (int i = 0; i < 10; i++) {
			System.out.println(generator.generate());
		}
		
	}

}
