package generators.copy;

import java.util.Random;

public class RandomWordsGenerator {

	// ZALEŻNOŚCI, DEPENDENCIES
	BoundRandomNumbers wordLenghtGenerator;
	RandomLettersGenerator randomLettersGenerator;

	public RandomWordsGenerator(int min, int max) {

		wordLenghtGenerator = new BoundRandomNumbers(min, max);
		randomLettersGenerator = new RandomLettersGenerator();

	}

	public String generate() {

		int dlugosc = wordLenghtGenerator.generate();
		String napis = "";

		for (int i = 0; i <= dlugosc; i++) {
			napis += randomLettersGenerator.generate();

		}
		return napis;
	}

	public static void main(String[] args) {

		RandomWordsGenerator generator = new RandomWordsGenerator(3,7);

		for (int i = 0; i < 10; i++) {
			System.out.println(generator.generate());
		}

	}

}
