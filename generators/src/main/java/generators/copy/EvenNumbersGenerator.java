package generators.copy;

public class EvenNumbersGenerator {

	private int generateNumber = 0;

	public int generate() {

		generateNumber = generateNumber + 2;
		return generateNumber;
	}

	public static void main(String[] args) {
		EvenNumbersGenerator generator = new EvenNumbersGenerator();

		for (int i = 0; i < 10; i++) {
			System.out.println(generator.generate());
		}

	}

}
