package generators.copy;

public class AlphabetGenerator {
	private char litera = 'a';
	

	public char generate() {
		
		if(litera> 'z') {
			litera = 'a';
		}
		return litera++;
	}
	
	public static void main(String[] args) {
		
		AlphabetGenerator generator = new AlphabetGenerator();
		
		for (int i = 0; i < 10; i++) {
			System.out.println(generator.generate());
		}
	}
}
