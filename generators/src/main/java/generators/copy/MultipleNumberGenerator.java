package generators.copy;

public class MultipleNumberGenerator {
	private int liczba;
	private int generateNumber;

	public MultipleNumberGenerator(int liczba) {
		this.liczba = liczba;
	}

	public int generate() {
		generateNumber = liczba + generateNumber;
		return generateNumber;
	}

	public static void main(String[] args) {
		MultipleNumberGenerator generator = new MultipleNumberGenerator(3);

		for (int i = 0; i < 10; i++) {
			System.out.println(generator.generate());
		}
	}

}
