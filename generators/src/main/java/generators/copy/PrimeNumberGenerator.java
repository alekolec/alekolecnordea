package generators.copy;

public class PrimeNumberGenerator {

	int generatedNumber = 2;

	public int generate() {

		do{
			generatedNumber++;
		}
		while (!isPrime(generatedNumber));
			
		return generatedNumber;
	}

	public boolean isPrime(int n) {
		if (n < 2) {
			return false;
		}
		for (int i = 2; i < n; i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;

	}
	
	public static void main(String[] args) {
		
		PrimeNumberGenerator generator = new PrimeNumberGenerator();
		
		for(int i= 0; i<10; i++){
			System.out.println(generator.generate());
		}
		
	}

}