package generators.copy;

public class GeometricSerieGenerator {
	private int roznica;
	private int generateNumber;
	
	
	public GeometricSerieGenerator(int liczbaPoczatkowa, int roznica) {
		this.roznica = roznica;
		this.generateNumber = liczbaPoczatkowa;
	}
	
	public int generate() {
		generateNumber *=roznica;
		return generateNumber/roznica;
	}
	
	public static void main(String[] args) {
		
		GeometricSerieGenerator generator = new GeometricSerieGenerator(3,3);
		
		for (int i = 0; i < 10; i++) {
			System.out.println(generator.generate());
		}
		
		
	}

	
	
}
