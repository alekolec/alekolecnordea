package generators;

public class SimpleSequenceGenerator {

	private int generateNumber = 0;
	
	public int generate() {
		generateNumber++;
		return generateNumber;
		
	}

	public static void main(String[] args) {

		SimpleSequenceGenerator generator = new SimpleSequenceGenerator();

		for (int i = 0; i < 10; i++) {
			System.out.println(generator.generate());
		}

	}
}
