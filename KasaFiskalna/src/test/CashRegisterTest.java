package test;

import java.time.LocalDateTime;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import kasa.Bill;
import kasa.CashRegister;
import kasa.ProgPodatkowy;

public class CashRegisterTest {
	
	private static CashRegister kasa;
	
	@BeforeClass
	public static void prepare() {
	kasa = new CashRegister();
	}
	
	@Test
	public void testUslugi(){
		
		Bill rachunekUslugi = kasa.generateBillUslugi("Fryzjer", 100.0);
		
		Assert.assertEquals(100.0, rachunekUslugi.getKwotaNetto(),0);
		Assert.assertEquals(8.0, rachunekUslugi.getKwotaPodatku(),0);
		Assert.assertEquals(ProgPodatkowy.US�UGI, rachunekUslugi.getProg());
		Assert.assertEquals(108.0, rachunekUslugi.getKwotaBrutto(),0);
		Assert.assertEquals("Fryzjer", rachunekUslugi.getNazwaUslugi());
		Assert.assertEquals(LocalDateTime.now().toLocalDate(), rachunekUslugi.getDt().toLocalDate());
		
	}
	
	@Test
	public void testVAT(){
		
		Bill rachunekVAT = kasa.generateBillVAT("Bulka", 1.0);
		
		Assert.assertEquals(1.0, rachunekVAT.getKwotaNetto(),0);
		Assert.assertEquals(0.23, rachunekVAT.getKwotaPodatku(),0);
		Assert.assertEquals(ProgPodatkowy.VAT, rachunekVAT.getProg());
		Assert.assertEquals(1.23, rachunekVAT.getKwotaBrutto(),0);
		Assert.assertEquals("Bulka", rachunekVAT.getNazwaUslugi());
		Assert.assertEquals(LocalDateTime.now().toLocalDate(), rachunekVAT.getDt().toLocalDate());
	}

	@Test
	public void testAkcyza(){
		
		Bill rachunekAkcyza = kasa.generateBillAkcyza("Wino", 100.0);
		
		Assert.assertEquals(100.0, rachunekAkcyza.getKwotaNetto(),0);
		Assert.assertEquals(31.0, rachunekAkcyza.getKwotaPodatku(),0);
		Assert.assertEquals(ProgPodatkowy.AKCYZA, rachunekAkcyza.getProg());
		Assert.assertEquals(131, rachunekAkcyza.getKwotaBrutto(),0);
		Assert.assertEquals("Wino", rachunekAkcyza.getNazwaUslugi());
		Assert.assertEquals(LocalDateTime.now().toLocalDate(), rachunekAkcyza.getDt().toLocalDate());
	}
	
	@Test
	public void testCwaniak(){
		
		Bill rachunekCwaniak = kasa.generateBillCwaniak("Szmuglowanie", 100);
		
		Assert.assertEquals(100.0, rachunekCwaniak.getKwotaNetto(),0);
		Assert.assertEquals(5.0, rachunekCwaniak.getKwotaPodatku(),0);
		Assert.assertEquals(ProgPodatkowy.CWANIAK, rachunekCwaniak.getProg());
		Assert.assertEquals(105, rachunekCwaniak.getKwotaBrutto(),0);
		Assert.assertEquals("Szmuglowanie", rachunekCwaniak.getNazwaUslugi());
		Assert.assertEquals(LocalDateTime.now().toLocalDate(), rachunekCwaniak.getDt().toLocalDate());
	}
}
