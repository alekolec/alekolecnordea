package kasa;

import java.time.LocalDateTime;

public class Bill {
	private LocalDateTime dt;
	private double kwotaNetto;
	private ProgPodatkowy prog;
	private double kwotaPodatku;
	private double kwotaBrutto;
	private String nazwaUslugi;
	
	public Bill(LocalDateTime dt, double kwotaNetto, ProgPodatkowy prog, double kwotaPodatku, double kwotaBrutto,
			String nazwaUslugi) {
			this.dt = dt;
		this.kwotaNetto = kwotaNetto;
		this.prog = prog;
		this.kwotaPodatku = kwotaPodatku;
		this.kwotaBrutto = kwotaBrutto;
		this.nazwaUslugi = nazwaUslugi;
	}

	public LocalDateTime getDt() {
		return dt;
	}

	public void setDt(LocalDateTime dt) {
		this.dt = dt;
	}

	public double getKwotaNetto() {
		return kwotaNetto;
	}

	public void setKwotaNetto(double kwotaNetto) {
		this.kwotaNetto = kwotaNetto;
	}

	public ProgPodatkowy getProg() {
		return prog;
	}

	public void setProg(ProgPodatkowy prog) {
		this.prog = prog;
	}

	public double getKwotaPodatku() {
		return kwotaPodatku;
	}

	public void setKwotaPodatku(double kwotaPodatku) {
		this.kwotaPodatku = kwotaPodatku;
	}

	public double getKwotaBrutto() {
		return kwotaBrutto;
	}

	public void setKwotaBrutto(double kwotaBrutto) {
		this.kwotaBrutto = kwotaBrutto;
	}

	public String getNazwaUslugi() {
		return nazwaUslugi;
	}

	public void setNazwaUslugi(String nazwaUslugi) {
		this.nazwaUslugi = nazwaUslugi;
	}

	@Override
	public String toString() {
		return "Bill [dt=" + dt + ", kwotaNetto=" + kwotaNetto + ", prog=" + prog + ", kwotaPodatku=" + kwotaPodatku
				+ ", kwotaBrutto=" + kwotaBrutto + ", nazwaUslugi=" + nazwaUslugi + "]";
	}
	
	
	

}
