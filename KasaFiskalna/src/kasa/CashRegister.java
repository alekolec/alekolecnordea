package kasa;

import java.time.LocalDateTime;

public class CashRegister {
	
	public Bill generateBillUslugi(String nazwaUslugi, double kwotaNetto) {
		LocalDateTime dt = LocalDateTime.now();
		ProgPodatkowy prog = ProgPodatkowy.US�UGI;
		double podatek = kwotaNetto * 0.08;
		double kwotaBrutto = kwotaNetto + podatek;
		
		return new Bill (dt,kwotaNetto,prog,podatek,kwotaBrutto,nazwaUslugi);
	}
	
	public Bill generateBillVAT(String nazwaUslugi, double kwotaNetto) {
		LocalDateTime dt = LocalDateTime.now();
		ProgPodatkowy prog = ProgPodatkowy.VAT;
		double podatek = kwotaNetto * 0.23;
		double kwotaBrutto = kwotaNetto + podatek;
		
		return new Bill (dt,kwotaNetto,prog,podatek,kwotaBrutto,nazwaUslugi);
	}

	public Bill generateBillAkcyza(String nazwaUslugi, double kwotaNetto) {
		LocalDateTime dt = LocalDateTime.now();
		ProgPodatkowy prog = ProgPodatkowy.AKCYZA;
		double podatek = kwotaNetto * 0.31;
		double kwotaBrutto = kwotaNetto + podatek;
		
		return new Bill (dt,kwotaNetto,prog,podatek,kwotaBrutto,nazwaUslugi);
	}
	
	public Bill generateBillCwaniak(String nazwaUslugi, double kwotaNetto) {
		LocalDateTime dt = LocalDateTime.now();
		ProgPodatkowy prog = ProgPodatkowy.CWANIAK;
		double podatek = kwotaNetto * 0.05;
		double kwotaBrutto = kwotaNetto + podatek;
		
		return new Bill (dt,kwotaNetto,prog,podatek,kwotaBrutto,nazwaUslugi);
	}
}
