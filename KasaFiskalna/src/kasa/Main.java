package kasa;

public class Main {
	
public static void main(String[] args) {
	
	CashRegister kasa = new CashRegister();
	
	Bill rachunekUslugi = kasa.generateBillUslugi("Fryzjer", 70.0);
	Bill rachunekAkcyza = kasa.generateBillAkcyza("Wino", 29.50);
	Bill rachunekCwaniak = kasa.generateBillCwaniak("Szmuglowanie", 1000.9);
	Bill rachunekVAT = kasa.generateBillVAT("Bu�ka", 0.9);
	
	System.out.println(rachunekUslugi);
	
}
}
