import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.BoxLayout;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class SmsSender implements IMessageListener {

	private JFrame frame;
	private SmsStation station;
	private JTextField txtNumer;
	private JTextField txtText;
	private JTextArea textArea;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SmsSender window = new SmsSender();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SmsSender() {
		initialize();
		station = new SmsStation(this);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(1, 1, 0, 0));
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel);
		panel.setLayout(new GridLayout(2, 1, 0, 0));
		
		JPanel topPanel = new JPanel();
		panel.add(topPanel);
		topPanel.setLayout(new GridLayout(2, 1, 0, 0));
		
		JPanel panel_1 = new JPanel();
		topPanel.add(panel_1);
		panel_1.setLayout(new GridLayout(0, 2, 0, 0));
		
		txtNumer = new JTextField();
		txtNumer.setHorizontalAlignment(SwingConstants.CENTER);
		txtNumer.setText("NUMBER");
		panel_1.add(txtNumer);
		txtNumer.setColumns(10);
		
		txtText = new JTextField();
		txtText.setHorizontalAlignment(SwingConstants.CENTER);
		txtText.setText("TEXT");
		panel_1.add(txtText);
		txtText.setColumns(10);
		
		JButton btnNewButton = new JButton("SEND");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				station.sendSMS(txtNumer.getText(), txtText.getText());
				
				
			}
		});
		topPanel.add(btnNewButton);
		
		JPanel buttomPanel = new JPanel();
		panel.add(buttomPanel);
		buttomPanel.setLayout(new BoxLayout(buttomPanel, BoxLayout.X_AXIS));
		
		 textArea = new JTextArea();
		buttomPanel.add(textArea);
	}

	@Override
	public void newMessageArrived(String message) {
		textArea.setText(textArea.getText() + "\n" + message);
		
	}

}
