public class SmsRequest implements Runnable{
	private String number;
	private String txt;
	private IMessageListener listener;
	
	public String getNumber() {
		return number;
	}
	
	public String getTxt() {
		return txt;
	}
	

	public SmsRequest(String number, String txt, IMessageListener listener) {
		super();
		this.number = number;
		this.txt = txt;
		this.listener = listener;
	}


	@Override
	public void run() {
		try {
			Thread.sleep(txt.length()*100);
			listener.newMessageArrived("SMS wys�any "  + txt) ;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("SMS o tre�ci: " + txt + " na numer: " + number + " zosta� wys�any");
	}



}
