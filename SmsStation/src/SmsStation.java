
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SmsStation {

	private ExecutorService service = Executors.newFixedThreadPool(5);
	private IMessageListener listener;
	
	

	public SmsStation(IMessageListener listener) {
		this.listener = listener;
	}



	public void sendSMS(String number, String txt) {
		SmsRequest request = new SmsRequest(number, txt, listener);
		service.submit(request);

	}

}
