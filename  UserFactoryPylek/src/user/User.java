package user;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

public class User {
	private String nickname;
	private String password;
	private UserType type;

	public User(String nickname, String password, UserType type) {
		this.nickname = nickname;
		this.password = password;
		this.type = type;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserType getType() {
		return type;
	}
	
	public void setType(UserType type) {
		this.type = type;
	}
	


	
	@Override
	public String toString() {
		return "User [nickname=" + nickname + ", password=" + password + ", type=" + type + "]";
	}



	
	

}
