<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:import url="header.jsp" />
<div class="container">
			<nav class="navbar">
				<ul class="nav navbar-nav">
				<li><a href="./ViewServlet?type=all">Wszystkie</a></li>
				<li><a href="./ViewServlet?type=pending">Oczekujące</a></li>
				<li><a href="./ViewServlet?type=done">Zrobione</a></li>
				</ul>
			</nav>
<c:if test="${msg ne null}">
<div class="alert alert-success">
	<p>${msg}</p>
</div>
</c:if>
<div class="well">
	<div class="row">
  		<div class="col-sm-6 col-md-4">

			<c:forEach items="${tasks}" var="task">
	
   				 <div class="thumbnail">
           			<div class="caption">
        <h3><a href="./SingleTaskServlet?id=${task.id}">${task.title}</a></h3>
        <p>${task.status}</p>
<c:choose>        
        <c:when test ="${task.status eq 'PENDING'}">
        <p><a href="./StatusServlet?status=${task.status}&id=${task.id}" class="btn btn-primary" role="button">Zrobione</a> <a href="#" class="btn btn-default" role="button">Usuń</a></p>
    </c:when>
    <c:otherwise>
     <p><a href="./StatusServlet?status=${task.status}&id=${task.id}" class="btn btn-primary" role="button">Oczekujące</a> <a href="#" class="btn btn-default" role="button">Usuń</a></p>
    </c:otherwise>
    </c:choose>
      </div>
    </div>
    </c:forEach>
  </div>
</div>
			
			

	
</div>	
</div>		
<c:import url="footer.jsp" />