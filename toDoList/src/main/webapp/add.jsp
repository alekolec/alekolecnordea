<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:import url="header.jsp" />

<c:if test="${msg ne null}">
<div class="alert alert-success">
	<p>${msg}</p>
</div>
</c:if>

<form action="./TaskServlet" method="POST">

	<div class="input-group">
	  <span class="input-group-addon" id="basic-addon1">Zadanie: </span>
	  <input type="text" name="title" class="form-control" placeholder="Jakie masz zadanie?" aria-describedby="basic-addon1">
	</div>
		<br>
	<div class="input-group">
	  <span class="input-group-addon" id="basic-addon1">Opis zadania: </span>
	  <input type="text" name="description" class="form-control" placeholder="Opisz zadanie" aria-describedby="basic-addon1">
	</div>
		<br>

	
	<button class="btn btn-success center-block">Dodaj</button>
	<br>
</form>
			
<c:import url="footer.jsp" />l>