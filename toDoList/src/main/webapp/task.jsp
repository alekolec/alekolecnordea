<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:import url="header.jsp" />
<div class="well">


<div class="row">

    

        
        <h2 class="card-title">${task.title}</h2>
        
        <p class="card-text">Opis zadania: ${task.description}</p>
        <br>
        <a href="./ViewServlet?type=all" class="btn btn-primary">Cofnij</a>


</div>
</div>
	
<c:import url="footer.jsp" />