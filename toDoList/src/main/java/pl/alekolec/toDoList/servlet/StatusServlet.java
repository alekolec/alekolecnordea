package pl.alekolec.toDoList.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.Transaction;

import pl.alekolec.toDoList.entity.Task;
import pl.alekolec.toDoList.entity.TaskStatus;
import pl.alekolec.toDoList.util.HibernateUtil;

/**
 * Servlet implementation class StatusServlet
 */
public class StatusServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StatusServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String status = request.getParameter("status");
		long id = Long.valueOf(request.getParameter("id"));
		Session session = HibernateUtil.openSession();
		Task task = session.load(Task.class, id);
		switch (status) {
		case "PENDING":
			task.setStatus(TaskStatus.DONE);
			break;
		case "DONE":
			task.setStatus(TaskStatus.PENDING);
			break;
		
		}
		
		Transaction t = session.beginTransaction();
		session.update(task);
		t.commit();
						
		session.close();
		request.setAttribute("msg", "Zmieniono status");
		switch (status) {
		case "PENDING":
			request.getRequestDispatcher("./ViewServlet?type=pending").forward(request, response);
			break;
		case "DONE":
			request.getRequestDispatcher("./ViewServlet?type=done").forward(request, response);
			break;
		
		}
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
