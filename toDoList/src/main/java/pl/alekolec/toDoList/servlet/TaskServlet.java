package pl.alekolec.toDoList.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;
import org.hibernate.Transaction;

import pl.alekolec.toDoList.entity.Task;
import pl.alekolec.toDoList.entity.TaskStatus;
import pl.alekolec.toDoList.util.HibernateUtil;

/**
 * Servlet implementation class TaskServlet
 */
public class TaskServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TaskServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String title = request.getParameter("title");
		String description = request.getParameter("description");
		
		
		Task task = new Task();
		task.setTitle(title);
		task.setStatus(TaskStatus.PENDING);
		task.setDescription(description);
		
		Session session = HibernateUtil.openSession();
		Transaction t = session.beginTransaction();
		session.save(task);
		t.commit();
		
		session.close();
		request.setAttribute("msg", "Zadanie dodane do listy");
		request.getRequestDispatcher("add.jsp").forward(request, response);
	}

}
