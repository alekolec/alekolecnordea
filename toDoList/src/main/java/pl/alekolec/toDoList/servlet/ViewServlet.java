package pl.alekolec.toDoList.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Session;

import pl.alekolec.toDoList.entity.Task;
import pl.alekolec.toDoList.entity.TaskStatus;
import pl.alekolec.toDoList.util.HibernateUtil;

/**
 * Servlet implementation class ViewServlet
 */
public class ViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String type = request.getParameter("type");
		String queryString ="";
		switch (type) {
		case "all":
			queryString = "from Task";
			break;
		case "pending":
			queryString = "from Task where status = '" + TaskStatus.PENDING + "'";
			break;
		case "done":
			queryString = "from Task where status = '" + TaskStatus.DONE + "'";
			break;
			
		default: 
			queryString = "from Task";
		}
	
		Session session = HibernateUtil.openSession();
		List<Task>listOfTasks = session.createQuery(queryString).list();
		
	
		
		session.close();
		request.setAttribute("tasks", listOfTasks);  //bindowanie wiazanie ze soba backendu i widoku
		request.getRequestDispatcher("view.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
