package com.alekolec.activityPlan.entity;

import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Training {
	
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private int id;
	@ManyToMany (cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "trainings")
	private Set<Sportsman> sportsmen;
	@ManyToOne (cascade = CascadeType.MERGE)
	private Activity activity;
	private LocalDateTime date;
	
	public Training() {}

	public Training(Set<Sportsman> sportsmen, Activity activity, LocalDateTime date) {
		super();
		this.sportsmen = sportsmen;
		this.activity = activity;
		this.date = date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setSportsmen(Set<Sportsman> sportsmen) {
		this.sportsmen = sportsmen;
	}
	
	public Set<Sportsman> getSportsmen() {
		return sportsmen;
	}

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	
	
	

}
