package com.alekolec.activityPlan.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Activity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String name;
	@OneToMany (fetch = FetchType.EAGER, mappedBy = "activity", cascade = CascadeType.ALL)
	private List<Training> trainings;
	
	
	public Activity() {}
		
	public Activity(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	

	public Activity(String name) {
		super();
		this.name = name;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	

}
