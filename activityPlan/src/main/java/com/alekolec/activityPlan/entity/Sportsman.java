package com.alekolec.activityPlan.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Sportsman {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String login;
	private String password;
	private String name;
	private String lastname;
	private int age;
	@ManyToMany (cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Training> trainings;
		
	public Sportsman() {}
		
	public Sportsman(int id, String login, String password, String name, String lastname, int age) {
		super();
		this.id = id;
		this.login = login;
		this.password = password;
		this.name = name;
		this.lastname = lastname;
		this.age = age;
	}
	
	
	public Sportsman(String login, String password, String name, String lastname, int age) {
		super();
		this.login = login;
		this.password = password;
		this.name = name;
		this.lastname = lastname;
		this.age = age;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	public Set<Training> getTrainings() {
		return trainings;
	}
	
	public void setTrainings(Set<Training> trainings) {
		this.trainings = trainings;
	}
	
	
	
}
