package com.alekolec.activityPlan.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.alekolec.activityPlan.entity.Sportsman;
import com.alekolec.activityPlan.repository.SportsmanRepository;


@Controller
@RequestMapping("/sportsman")
public class SportsmanController {
	
	@Autowired
	SportsmanRepository sportsmanRepository;
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public String showAll(ModelMap model) {
		List<Sportsman> listOfSportsmen = (List<Sportsman>) sportsmanRepository.findAll();
		model.addAttribute("sportsmen", listOfSportsmen);
		return "sportsman/all";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addSportsmanView() {
		return "sportsman/add";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public void addSportsman(@RequestParam("login") String login,
							 @RequestParam("password") String password,
							 @RequestParam("name") String name,
							 @RequestParam("lastname") String lastname,
							 @RequestParam("age") int age,
					         HttpServletRequest request, HttpServletResponse response) throws IOException {
		Sportsman sportsman = new Sportsman();
		sportsman.setLogin(login);
		sportsman.setPassword(password);
		sportsman.setName(name);
		sportsman.setLastname(lastname);
		sportsman.setAge(age);
		
		sportsmanRepository.save(sportsman);
		
		response.sendRedirect(request.getContextPath() + "sportsman/all");
	}

}
