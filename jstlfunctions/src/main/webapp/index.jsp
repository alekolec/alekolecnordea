<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<!DOCTYPE html>
<html>
	<head>
		<title>Tytuł strony</title>
		<meta charset="UTF-8">
	</head>
		<body>
			<c:set var="array" value="${fn:split('Dzisiaj jest test',' ') }"/>
			<ul>
			<c:forEach items="${array}" var="str">
			<li>${str} </li>
			</c:forEach>
			</ul>
			
			<c:set var="joined" value="${fn:join(array, '-')}" />
			<p>${joined}</p>
			
			
			<c:out value="${fn:replace(joined,'-', '|')}"></c:out>
			
			<c:if test="${fn:contains(joined, 'jest')}">
			<p>Tekst</p>
			</c:if>
			
			<h3>Kolorki:</h3>
			<c:set var="colors" value="rgba(255,0,255,1)|#cc0000|black|brown|blue|purple|green|yellow|red|orange" />
			<c:set var="colorsArray" value="${fn:split(colors,'|') }" />
			<ul>
			<c:forEach items="${colorsArray}" var="color">
			<li style="color: ${color}"}>${color } </li>
			</c:forEach>
			</ul>
			
			<c:if test="${fn:startsWith(colors, 'rgba')}">
				<p>Komórki zaczynają się od 'rgba'</p>
			</c:if>
			
			<c:if test="${fn:endsWith(colors, 'orange') }">
				<p>Zakończyliśmy na pomarańczach</p>
			</c:if>
			<p><c:out value="${fn:length(colors)}" /> </p>
			
			<c:set var="substr" value="${fn:substring(colors, 23, 30 )}" />
			<p>${substr}</p>
		
		<a href="./SimpleServlet">Pierwszy</a>
		<a href="./SimpleServlet/action/add">Drugi</a>
		<a href="./SimpleServlet/action/show">Trzeci</a>
		<a href="./SimpleServlet/action/show/1">Czwarty</a>
		
		</body>
	
</html>