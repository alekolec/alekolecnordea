package com.alekolec.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SimpleServlet
 */
public class SimpleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SimpleServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/*
		 * response.getWriter() .append("Served at: ")
		 * .append(request.getContextPath()) .append(" ")
		 * .append(request.getServletPath()) .append(" ")
		 * .append(request.getPathInfo());
		 */
		String addr = request.getPathInfo();
		if(addr != null){
			String[] urlString = addr.split("/");
			
			int urlLength = urlString.length - 1;
			System.out.println("\t > " + urlString[urlLength]);
			switch (urlString[urlLength]) {
			case "add":
				response.sendRedirect(request.getContextPath() + "/add.jsp");
				//request.getRequestDispatcher("add.jsp").forward(request, response);
				break;
			case "show":
				response.sendRedirect(request.getContextPath() + "/all.jsp");
				//request.getRequestDispatcher("all.jsp").forward(request, response);
				break;
			case "1":
				response.sendRedirect(request.getContextPath() + "/single.jsp");
				//request.getRequestDispatcher("single.jsp").forward(request, response);
				break;
			}
		} else {
			request.getRequestDispatcher("./main.jsp").forward(request, response);
		}
		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
