package pl.home.piotrd.tdd;

public class Client {
	private double salary;
	private double debt;
	private double accountBalance;

	public double getSalary() {
		return salary;
	}

	public double getDebt() {
		return debt;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public Client(double salary, double debt, double accountBalance) {
		this.salary = salary;
		this.debt = debt;
		this.accountBalance = accountBalance;
	}
}
