package pl.home.piotrd.tdd;

public enum Benefit {
	KARTA_KREDYTOWA("Karta_Kredytowa"),
	LOKATA("Lokata"),
	VIP("VIP"),
	KREDYT("Kredyt");
	// TODO: add more benefits from exercise

	private String name;

	private Benefit(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}
}