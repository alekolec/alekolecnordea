package pl.home.piotrd.tdd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Offer {
	private final List<Benefit> benefits;
	private final Client client;

	public Offer(Client client) {
		benefits = new ArrayList<Benefit>();
		this.client = client;
	}

	/**
	 * If offer was calculated, returns offer for given client If offer was not
	 * yet calculated, returns empty list
	 * 
	 * @return List of benefits for given client (named in constructor)
	 */
	public List<Benefit> getOffer() {
		return Collections.unmodifiableList(benefits);
	}

	/**
	 * Calculates offer based on requirements
	 * 
	 * @param client
	 *            - client for which the offer should be calcualted
	 */
	public void calculateOffer(Client client) {
		if(client.getDebt()>0d)
			return;
		if(shouldReceiveBond(client))
			benefits.add(Benefit.LOKATA);
		if(shouldReceiveVipAccount(client))
			benefits.add(Benefit.VIP);
		if(client.getAccountBalance()>=20000) {
			benefits.add(Benefit.LOKATA);
			
		}
		if(client.getSalary()>3000) {
			benefits.add(Benefit.LOKATA);
		}

	}

	private boolean shouldReceiveVipAccount(Client client) {
		if(client.getAccountBalance()>20000d || client.getSalary()<=3000d)
			return true;
		return false;
	}

	private boolean shouldReceiveBond(Client client) {
		if(client.getAccountBalance()<=20000d || client.getSalary()>3000d)
			return true;
		return false;
	}
}


