package pl.home.piotrd.test.tdd;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import pl.home.piotrd.tdd.Benefit;
import pl.home.piotrd.tdd.Client;
import pl.home.piotrd.tdd.Offer;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.hasItem;




public class TestOffer {

	@Test
	public void testOfferOption1() {
		Client client = new Client(2000d, 0d, 20000d);
		Offer offer = new Offer(client);
		offer.calculateOffer(client);
		List<Benefit> benefits = offer.getOffer();
		assertThat(benefits, hasItem(Benefit.LOKATA));
		//Assert.assertTrue(benefits.contains(Benefit.LOKATA));
	}
	
	@Test
	public void testOfferOption2() {
		Client client = new Client(1900d, 0d, 19000d);
		Offer offer = new Offer(client);
		offer.calculateOffer(client);
		List<Benefit> benefits = offer.getOffer();
		Assert.assertTrue(benefits.contains(Benefit.KARTA_KREDYTOWA));
		Assert.assertTrue(benefits.contains(Benefit.VIP));
	}
	
	@Test
	public void testOfferOption3() {
		Client client = new Client(1900d, 100d, 19000d);
		Offer offer = new Offer(client);
		offer.calculateOffer(client);
		List<Benefit> benefits = offer.getOffer();
		Assert.assertTrue(benefits.isEmpty());
	}


}
