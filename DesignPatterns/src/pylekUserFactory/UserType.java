package pylekUserFactory;

import java.util.Collection;
import java.util.EnumSet;

public class UserType {
	public final String name;
	public final EnumSet<PERMISSION> permissions;

	
	public String getName() {
		return name;
	}
	
	public EnumSet<PERMISSION> getPermissions() {
		return permissions;
	}

	public UserType(String name, Collection<PERMISSION> permissions) {
		this.name = name;
		this.permissions = EnumSet.copyOf(permissions);
	}

	@Override
	public String toString() {
		return "UserType [name=" + name + ", permissions=" + permissions + "]";
	}
	
	
	
}