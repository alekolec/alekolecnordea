package pylekUserFactory;

import java.util.Arrays;

import static pylekUserFactory.PERMISSION.*;

public class Main {
	public static void main(String[] args) {
		UserFactory userFactory = new UserFactory();

		userFactory.addType(new UserType("Client", Arrays.asList(CREATE_ORDERS)));

		userFactory.addType(new UserType("Menager", Arrays.asList(PERMISSION.values())));

		userFactory.addType(
				new UserType("Seller", Arrays.asList(CREATE_PRODUCTS, DELETE_PRODUCTS, MODIFY_PRODUCTS)));

		User user = userFactory.create("Edek", "123", "Client");
		System.out.println(user);

		User seller = userFactory.create("Edek", "234", "Seller");
		System.out.println(seller);

	}
}
