package pylekUserFactory;

import java.util.HashMap;
import java.util.Map;

public class UserFactory {
	private Map<String, UserType> userTypes = new HashMap<>();

	public void addType(UserType userType) {
		userTypes.put(userType.getName(), userType);
	}

	public User create(String nickname, String password, String type) {
					
			return new User(nickname, password, userTypes.get(type));
		}
	}
	
	
	

