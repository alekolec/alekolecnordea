package abstractFactoryBikeFactory;


public abstract class BikeFactory {
	
	public static AbstractBike createKROSS5Gears() {
		return new AbstractBike("KROSS", 1, 5,BikeType.BICYCLE);
	}

	public static AbstractBike createMERIDA6Gears() {
		return new AbstractBike("MERIDA", 1, 6, BikeType.BICYCLE);
	}
	
	public static AbstractBike createINIANA3Gears() {
		return new AbstractBike("INIANA", 2, 3, BikeType.TANDEM);
	}
	
	public static AbstractBike createFELT6Gears() {
		return new AbstractBike("FELT", 1, 6, BikeType.BICYCLE);
	}
	
	public static AbstractBike createGOETZE1Gear() {
		return new AbstractBike("GOETZE", 2, 1, BikeType.TANDEM);
	}
	

}
