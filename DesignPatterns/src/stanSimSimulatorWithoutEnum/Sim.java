package stanSimSimulatorWithoutEnum;

public class Sim {

	private State state;

	private static final State RESTED = new RestedState();

	private static final State TIRED = new State() {

		@Override
		public void work() {
			System.out.println("Jestem zm�czony, nie b�d� pracowa�");
		
		}
	};

	private static final State MOTIVATE = new State() {

		@Override
		public void work() {
			System.out.println("Pracuj� bardzo wydajnie");
		}
	};

	public Sim() {
		this.state = RESTED;
	}

	public void rest() {
		System.out.println("Odpoczywam...");
		state = RESTED;
	}

	public void work() {   // to kt�re work si� wykona zale�y od stanu w ktorym b�dzie znajdowa� si� SIM
		state.work();
		state = TIRED;
	}

	public void reward() {
		state = MOTIVATE;
	}

	/*
	 * private enum State { RESTED {
	 * 
	 * @Override void work() { System.out.println("Ok. Pracuj�");
	 * 
	 * } }, TIRED {
	 * 
	 * @Override void work() { System.out.println(
	 * "Jestem zm�czony, nie b�d� pracowa�.");
	 * 
	 * } },
	 * 
	 * MOTIVATE {
	 * 
	 * @Override void work() { System.out.println("Pracuj� bardzo wydajnie");
	 * 
	 * } };
	 * 
	 * 
	 * abstract void work();
	 * 
	 * 
	 * }
	 */
}
