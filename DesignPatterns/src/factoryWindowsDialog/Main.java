package factoryWindowsDialog;

public class Main {

	public static void main(String[] args) {
		
		WindowsDialogFactory factory = new WindowsDialogFactory();
		
		factory.showWarning("Warning");
		factory.showError("Error");
		factory.showInfo("Informacja");
		factory.showMessage("Message");
		factory.showNone("None");

	}

}
