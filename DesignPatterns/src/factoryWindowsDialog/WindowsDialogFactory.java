package factoryWindowsDialog;
import javax.swing.JOptionPane;

public class WindowsDialogFactory {
	
	public void showWarning(String tresc) {
	JOptionPane.showMessageDialog(null,		
		    tresc,
		    "Error",			
		    JOptionPane.WARNING_MESSAGE);	
}
	
	public void showInfo(String tresc) {
		JOptionPane.showMessageDialog(null,		
			    tresc,
			    "Info",			
			    JOptionPane.INFORMATION_MESSAGE);	
	}
	
	public void showError(String tresc) {
		JOptionPane.showMessageDialog(null,		
			    tresc,
			    "Error",			
			    JOptionPane.ERROR_MESSAGE);	
	}
	
	public void showMessage(String tresc) {
		JOptionPane.showMessageDialog(null,		
			    tresc,
			    "Message",			
			    JOptionPane.PLAIN_MESSAGE);	
	}
	
	public void showNone(String tresc) {
		JOptionPane.showMessageDialog(null,		
			    tresc
	);	
	}
	
}