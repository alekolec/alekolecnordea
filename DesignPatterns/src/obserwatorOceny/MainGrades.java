package obserwatorOceny;

public class MainGrades {
	
	public static void main(String[] args) {
		
		Grades grades = new Grades();
		
		GradesObserver randomObserver = new GradesObserver() {
			
			@Override
			public void newGrade(int grade) {
				System.out.println("Randomowy typ: O, ocena " + grade);
				
			}
		};
		
		GradesObserver student = new Student();
		GradesObserver teacher = new Teacher();
		GradesObserver parent = new Parent();
		
		grades.addObserver(randomObserver);
		grades.addObserver(student);
		grades.addObserver(teacher);
		grades.addObserver(parent);
		
		grades.addGrade(1);
		grades.addGrade(2);
		grades.addGrade(3);
		grades.addGrade(4);
		grades.addGrade(5);
		
	}

}
