package obserwatorOceny;

import java.util.LinkedList;
import java.util.List;

public class Grades {

	private List<Integer> gradesList = new LinkedList<>();
	private List<GradesObserver> observers = new LinkedList<>();
	
	
	public void addGrade(int grade) {
		System.out.println("Dodano ocen�: " + grade);
		gradesList.add(grade);
		
		for(GradesObserver gradesObserver :  observers) {
			gradesObserver.newGrade(grade);
		}
	}
	
	
	
	public void addObserver(GradesObserver gradesObserver) {
		observers.add(gradesObserver);
	}
}
