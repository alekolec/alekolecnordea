package obserwatorOceny;

public class Teacher implements GradesObserver {

	@Override
	public void newGrade(int grade) {
		System.out.print("Teacher: ");
		if (grade <= 3 && grade > 0) {
			System.out.println("Dzieciak zdolny, ale leniwy");
		} else if (grade == 4) {
			System.out.println("Jakby si� postara� by�oby 5?");
		} else if (grade > 4 && grade <= 6) {
			System.out.println("Zadania by�y za �atwe");
		} else {
			System.out.println("Przecie� nie ma takiej oceny");
		}

	}

}
