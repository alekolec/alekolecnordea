package obserwatorOceny;

public class Student implements GradesObserver {

	@Override
	public void newGrade(int grade) {
		System.out.print("Student: ");
		if(grade<3 && grade>0) {
			System.out.println("Znowu :(");
		} else if(grade == 3) {
			System.out.println("Mo�e by� ;)");
		} else if(grade>3 && grade<=6){
			System.out.println("Super!");
		} else {
			System.out.println("Nie ma przecie� takiej oceny");
		}
		
	}

}
