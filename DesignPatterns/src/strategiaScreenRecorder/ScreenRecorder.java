package strategiaScreenRecorder;

import java.util.ArrayList;
import java.util.List;

public class ScreenRecorder {

	private List<Profile> profiles;
	private Profile currentProfile;
	private PROCESS rec = PROCESS.NOT_RECORDING;

	public ScreenRecorder() {
		this.profiles = new ArrayList<>();
	}
	
	public void addProfile(Profile profile) {
		profiles.add(profile);
	}
	
	public void listProfiles() {
		for(int i = 0; i<profiles.size(); i++) {
			System.out.println(i+1 + " - " + profiles.get(i));
		}
	}
	
	public void setProfile(int position) {
		if(position>(profiles.size()) || (position<1)) {
			System.out.println("Nie ma takiego!");
		} else {
		currentProfile = profiles.get(position-1);
	}
	}
	public void startRecording() {
		if(rec.equals(PROCESS.RECORDING)) {
			System.out.println("Nie mo�na rozpocz�� kolejnego nagrywania, bo nagrywam");
		} else if(currentProfile == null) {
			System.out.println("Nie wybrano profilu");
		} else {
		System.out.println("Rozpoczynam nagrywanie: " + currentProfile);
		rec = PROCESS.RECORDING;
		}
		}

	
	public void stopRecording() {
		if(rec.equals(PROCESS.NOT_RECORDING)) {
			System.out.println("Nie mog� zako�czy� nagrywania skoro nie zacz��em");
		} else {
		System.out.println("Ko�cz� nagrywanie.");
		rec = PROCESS.NOT_RECORDING;
		}
	}
	
	public List<Profile> getProfiles() {
		return profiles;
	}

}
