package strategiaScreenRecorder;

import java.util.Scanner;

public class ConsoleMain {
	private static final int ADD = 1;
	private static final int LIST = 2;
	private static final int START = 3;
	private static final int STOP = 4;
	private static final int SET = 5;
	private static final int EXIT = 6;

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		ScreenRecorder recorder = new ScreenRecorder();

		while (true) {
			printOptions();
			int option = Integer.parseInt(scanner.nextLine());
			if(option<1 || option> 6) {
				System.out.println("Nie ma takiej opcji!");
				continue;
			}
			switch (option) {
			case ADD:
				System.out.println("Podaj codec, extension, resolution");
				System.out.println("Codecs: ");
				printProfile();
				String line = scanner.nextLine();
				try {
					String[] splits = line.split("[\\W]+");
					CODEC codec = CODEC.valueOf(splits[0]);
					EXTENSION extension = EXTENSION.valueOf(splits[1]);
					RESOLUTION resolution = RESOLUTION.valueOf(splits[2]);

					recorder.addProfile(new Profile(codec, extension, resolution));
				} catch (IllegalArgumentException e) {
					System.out.println("Z�y format");
				}
				break;
			case LIST:
				recorder.listProfiles();
				break;
			case START:
				recorder.startRecording();
				break;
			case STOP:
				recorder.stopRecording();
				break;
			case SET:
				System.out.println("Podaj ktorego profilu chcesz u�y�");
				int prof = Integer.parseInt(scanner.nextLine());
				recorder.setProfile(prof);
				break;
			case EXIT:
				scanner.close();
				return;

			}
		}

	}

	private static void printProfile() {
		for (CODEC codec : CODEC.values()) {
			System.out.println(codec);
		}
		System.out.println("Extensions: ");
		for (EXTENSION extension : EXTENSION.values()) {
			System.out.println(extension);
		}
		System.out.println("Resolutions: ");
		for (RESOLUTION resolution : RESOLUTION.values()) {
			System.out.println(resolution);
		}
	}

	public static void printOptions() {
		System.out.println("Je�li chcesz wykona� - wci�nij: ");
		System.out.println(ADD + " - dodaj profil");
		System.out.println(LIST + "- wy�wietl list�");
		System.out.println(START + " - rozpocznij nagrywanie");
		System.out.println(STOP + " - zako�cz nagrywanie");
		System.out.println(SET + " - ustaw profil");
		System.out.println(EXIT + " - wyjd� z aplikacji");

	}
}