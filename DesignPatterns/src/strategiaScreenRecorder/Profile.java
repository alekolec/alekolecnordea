package strategiaScreenRecorder;

public class Profile {
	
	private CODEC codec;
	private EXTENSION extension;
	private RESOLUTION resolution;
	
	public Profile(CODEC codec, EXTENSION extension, RESOLUTION resolution) {
		super();
		this.codec = codec;
		this.extension = extension;
		this.resolution = resolution;
	}

	public CODEC getCodec() {
		return codec;
	}

	public void setCodec(CODEC codec) {
		this.codec = codec;
	}

	public EXTENSION getExtension() {
		return extension;
	}

	public void setExtension(EXTENSION extension) {
		this.extension = extension;
	}

	public RESOLUTION getResolution() {
		return resolution;
	}

	public void setResolution(RESOLUTION resolution) {
		this.resolution = resolution;
	}

	@Override
	public String toString() {
		return "Profile [codec=" + codec + ", extension=" + extension + ", resolution=" + resolution + "]";
	}
	
	
	
	
}
