package strategiaScreenRecorder;

public class RecordingMain {

	public static void main(String[] args) {
		
		ScreenRecorder recorder = new ScreenRecorder();
		
		
		recorder.addProfile(new Profile(CODEC.H263, EXTENSION.AVI, RESOLUTION.R1366x768));
		recorder.addProfile(new Profile(CODEC.H264, EXTENSION.MP3, RESOLUTION.R1920x1080));
		recorder.addProfile(new Profile(CODEC.THEORA, EXTENSION.MKV, RESOLUTION.R800x600));
		recorder.addProfile(new Profile(CODEC.VR8, EXTENSION.MP4, RESOLUTION.R2560x1440));
		recorder.addProfile(new Profile(CODEC.H263, EXTENSION.MKV, RESOLUTION.R800x600));
		recorder.addProfile(new Profile(CODEC.THEORA, EXTENSION.MP3, RESOLUTION.R1366x768));
		
		
	
		recorder.listProfiles();
		
		recorder.startRecording();
		
		recorder.setProfile(1);

		recorder.stopRecording();
		recorder.startRecording();
		recorder.startRecording();
		recorder.stopRecording();
		
		recorder.setProfile(7);
		recorder.setProfile(0);

	}

}
