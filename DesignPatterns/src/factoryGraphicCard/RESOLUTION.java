package factoryGraphicCard;

public enum RESOLUTION {
	R1920x1080 (1920, 1080),
	R2560x1440 ( 2560, 1440),
	R800x600 (800,600),
	R600x400 (600,400),
	R1366x768 (1366, 768);
	
	private int width; //szerokosc
	private int height; // wysokosc
	
	private RESOLUTION(int w, int h) {
		this.width = w;
		this.height = h;
	}
	
	public int getHeight() {
		return height;
	}
	public int getWidth() {
		return width;
	}

}
