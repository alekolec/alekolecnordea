package factoryGraphicCard;

public enum COLORS {
	B32 (32),
	B16 (16),
	B8 (8),
	MONO (2);
	
	private int bits;
	
	private COLORS(int bits) {
		this.bits = bits;
	}
	
	public int getBits() {
		return bits;
	}

}
