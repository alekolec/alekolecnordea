package factoryGraphicCard;

public enum FORMAT {
	F16x10 (16, 10),
	F16x9 (16,9),
	F4x3 (4,3),
	F3x2 (3,2);
	
	private int width;
	private int height;
	
	
	private FORMAT(int w, int h) {
		this.width = w;
		this.height = h;
	}
	
	public int getHeight() {
		return height;
	}
	
	public int getWidth() {
		return width;
	}
}
