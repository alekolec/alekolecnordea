package abstractFactoryPCFactory;

public class AsusPC extends AbstractPC {

	private AsusPC(String name, COMPUTER_BRAND brand, int cpu_power, double gpu_power, boolean isOverclocked) {
		super(name, brand, cpu_power, gpu_power, isOverclocked);
		
	}
	
	public static AbstractPC createASUSFastPC() {
		return new AsusPC("Asus1", COMPUTER_BRAND.ASUS, 89, 0.56, true );
		
	}
	
	public static AbstractPC createASUSSlowPC() {
		return new AsusPC("Asus2", COMPUTER_BRAND.ASUS, 43, 0.23, false );
		
	}
	
	

}
