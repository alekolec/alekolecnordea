package polecenieKolejkaZadan;



public class Main {
	
	public static void main(String[] args) {
		
		ExecutionStrategy executor = new NoQueueExecutor();
		
		executor.add(new HelloWorld(1));
		executor.add(new HelloWorld(2));
		executor.add(new Dolphin(6));
		executor.add(new SaveNumbers(2));
		executor.add(new Counting(10));
		executor.add(new Counting(9));
		
		executor.extecuteAll();

			
		}
		
	}


