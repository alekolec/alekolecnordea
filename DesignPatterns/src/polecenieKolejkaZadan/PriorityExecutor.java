package polecenieKolejkaZadan;

import java.util.PriorityQueue;
import java.util.Queue;

public class PriorityExecutor implements ExecutionStrategy {
	
	Queue<Task> tasks = new PriorityQueue<Task>(); 
	
	
	@Override
	public void add(Task task) {
		tasks.offer(task);
	}
	

	@Override
	public void extecuteAll() {
		while(!tasks.isEmpty()) {
			Task task = tasks.poll();
			System.out.println("Nazwa: " + task.getName() + "Priorytet: " + task.getPriority());
			task.execute();
			
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}

}
