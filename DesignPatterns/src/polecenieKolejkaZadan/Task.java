package polecenieKolejkaZadan;


public abstract class Task implements Comparable <Task> {
	private final int priority;
	private final String name;

	public Task(String name) {
		this(1, name); // ta linia wywoluje metode z 11
	}

	public Task(int priority, String name) {
		this.priority = priority;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public int getPriority() {
		return priority;
	}

	public abstract void execute();
	

	public int compareTo(Task t) {
	return t.priority- priority;
}
}
