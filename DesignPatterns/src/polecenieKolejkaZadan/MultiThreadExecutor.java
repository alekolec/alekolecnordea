package polecenieKolejkaZadan;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class MultiThreadExecutor implements ExecutionStrategy {
	
	private Queue<Task> tasks = new LinkedList<Task>(); 
	private Executor executor = Executors.newFixedThreadPool(2);
	
	@Override
	public void add(Task task) {
		tasks.offer(task);
	}
	

	@Override
	public void extecuteAll() {
		while(!tasks.isEmpty()) {
			Task task = tasks.poll();
			
			executor.execute(new Runnable() {

				@Override
				public void run() {
					System.out.println("Nazwa: " + task.getName() + "Priorytet: " + task.getPriority());
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
				
			});
			
			
			
		}
	}

}
