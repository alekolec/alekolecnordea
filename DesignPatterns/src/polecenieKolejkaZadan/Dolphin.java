package polecenieKolejkaZadan;

public class Dolphin extends Task{

	public Dolphin(int priority) {
		super(priority, "Dolphin");
		
	}

	@Override
	public void execute() {
		System.out.println("                               _.-~~.)");
		System.out.println("         _.--~~~~~---....__  .' . .,' ");
		System.out.println("       ,'. . . . . . . . . .~- ._ (");
		System.out.println("      ( .. .g. . . . . . . . . . .~-._");
		System.out.println("   .~__.-~    ~`. . . . . . . . . . . -. ");
		System.out.println("   `----..._      ~-=~~-. . . . . . . . ~-.  ");
		System.out.println("             ~-._   `-._ ~=_~~--. . . . . .~.  ");
		System.out.println("              | .~-.._  ~--._-.    ~-. . . . ~-.");
		System.out.println("                .(   ~~--.._~'       `. . . . .~-.                ,");
		System.out.println("                `._         ~~--.._    `. . . . . ~-.    .- .   ,'/");
		System.out.println("_  . _ . -~        _ ..  _          ~~--.`_. . . . . ~-_     ,-','`  .");
		System.out.println("             ` ._           ~                ~--. . . . .~=.-'. /. `");
		System.out.println("       - . -~            -. _ . - ~ - _   - ~     ~--..__~ _,. /     - ~");
		System.out.println("              . __ ..                   ~-               ~~_. (  `");
		System.out.println(")`. _ _               `-       ..  - .    . - ~ ~ .        ~-` ` `  `. _");
	}


	

}
