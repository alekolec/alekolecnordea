package polecenieKolejkaZadan;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Reader;

public class SaveNumbers extends Task {

	public SaveNumbers(int priority) {
		super(priority, "Save Numbers");
	
	}

	@Override
	public void execute() {
				
		try{
		    PrintWriter writer = new PrintWriter("numbers.txt");
		    writer.println("1,2,3,4,5");
		    writer.close();
		} catch (IOException e) {
		   // do something
		}
		
	}

}
