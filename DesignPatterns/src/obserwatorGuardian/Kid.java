package obserwatorGuardian;

import java.util.Observable;

public class Kid extends Observable{
	private String name;
	private int kidID;
	private static int counter;
	
	
	
	public Kid(String name) {
		this.name = name;
		
		this.kidID = counter;
		counter++;
	}

	public void doSomething(String something) {
		setChanged();
		notifyObservers(something);
//		System.out.println(something);
	}
	
	public String getName() {
		return name;
	}
	
	public int getKidID() {
		return kidID;
	}

	@Override
	public String toString() {
		return name;
	}
	
	

}
