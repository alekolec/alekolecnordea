package obserwatorGuardian;

import java.util.ArrayList;
import java.util.List;

public class Server {
	private List<Guardian> guardians;
	private List<Kid> kids;
	
	public Server() {
		this.guardians = new ArrayList<>();
		this.kids = new ArrayList<>();
	}
	
	public List<Guardian> getGuardians() {
		return guardians;
	}
	
	public List<Kid> getKids() {
		return kids;
	}
	
	public void addGruardian(Guardian guardian) {
		guardians.add(guardian);
		
		System.out.println("Guardian added: " + guardian.getGuardianID());
	}
	
	public void addKid(Kid kid) {
		kids.add(kid);

		System.out.println("Kid added: " + kid.getKidID());
	}
	
	public void connectGuardian(int guardianID, int kidID ) {
		kids.get(kidID).addObserver(guardians.get(guardianID));
		
	}

	
}
