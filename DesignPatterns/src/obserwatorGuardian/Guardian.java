package obserwatorGuardian;

import java.util.Observable;
import java.util.Observer;

public class Guardian implements Observer {
	private String name;
	private int guardianID;
	private static int counter;

	
	public Guardian(String name) {
		this.name = name;
		
		this.guardianID = counter;
		counter++;
		
	}

	@Override
	public void update(Observable arg, Object arg1) {
		System.out.println("Ja - " + name +   " zosta�em poinformowany od " + arg + " o " + arg1);
		
	}
	
	public String getName() {
		return name;
	}
	
	public int getGuardianID() {
		return guardianID;
	}

	@Override
	public String toString() {
		return name;
	}

	
}
