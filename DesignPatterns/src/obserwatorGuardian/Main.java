package obserwatorGuardian;

public class Main {

	public static void main(String[] args) {
		
		Server server = new Server();
		
		Kid k =	new Kid("Dzieciak");
		server.addKid(k);
		
		server.addGruardian(new Guardian("Stefan"));
		server.addGruardian(new Guardian("Adam"));
		server.addGruardian(new Guardian("Aleks"));
		server.addGruardian(new Guardian("Janina"));
		server.addGruardian(new Guardian("Anna"));
		
		server.connectGuardian(0, 0);
		server.connectGuardian(1, 0);
		server.connectGuardian(2, 0);
		
		k.doSomething("something");
		

	}

}