package dekoratorPizza;

import java.util.List;

public class PepperoniPizza implements Pizza{

	Pizza simplePizza;



	public PepperoniPizza(Pizza simplePizza) {
		this.simplePizza = simplePizza;
	}

	@Override
	public List<String> getToppings() {
		List<String> toppings = simplePizza.getToppings();
		toppings.add("Pepperoni");
		return toppings;
	}

	@Override
	public List<String> getIngredients() {
		List<String> ingredients = simplePizza.getIngredients();
		return ingredients;
	}

	@Override
	public String toString() {
		return "PepperoniPizza: toppings: " + getToppings() + ", ingredients: " + getIngredients();
	}
}
