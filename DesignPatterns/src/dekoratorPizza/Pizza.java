package dekoratorPizza;

import java.util.List;

public interface Pizza {

	List<String > getToppings();
	List<String > getIngredients();
}
