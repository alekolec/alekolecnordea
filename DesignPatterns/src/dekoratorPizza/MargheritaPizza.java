package dekoratorPizza;

import java.util.List;

public class MargheritaPizza extends SimplePizza {

private Pizza simplePizza;


	public MargheritaPizza(Pizza simplePizza) {
		this.simplePizza = simplePizza;
	}


	@Override
	public List<String> getToppings() {
		List<String>toppings = simplePizza.getToppings();
		toppings.add("Cheese");
		return toppings;
	}


	@Override
	public List<String> getIngredients() {
		List<String> ingredients = simplePizza.getIngredients();
		return ingredients;
	}


	@Override
	public String toString() {
		return "MargheritaPizza: toppings: " + getToppings() + ", ingredients: " + getIngredients();
	}




}
