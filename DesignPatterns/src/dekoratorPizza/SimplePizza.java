package dekoratorPizza;

import java.util.ArrayList;
import java.util.List;

public class SimplePizza implements Pizza {
	
	List<String> ingredients = new ArrayList<String>();
	List<String> topping = new ArrayList<String>();
	
	
	public SimplePizza() {
		this.ingredients.add("ciasto");
		this.ingredients.add("sos");

	}
	
	public List<String> getToppings() {
		return topping;
	}
	
	public List<String> getIngredients() {
		return ingredients;
	}


}
