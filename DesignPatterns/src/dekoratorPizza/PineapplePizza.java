package dekoratorPizza;

import java.util.List;

public class PineapplePizza implements Pizza {

	private Pizza simplePizza;

	public PineapplePizza(Pizza simplePizza) {

		this.simplePizza = simplePizza;
	}

	@Override
	public List<String> getToppings() {
		List<String > toppings = simplePizza.getToppings();
		toppings.add("pineapple");
		toppings.add("cheese");
		return toppings;
	}

	@Override
	public List<String> getIngredients() {

		return simplePizza.getIngredients();
	}

	@Override
	public String toString() {
		return "PineapplePizza: toppings: " + getToppings() + ", ingredients: " + getIngredients();
	}
}
