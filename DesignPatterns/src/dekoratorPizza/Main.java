package dekoratorPizza;

public class Main {

	public static void main(String[] args) {

		Pizzeria pizzeria = new Pizzeria();

		Pizza pineapple = new PineapplePizza(new SimplePizza());
		Pizza margherita = new MargheritaPizza(new SimplePizza());
		Pizza sausage = new SausagePizza(new SimplePizza());
		Pizza pepperoni = new PepperoniPizza(new SimplePizza());
		
		pizzeria.addPizza(pineapple);
		pizzeria.addPizza(margherita);
		pizzeria.addPizza(sausage);
		pizzeria.addPizza(pepperoni);
		
		pizzeria.printMenu();

	}

}
