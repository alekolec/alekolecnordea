package dekoratorPizza;

import java.util.List;

public class SausagePizza implements Pizza {
	Pizza simplePizza;

	public SausagePizza(Pizza simplePizza) {

		this.simplePizza = simplePizza;
	}

	@Override
	public List<String> getToppings() {
		List<String> toppings = simplePizza.getToppings();
		toppings.add("Sausage");
		return toppings;
	}

	@Override
	public List<String> getIngredients() {
		List<String> ingredients = simplePizza.getIngredients();
		return ingredients;
	}

	@Override
	public String toString() {
		return "SausagePizza: toppings: " + getToppings() + ", ingredients: " + getIngredients();
	}

}
