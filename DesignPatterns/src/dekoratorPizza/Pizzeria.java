package dekoratorPizza;

import java.util.HashSet;
import java.util.Set;

public class Pizzeria {

	Set <Pizza> menu = new HashSet<Pizza>();

	public void addPizza(Pizza pizza){
		menu.add(pizza);
	}

	public Set<Pizza> getMenu() {
		return menu;
	}

	public void printMenu() {
		for(Pizza pizza : menu) {
			System.out.println(pizza);
		}
	}

}
