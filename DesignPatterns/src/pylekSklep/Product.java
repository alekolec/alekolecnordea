package pylekSklep;

public class Product {
	private final String name;
	private final int price;
	private final String description;
	
	public Product(String name, int price, String description) {

		this.name = name;
		this.price = price;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public int getPrice() {
		return price;
	}

	public String getDescription() {
		return description;
	}

	//nie moze byc setterow, bo musza byc niezmienne
	
}
