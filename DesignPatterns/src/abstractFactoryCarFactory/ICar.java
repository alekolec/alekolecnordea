package abstractFactoryCarFactory;

public interface ICar {
	
	void drive();
	double getAccel();
	double getMaxSpeed();

}
