package abstractFactoryCarFactory;

public abstract class CarFactory {
	
	public static AbstractCar createBMWM3() {
		return new AbstractCar("BMW", 250.0, 0.8);
	}

	public static AbstractCar createHondaCivic() {
		return new AbstractCar("Honda", 251.0, 0.75);
	}
}
