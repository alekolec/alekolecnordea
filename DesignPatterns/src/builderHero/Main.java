package builderHero;

public class Main {

	public static void main(String[] args) {
		
		Hero.Builder builder = new Hero.Builder("nana","nana");
		builder.setAlive(true).setHeight(0.23).setHp(25);
		
		Hero h = builder.create();
		System.out.println(h);
		
		builder.setSurname("abc");
		
		Hero h2 = builder.create();
		System.out.println(h2);

	}

}
