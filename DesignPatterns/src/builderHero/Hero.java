package builderHero;

public class Hero {

	private String m_name, m_surname, m_legacy;
	private double m_height, m_width, m_strength;
	private int m_level, m_hp, m_handCount;
	private boolean m_isAlive;
	
	

	public Hero(String m_name, String m_surname, String m_legacy, double m_height, double m_width, double m_strength,
			int m_level, int m_hp, int m_handCount, boolean m_isAlive) {
		this.m_name = m_name;
		this.m_surname = m_surname;
		this.m_legacy = m_legacy;
		this.m_height = m_height;
		this.m_width = m_width;
		this.m_strength = m_strength;
		this.m_level = m_level;
		this.m_hp = m_hp;
		this.m_handCount = m_handCount;
		this.m_isAlive = m_isAlive;
	}



	private Hero(Builder builder) {
		m_name = builder.name;
		m_surname = builder.surname;
		m_legacy = builder.legacy;
		
		m_height = builder.height;
		m_width = builder.width;
		m_strength = builder.strength;
		
		m_level = builder.level;
		m_hp = builder.hp;
		m_handCount = builder.handCount;
		
		m_isAlive = builder.isAlive;
	}
	
	

	@Override
	public String toString() {
		return "Hero [m_name=" + m_name + ", m_surname=" + m_surname + ", m_legacy=" + m_legacy + ", m_height="
				+ m_height + ", m_width=" + m_width + ", m_strength=" + m_strength + ", m_level=" + m_level + ", m_hp="
				+ m_hp + ", m_handCount=" + m_handCount + ", m_isAlive=" + m_isAlive + "]";
	}



	public static class Builder {
		private String name, surname, legacy;
		private double height, width, strength;
		private int level, hp, handCount;
		private boolean isAlive;

		public Builder(String name, String surname) {
			this.name = name;
			this.surname = surname;
			this.handCount = 4;

		}

		public Builder setName(String name) {   // dzi�ki temu, �e zwracamy Builder mo�emy w jednej linii wywo�ywa� set.set.set
			this.name = name;
			return this;
		}

		public Builder setSurname(String surname) {
			this.surname = surname;
			return this;
		}

		public Builder setLegacy(String legacy) {
			this.legacy = legacy;
			return this;
		}

		public Builder setHeight(double height) {
			this.height = height;
			return this;
		}

		public Builder setWidth(double width) {
			this.width = width;
			return this;
		}

		public Builder setStrength(double strength) {
			this.strength = strength;
			return this;
		}

		public Builder setLevel(int level) {
			this.level = level;
			return this;
		}

		public Builder setHp(int hp) {
			this.hp = hp;
			return this;
		}

		public Builder setHandCount(int handCount) {
			this.handCount = handCount;
			return this;
		}

		public Builder setAlive(boolean isAlive) {
			this.isAlive = isAlive;
			return this;
		}

		public Hero create() {    // jesli by byl konstruktor Hero public to zastapiloby to Hero h = new Hero(builder)
			return new Hero(this);
		}

	}

}
