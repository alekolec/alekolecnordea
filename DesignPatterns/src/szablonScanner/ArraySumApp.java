package szablonScanner;

import java.util.Scanner;

public class ArraySumApp extends StandardConsoleAppTemplateMethod {
	int[] tablica;
	int suma;

	@Override
	public void showResult() {
		System.out.println(suma);

	}

	@Override
	public void calculateResult() {
		suma = 0;
		for (int i : tablica) {
			suma += i;
		}

	}

	@Override
	public void getData(Scanner scanner) {
		String tekst = scanner.nextLine();

		String[] tablicaTekstu = tekst.split(",");
		tablica = new int[tablicaTekstu.length];

		for (int i = 0; i < tablicaTekstu.length; i++) {

			
			tablica[i] = Integer.parseInt(tablicaTekstu[i]);
		}
	}

	public static void main(String[] args) {
		new ArraySumApp().main();

	}

}
