package szablonScanner;

import java.util.Scanner;

public class BiggerNumberApp extends StandardConsoleAppTemplateMethod {
	private int a;
	private int b;
	private int result;

	@Override
	public void showResult() {
		System.out.println(result);

	}

	@Override
	public void calculateResult() {
		if (a > b) {
			result = a;
		} else {

			result = b;
		}
	}

	@Override
	public void getData(Scanner scanner) {
		System.out.println("Podaj pierwsz� liczb�");
		a = scanner.nextInt();
		System.out.println("Podaj drug� liczb�");
		b = scanner.nextInt();

	}

	public static void main(String[] args) {
		new BiggerNumberApp().main();
	}

}
