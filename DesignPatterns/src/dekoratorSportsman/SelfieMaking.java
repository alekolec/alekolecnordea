package dekoratorSportsman;

public class SelfieMaking implements Sportsman{
	
	private Sportsman sportsman;
	
	public SelfieMaking(Sportsman sportsman) {
		this.sportsman = sportsman;
	}
	
	@Override
	public void prepare() {
		sportsman.prepare();
		System.out.println("Selfie!");
		
	}
	
	@Override
	public void doPumps(int number) {
		sportsman.doPumps(number);
		System.out.println("Selfie!");
		
	}
	
	@Override
	public void doSquats(int number) {
		sportsman.doSquats(number);
		System.out.println("Selfie!");
		
	}
	
	@Override
	public void doCrunches(int number) {
		sportsman.doCrunches(number);
		System.out.println("Selfie!");
		
	}

}
