package dekoratorSportsman;

public class Main {

	public static void main(String[] args) {
		
		FitnessStudio fitnessStudio = new FitnessStudio();
		Sportsman sportsman = new BasicSportsman();
		Sportsman waterDrinker = new WaterDrinking(sportsman);
		Sportsman doubleSeriesDrinker = new DoubleSeries(waterDrinker);
		
		//fitnessStudio.train(waterDrinker);
		
		Sportsman selfier = new SelfieMaking(sportsman);
		Sportsman noPrepare = new NoPrepare(sportsman);
		fitnessStudio.train(doubleSeriesDrinker);
		System.out.println("");
		fitnessStudio.train(selfier);
		System.out.println("");
		fitnessStudio.train(noPrepare);
		System.out.println("");
		
		Sportsman dziwak = new WaterDrinking(new WaterDrinking(new DoubleSeries(new NoPrepare(sportsman))));
		fitnessStudio.train(dziwak);
		
		Sportsman pijeWodeBezRozgrzewki = new WaterDrinking(new NoPrepare(new BasicSportsman()));
		fitnessStudio.train(pijeWodeBezRozgrzewki);

	}

}
