package dekoratorSportsman;

public class BasicSportsman implements Sportsman {

	@Override
	public void prepare() {
		System.out.println("Rozgrzewka..");

	}

	@Override
	public void doPumps(int number) {

		for (int i = 1; i <= number; i++) {
			System.out.println(i + " Pompka");
		}

	}

	@Override
	public void doSquats(int number) {

		for (int i = 1; i <= number; i++) {
			System.out.println(i + " Przysiad");
		}

	}

	@Override
	public void doCrunches(int number) {

		for (int i = 1; i <= number; i++) {
			System.out.println(i + " Brzuszek");
		}

	}

}
