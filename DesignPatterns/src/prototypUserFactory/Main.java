package prototypUserFactory;

import java.util.Arrays;

import static prototypUserFactory.PERMISSION.*;

public class Main {
	public static void main(String[] args) {
		UserFactory userFactory = new UserFactory();

		userFactory.addPrototype(new User("", "", "Client", Arrays.asList(CREATE_ORDERS)));

		userFactory.addPrototype(new User("", "", "Menager", Arrays.asList(PERMISSION.values())));

		userFactory.addPrototype(
				new User("", "", "Seller", Arrays.asList(CREATE_PRODUCTS, DELETE_PRODUCTS, MODIFY_PRODUCTS)));

		User user = userFactory.create("Edek", "123", "Client");
		System.out.println(user);

		User seller = userFactory.create("Edek", "234", "Seller");
		System.out.println(seller);

	}
}
