package prototypUserFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

public class User {
	private String nickname;
	private String password;
	private String kind;
	private EnumSet<PERMISSION> permissions;

	public User(String nickname, String password, String kind, Collection<PERMISSION> permissions) {
		this.nickname = nickname;
		this.password = password;
		this.kind = kind;
		this.permissions = EnumSet.copyOf(permissions);

	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public void setPermissions(Collection<PERMISSION> permissions) {
		this.permissions = EnumSet.copyOf(permissions);
	}
	
	public Set<PERMISSION> getPermissions() {
		return permissions;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kind == null) ? 0 : kind.hashCode());
		result = prime * result + ((nickname == null) ? 0 : nickname.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((permissions == null) ? 0 : permissions.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (kind == null) {
			if (other.kind != null)
				return false;
		} else if (!kind.equals(other.kind))
			return false;
		if (nickname == null) {
			if (other.nickname != null)
				return false;
		} else if (!nickname.equals(other.nickname))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (permissions == null) {
			if (other.permissions != null)
				return false;
		} else if (!permissions.equals(other.permissions))
			return false;
		return true;
	}

	@Override
	protected User clone() throws CloneNotSupportedException {

		return new User(nickname, password, kind, permissions);
	}

	@Override
	public String toString() {
		return "User [nickname=" + nickname + ", password=" + password + ", kind=" + kind + ", permissions="
				+ permissions + "]";
	}
	
	

}
