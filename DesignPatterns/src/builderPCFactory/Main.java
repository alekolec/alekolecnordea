package builderPCFactory;

public class Main {

	public static void main(String[] args) {

		AbstractPC.Builder builder = new AbstractPC.Builder();
		builder.setBrand(COMPUTER_BRAND.APPLE).setName("Apple1").setRamCount(3.7).setOverclocked(true);

		AbstractPC apple1 = builder.create();
		System.out.println(apple1);

		builder.setBrand(COMPUTER_BRAND.ASUS).setName("Asus1").setCpu_power(56);
		AbstractPC asus1 = builder.create();
		System.out.println(asus1.getM_name());
	}

}
