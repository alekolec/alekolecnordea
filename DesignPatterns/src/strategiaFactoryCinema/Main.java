package strategiaFactoryCinema;

public class Main {
	
public static void main(String[] args) {
	IWatchersPolicy policy = new NoWatcherCheckPolicy();
	
	
	Cinema cinema = new Cinema(3, policy, MovieType.ADULT);
	
	cinema.addWatcher(new Watcher(2, "Ja�", false));
	cinema.addWatcher(new Watcher(20, "Sta�", true));
	cinema.addWatcher(new Watcher(45, "Miros�aw", false));
	cinema.addWatcher(new Watcher(60, "Zdzis�aw", true));
	
	cinema.startMovie();
	cinema.checkWatchers();
	cinema.stopMovie();
	
}
}
