package strategiaFactoryCinema;

public class Watcher {
	private int age;
	private String name;
	private boolean isStudent;
	
	public Watcher(int age, String name, boolean isStudent) {
		this.age = age;
		this.name = name;
		this.isStudent = isStudent;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isStudent() {
		return isStudent;
	}

	public void setStudent(boolean isStudent) {
		this.isStudent = isStudent;
	}
	
	
	

}
