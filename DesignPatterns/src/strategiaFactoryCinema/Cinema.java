package strategiaFactoryCinema;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Cinema {
	private int numberOfSeats;
	private IWatchersPolicy policy;
	private MovieType currentMovieType;
	private List<Ticket> watchersList;
	private boolean isMovieStarted;

	public Cinema(int numberOfSeats, IWatchersPolicy policy, MovieType currentMovieType) {
		this.numberOfSeats = numberOfSeats;
		this.policy = policy;
		this.currentMovieType = currentMovieType;
		this.watchersList = new ArrayList<>();
		this.isMovieStarted = false;
	}

	public void setPolicy(IWatchersPolicy policy) {
		this.policy = policy;
	}

	public void addWatcher(Watcher w) {
		if (isMovieStarted) {
			System.out.println("Nie mo�na kupowa� - film si� rozpocz��");
			return;
		} 
			if (watchersList.size() >= numberOfSeats) {
				System.out.println("Brakuje ju� miejsc! Nie wejdziesz " + w.getName());
			} else {
				if (w.getAge() < 3) {
					Ticket ticketKid = TicketFactory.createKidTicket(w);
					watchersList.add(ticketKid);
				} else if (w.getAge() < 21) {
					Ticket ticketUnderAge = TicketFactory.createUnderageTicket(w);
					if (w.isStudent()) {
						ticketUnderAge.setPrice(ticketUnderAge.getPrice() * 0.8);
					}
					watchersList.add(ticketUnderAge);
				} else {
					Ticket ticketAdult = TicketFactory.createAdultTicket(w);
					watchersList.add(ticketAdult);
				}
			}
		}
	

	public void startMovie() {
		isMovieStarted = true;
	}

	public void stopMovie() {
		isMovieStarted = false;
		watchersList.clear();
	}

	public void checkWatchers() {
		Iterator<Ticket> it = watchersList.iterator();
		while (it.hasNext()) {
			Ticket ticket = it.next();
			if(policy.checkWatcher(ticket, currentMovieType)) {
				System.out.println("Przykro mi, to film nie dla Ciebie, wylatujesz - " + ticket.getWatcher().getName());
				it.remove();
			}
	}
	}
}
