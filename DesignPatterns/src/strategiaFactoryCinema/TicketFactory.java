package strategiaFactoryCinema;

public class TicketFactory {
	
	public static Ticket createAdultTicket(Watcher w) {
		return new Ticket(w, MovieType.ADULT);
	}

	public static Ticket createUnderageTicket(Watcher w) {
		return new Ticket(w, MovieType.UNDERAGE);
	}
	
	public static Ticket createKidTicket(Watcher w) {
		return new Ticket(w, MovieType.KID);
	}
}
