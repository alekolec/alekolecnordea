package strategiaFactoryCinema;

public class UnderageWatcherCheckPolicy implements IWatchersPolicy {

	@Override
	public boolean checkWatcher(Ticket ticketChecked, MovieType currentMovie) {
		if(ticketChecked.getTicketType().getType() < currentMovie.getType()){
			return true;	// wyrzucam
		}
		return false;	// nie wyrzucam
	}

}
