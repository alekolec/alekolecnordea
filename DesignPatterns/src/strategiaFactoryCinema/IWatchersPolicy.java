package strategiaFactoryCinema;

public interface IWatchersPolicy {
	boolean checkWatcher(Ticket ticketChecked, MovieType currentMovie);
}