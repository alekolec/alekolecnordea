package strategiaFactoryCinema;

import java.util.Random;

public class LoudWatcherCheckPolicy implements IWatchersPolicy {
	private Random r = new Random();

	@Override
	public boolean checkWatcher(Ticket ticketChecked, MovieType currentMovie) {
		int los = r.nextInt(100);
		if (los < 10) {
			return true; // wyrzucam dla <10
		}
		return false;
	}
}