package strategiaFactoryCinema;

public class NoWatcherCheckPolicy implements IWatchersPolicy {

	public boolean checkWatcher(Ticket ticketChecked, MovieType currentMovie) {
		return false;
	}

}
