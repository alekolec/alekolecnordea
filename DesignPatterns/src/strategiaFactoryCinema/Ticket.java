package strategiaFactoryCinema;

public class Ticket {
	private double price = 20;	
	private Watcher watcher;
	private MovieType type; // na jaki rodzaj filmu jest ten bilet (na podstawie wieku!)
	
	public Watcher getWatcher() {
		return watcher;
	}
	public void setWatcher(Watcher watcher) {
		this.watcher = watcher;
	}
	public MovieType getTicketType() {
		return type;
	}
	public void setType(MovieType type) {
		this.type = type;
	}
	public Ticket(Watcher watcher, MovieType type) {

		this.watcher = watcher;
		this.type = type;
	}
	
	public double getPrice() {
		return price;
	}
	
	public  void setPrice(double price) {
		this.price = price;
	}

	
	
	
	

}
