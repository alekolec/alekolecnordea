package stanSimSimulator;

public class Sim {

	private State state;

	public Sim() {
		this.state = State.RESTED;
	}

	public void rest() {
		System.out.println("Odpoczywam...");
		state = State.RESTED;
	}

	public void work() {
		state.work();
		state = State.TIRED;
	}
	
	public void reward() {
		state = State.MOTIVATE;
	}

	private enum State {
		RESTED {
			@Override
			void work() {
				System.out.println("Ok. Pracuj�");
								
			}
		},
		TIRED {
			@Override
			void work() {
				System.out.println("Jestem zm�czony, nie b�d� pracowa�.");

			}
		},
		
		MOTIVATE {
			@Override
			void work() {
				System.out.println("Pracuj� bardzo wydajnie");

			}
		};
		

		abstract void work();
			
		
	}

}
